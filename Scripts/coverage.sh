#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd ../Server/
coverage-3.6 run --source=ModelChecking manage.py test ModelChecking.tests
coverage-3.6 html --directory="$DIR/html"
rm .coverage
cd $DIR/html
firefox index.html
