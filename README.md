Frontend for Spin
=============================

Web-interface for the [Spin Model Checker](http://spinroot.com/spin/whatispin.html "Spin Project").

REQUIREMENTS
------------

* Linux
* Python 3.6+
* Django 1.10.5+
* Psycopg 2.7+
* PostgreSQL 9.5.9+

QUICK START
-----------

###### Creating Database

At the first stage you should create empty database with owner account. To do this, go to the query mode to the database:

        sudo -u postgres psql

Now you should enter the following queries. Database name must be 'mcf', username must be 'admin', but password could be arbitrary:

        CREATE DATABASE mcf;
        CREATE USER admin WITH password 'yourpass';
        GRANT ALL ON DATABASE mcf TO admin;
        \q

Create new file with your password:

        echo "yourpass" > Server/Configuration/password.txt

PostgreSQL must be run on the 'location:5432' address:

        sudo /etc/init.d/postgresql restart

Make all database migrations:

        cd Server/
        sudo python3.6 manage.py makemigrations
        sudo python3.6 manage.py migrate

###### Django Secret Key Generation

After that you should generate secret key for the django server. You can use online generators, like [this](https://djskgen.herokuapp.com/ "Django Key Generator").
Create new file with the generated key in the "Configuration" folder:

        echo "yourkey" > Server/Configuration/key.txt

###### Starting Django Server Locally

To start django server locally you should run the following script with superuser privileges:

        cd Scripts/
        sudo sh autorun.sh

After that the server is available at localhost:80. If this port is used by another application, then the script will cause an error.
