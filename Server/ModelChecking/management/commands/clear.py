from django.core.management import BaseCommand

from ModelChecking.models import *


class Command(BaseCommand):

    @staticmethod
    def clear_curator():
        Curator.objects.all().delete()

    @staticmethod
    def clear_member():
        Member.objects.all().delete()

    @staticmethod
    def clear_group():
        Group.objects.all().delete()

    @staticmethod
    def clear_file():
        File.objects.all().delete()

    @staticmethod
    def clear_project():
        Project.objects.all().delete()

    @staticmethod
    def clear_user():
        User.objects.all().exclude(user_type=UserType.SUPERUSER).delete()

    def handle(self, *args, **options):
        self.clear_curator()
        self.clear_member()
        self.clear_group()
        self.clear_file()
        self.clear_project()
        self.clear_user()
