import random
import string
from encodings import utf_8
from random import randint

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

from ModelChecking.models import *


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    @staticmethod
    def generate_boolean():
        return bool(random.getrandbits(1))

    @staticmethod
    def generate_string(length):
        return ''.join(random.choice(string.printable) for _ in range(randint(1, length)))

    @staticmethod
    def generate_email(length):
        return Command.generate_identifier(length - 10) + '@' + Command.generate_identifier(5) + '.' + Command.generate_identifier(3)

    @staticmethod
    def generate_password(length):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(randint(5, length)))

    @staticmethod
    def generate_identifier(length):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(randint(1, length)))

    @staticmethod
    def generate_bytes(length):
        return ''.join(random.choice(string.hexdigits[:-6]) for _ in range(randint(0, length)))

    @staticmethod
    def generate_name(length):
        return (''.join(random.choice(string.ascii_letters) for _ in range(randint(1, length)))).capitalize()

    @staticmethod
    def generate_position():
        res = random.choice(['Assistant professor', 'Assistant', 'Teacher', 'Senior teacher', 'Professor'])
        return res

    @staticmethod
    def generate_integer(minimum, maximum):
        return randint(minimum, maximum)

    @staticmethod
    def add_user(count):
        first_user_id = None
        for _ in range(count):
            email = Command.generate_email(EMAIL_LENGTH)
            while User.objects.filter(email=email).exists():
                email = Command.generate_email(EMAIL_LENGTH)

            password = Command.generate_password(PASSWORD_LENGTH)
            user_type = randint(UserType.STUDENT, UserType.TEACHER)
            first_name = Command.generate_name(NAME_LENGTH)
            last_name = Command.generate_name(NAME_LENGTH)
            position = Command.generate_position() if user_type == UserType.TEACHER else 'Student'

            user = User(email=email, password=password, user_type=user_type, first_name=first_name, last_name=last_name, position=position)
            user.save()

            if first_user_id is None:
                first_user_id = user.user_id

        print(str(count) + ' row(s) successfully added in table User.')
        return first_user_id

    @staticmethod
    def add_group(count):
        first_group_id = None
        for _ in range(count):
            title = Command.generate_string(TITLE_LENGTH)

            group = Group(title=title)
            group.save()

            if first_group_id is None:
                first_group_id = group.group_id

        print(str(count) + ' row(s) successfully added in table Group.')
        return first_group_id

    @staticmethod
    def add_curator(count, first_user_id, first_group_id):
        first_curator_id = None
        for _ in range(count):
            user_id = Command.generate_integer(first_user_id, first_user_id + count - 1)
            group_id = Command.generate_integer(first_group_id, first_group_id + count - 1)

            try:
                user_id = User.objects.get(user_id=user_id)
                group_id = Group.objects.get(group_id=group_id)
            except ObjectDoesNotExist:
                continue

            is_owner = Command.generate_boolean()
            is_notification = Command.generate_boolean()

            curator = Curator(user_id=user_id, group_id=group_id, is_owner=is_owner, is_notification=is_notification)
            curator.save()

            if first_curator_id is None:
                first_curator_id = curator.curator_id

        print(str(count) + ' row(s) successfully added in table Curator.')
        return first_curator_id

    @staticmethod
    def add_member(count, first_user_id, first_group_id):
        first_member_id = None
        for _ in range(count):
            user_id = Command.generate_integer(first_user_id, first_user_id + count - 1)
            group_id = Command.generate_integer(first_group_id, first_group_id + count - 1)

            try:
                user_id = User.objects.get(user_id=user_id)
                group_id = Group.objects.get(group_id=group_id)
            except ObjectDoesNotExist:
                continue

            is_notification = Command.generate_boolean()

            member = Member(user_id=user_id, group_id=group_id, is_notification=is_notification)
            member.save()

            if first_member_id is None:
                first_member_id = member.member_id

        print(str(count) + ' row(s) successfully added in table Member.')
        return first_member_id

    @staticmethod
    def add_project(count, first_user_id):
        first_project_id = None
        for _ in range(count):
            user_id = Command.generate_integer(first_user_id, first_user_id + count - 1)

            try:
                user_id = User.objects.get(user_id=user_id)
            except ObjectDoesNotExist:
                continue

            title = Command.generate_string(TITLE_LENGTH)

            project = Project(user_id=user_id, title=title)
            project.save()

            if first_project_id is None:
                first_project_id = project.project_id

        print(str(count) + ' row(s) successfully added in table Project.')
        return first_project_id

    @staticmethod
    def add_file(count, first_project_id):
        first_file_id = None
        for _ in range(count):
            project_id = Command.generate_integer(first_project_id, first_project_id + count - 1)

            try:
                project_id = Project.objects.get(project_id=project_id)
            except ObjectDoesNotExist:
                continue

            name = Command.generate_identifier(FILENAME_LENGTH)
            extension = Command.generate_identifier(EXTENSION_LENGTH)
            byte_array = bytes(Command.generate_bytes(1024 * 1024), encoding=utf_8.getregentry().name)

            file = File(project_id=project_id, name=name, extension=extension, byte_array=byte_array)
            file.save()

            if first_file_id is None:
                first_file_id = file.file_id

        print(str(count) + ' row(s) successfully added in table File.')
        return first_file_id

    def handle(self, *args, **options):
        count = int(options['count'])

        if count <= 0:
            print('Incorrect count. Please type integer value.')
            return

        first_user_id = self.add_user(count)
        first_group_id = self.add_group(count)
        first_curator_id = self.add_curator(count, first_user_id=first_user_id, first_group_id=first_group_id)
        first_member_id = self.add_member(count, first_user_id=first_user_id, first_group_id=first_group_id)
        first_project_id = self.add_project(count, first_user_id=first_user_id)
        first_file_id = self.add_file(count, first_project_id=first_project_id)
