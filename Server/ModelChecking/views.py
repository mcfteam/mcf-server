from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext

from ModelChecking.scripts.authentication.authentication import authentication_request_handler
from ModelChecking.scripts.web.group import group_request_handler
from ModelChecking.scripts.web.project import project_request_handler
from ModelChecking.scripts.web.spin import spin_request_handler
from ModelChecking.scripts.web.student import student_request_handler
from ModelChecking.scripts.web.teacher import teacher_request_handler
from ModelChecking.scripts.web.test import test_request_handler


def test(request):
    context = test_request_handler(request)
    return render(request, 'test.html', context)


def spin(request):
    context = {}
    if authentication_request_handler(context, request):
        return HttpResponseRedirect(request.get_full_path())

    redirect = spin_request_handler(context, request)
    if isinstance(redirect, HttpResponseRedirect):
        return redirect

    return render(request, 'spin.html', context, RequestContext(request))


def teacher(request):
    context = {}
    if authentication_request_handler(context, request):
        return HttpResponseRedirect(request.get_full_path())

    teacher_request_handler(context, request)
    return render(request, 'teacher.html', context)


def student(request):
    context = {}
    if authentication_request_handler(context, request):
        return HttpResponseRedirect(request.get_full_path())

    student_request_handler(context, request)
    return render(request, 'student.html', context)


def project(request):
    context = {}
    if authentication_request_handler(context, request):
        return HttpResponseRedirect(request.get_full_path())

    project_request_handler(context, request)
    return render(request, 'project.html', context, RequestContext(request))


def group(request):
    context = {}
    if authentication_request_handler(context, request):
        return HttpResponseRedirect(request.get_full_path())

    group_request_handler(context, request)
    return render(request, 'group.html', context)
