from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory
from django.test import TestCase

from ModelChecking.scripts.web.spin import spin_request_handler


class SpinTestCase(TestCase):
    """
        Test case for 'Spin' server application.

        :author Boyarkin Nikita
    """

    def setUp(self):
        self.factory = RequestFactory()

    def test_get_request_context_without_data(self):
        request = self.factory.get('/')
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {}
        self.assertEquals(context, expected)

    def test_get_request_context_with_data(self):
        data = {'first': 'value', 'second': 'value'}
        request = self.factory.get('/', data)
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {}
        self.assertEquals(context, expected)

    def test_post_request_context_without_data(self):
        request = self.factory.post('/')
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {'spinErrorCode': 0xF}
        self.assertEquals(context, expected)

    def test_post_request_context_without_constructor_mode(self):
        data = {'options': 'some', 'constructor': 'some', 'promela-source': 'some', 'command-line': 'some'}
        request = self.factory.post('/spin', data)
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {'spinErrorCode': 0xF}
        self.assertEquals(context, expected)

    def test_post_request_context_without_options(self):
        data = {'constructor-mode': 'true', 'constructor': 'some', 'promela-source': 'some', 'command-line': 'some'}
        request = self.factory.post('/', data)
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {'constructorMode': 'true', 'spinErrorCode': 0x10}
        self.assertEquals(context, expected)

    def test_post_request_context_without_constructor(self):
        data = {'constructor-mode': 'true', 'options': 'some', 'promela-source': 'some', 'command-line': 'some'}
        request = self.factory.post('/', data)
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {'constructorMode': 'true', 'spinErrorCode': 0x11, 'savedOptions': 'some'}
        self.assertEquals(context, expected)

    def test_post_request_context_without_promela_source(self):
        data = {'constructor-mode': 'true', 'verification-mode': 'true', 'options': 'some', 'constructor': 'some', 'command-line': 'some'}
        request = self.factory.post('/', data)
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {'constructorMode': 'true', 'verificationMode': 'true', 'spinErrorCode': 0xE, 'savedConstructor': 'some', 'savedOptions': 'some'}
        self.assertEquals(context, expected)

    def test_post_request_context_wrong_constructor_mode(self):
        data = {'constructor-mode': 'wrong', 'options': 'some', 'constructor': 'some', 'promela-source': 'some', 'command-line': 'some'}
        request = self.factory.post('/spin', data)
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {'spinErrorCode': 0x1}
        self.assertEquals(context, expected)

    def test_post_request_context_wrong_constructor(self):
        data = {'constructor-mode': 'false', 'options': 'some', 'constructor': 'some', 'promela-source': 'some', 'command-line': 'some'}
        request = self.factory.post('/', data)
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {'constructorMode': 'false', 'spinErrorCode': 0x2, 'savedOptions': 'some'}
        self.assertEquals(context, expected)

    def test_post_request_wrong_promela_source(self):
        data = {'constructor-mode': 'true', 'verification-mode': 'true', 'options': 'some', 'constructor': 'some', 'promela-source': '', 'command-line': 'some'}
        request = self.factory.post('/', data)
        request.user = AnonymousUser()
        request.session = {}
        context = {}
        spin_request_handler(context, request)
        expected = {'constructorMode': 'true', 'verificationMode': 'true', 'spinErrorCode': 0xD, 'savedConstructor': 'some', 'savedOptions': 'some'}
        self.assertEquals(context, expected)

        # TODO Create more execution tests.
