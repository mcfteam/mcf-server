from django.test import TestCase

from ModelChecking.scripts.database.add import *


class DatabaseAddTestCase(TestCase):
    def test_add_student_1(self):
        try:
            add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_add_student_2(self):
        try:
            add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_add_same_student_1(self):
        try:
            add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')

            # Must throw exception, test failed.
            assert True

        except AlreadyExistsException:
            # Everything ok.
            pass

    def test_add_same_student_2(self):
        try:
            add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            # Must throw exception, test failed.
            assert True

        except AlreadyExistsException:
            # Everything ok.
            pass

    def test_add_same_student_3(self):
        try:
            add_student('tomas99@yahoo.com', 'tom123', 'Tomas', 'Lord')
            add_student('tomas99@yahoo.com', 'tom123', 'Tomas', 'Lord')
            # Everything ok.
            pass

        except AlreadyExistsException:
            # Something goes wrong.
            assert True

    def test_add_same_student_4(self):
        try:
            add_student('suzana93@gmail.com', 'nana93', 'Sozana', 'vektorevna')
            add_student('suzana93@gmail.com', 'suzana000', 'Dasha', 'Fadeeva')
            # Must throw same student, test failed.
            assert True

        except AlreadyExistsException:
            # Everything ok.
            pass

    def test_add_student_empty_field_1(self):
        try:
            add_student('iavn2010@gmail.com', '120006', '', 'Ivanov')
            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_student_empty_field_2(self):
        try:
            add_student('iavn2010@gmail.com', '120006', '', '')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_student_empty_field_3(self):
        try:
            add_student('iavn2010@gmail.com', '120006', 'Ivan', '')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_student_empty_field_4(self):
        try:
            add_student('', '120006', 'Ivan', 'Ivanov')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_student_empty_field_5(self):
        try:
            add_student('', '', '', '')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_student_email_validation_1(self):
        try:
            add_student('@gmail.com', '123456', 'Ivan', 'Ivanov')
            # Must throw exception , test failed.
            assert True

        except ValidationException:
            # Everything ok.
            pass

    def test_add_student_email_validation_2(self):
        try:
            add_student('ivanivanov@gmail.', '123456', 'Ivan', 'Ivanov')
            # Must throw exception , test failed.
            assert True

        except ValidationException:
            # Everything ok.
            pass

    def test_add_student_email_validation_3(self):
        try:
            add_student('@yahoo.com', 'tom123', 'Tomas', 'Lord')
            # Must throw exception , test failed.
            assert True

        except ValidationException:
            # Everything ok.
            pass

    def test_add_student_email_validation_4(self):
        try:
            add_student('tomas99@yahoo.', 'tom123', 'Tomas', 'Lord')
            # Must throw exception invalid email, test failed.
            assert True

        except ValidationException:
            # Everything ok.
            pass

    def test_add_student_email_validation_5(self):
        try:
            add_student('suzana93@.sdf', 'nana93', 'Sozana', 'vektorevna')
            # Must throw exception , test failed.
            assert True

        except ValidationException:
            # Everything ok.
            pass

    def test_add_same_teacher_1(self):
        try:
            add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')

            # Must throw exception, test failed.
            assert True

        except AlreadyExistsException:
            # Everything ok.
            pass

    def test_add_same_teacher_2(self):
        try:
            add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')

            # Must throw exception, test failed.
            assert True

        except AlreadyExistsException:
            # Everything ok.
            pass

    def test_add_same_teacher_3(self):
        try:
            add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')

            # Must throw exception, test failed.
            assert True

        except AlreadyExistsException:
            # Everything ok.
            pass

    def test_add_teacher_empty_field_1(self):
        try:
            add_teacher('segeysergeev80@gmail.com', 'se1980', '', '', 'Professor')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_teacher_empty_field_2(self):
        try:
            add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', '', 'Professor')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_teacher_empty_field_3(self):
        try:
            add_teacher('segeysergeev80@gmail.com', 'se1980', '', 'Sergeev', 'Professor')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_teacher_empty_field_4(self):
        try:
            add_teacher('', '', '', '', '')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_teacher_empty_field_5(self):
        try:
            add_teacher('georgealexandrovich10@gmail.com', 'G20201', '', '', 'Assistant Teacher')
            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_teacher_email_validation_1(self):
        try:
            add_teacher('@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            # Must throw exception , test failed.
            assert True

        except ValidationException:
            # Everything ok.
            pass

    def test_add_teacher_email_validation_2(self):
        try:
            add_teacher('segeysergeev80@gmail.', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            # Must throw exception , test failed.
            assert True

        except ValidationException:
            # Everything ok.
            pass

    def test_add_teacher_email_validation_3(self):
        try:
            add_teacher('georgealexandrovich10@gmail.', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            # Must throw exception , test failed.
            assert True

        except ValidationException:
            # Everything ok.
            pass

    def test_add_group_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            add_group(teacher_id, '13541/3')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_add_group_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            add_group(teacher_id, '13541/3')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_add_group_empty_field_1(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            add_group(teacher_id, '')

            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_project_1(self):
        try:
            teacher_id = add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            add_project(teacher_id, 'CNC Project')

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_add_project_2(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            add_project(teacher_id, 'Artificial Intelligence Project')

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_add_project_3(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            add_project(teacher_id, 'Image Processing Project')

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_add_project_empty_field_1(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            add_project(teacher_id, '')

            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_file_1(self):
        try:
            teacher_id = add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            project_id = add_project(teacher_id, 'CNC Project')
            add_file(project_id, 'source', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_add_file_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            add_file(project_id, 'source2', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_add_file_3(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            project_id = add_project(teacher_id, 'Artificial Intelligence Project')
            add_file(project_id, 'source3', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_add_file_empty_field_1(self):
        try:
            teacher_id = add_teacher('exandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'CNC Project')
            add_file(project_id, '', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')

            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_file_empty_field_2(self):
        try:
            teacher_id = add_teacher('exandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'CNC Project')
            add_file(project_id, '', '', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')

            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_file_empty_field_3(self):
        try:
            teacher_id = add_teacher('exandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'CNC Project')
            add_file(project_id, '', '', b'')

            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_file_empty_field_4(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            add_file(project_id, '', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')

            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_add_file_empty_field_5(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            add_file(project_id, '', '', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')

            # Must throw exception, test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass
