from django.test import TestCase

from ModelChecking.scripts.database.add import *
from ModelChecking.scripts.database.remove import *


class DatabaseRemoveTestCase(TestCase):
    def test_remove_student_1(self):
        try:
            student_id = add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            remove_student(student_id)

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_remove_unexist_student_1(self):
        try:
            student_id = add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            remove_student(student_id)
            remove_student(student_id)

            # Must throw exception, test failed.
            assert True

        except NotExistsException:
            # Everything ok.
            pass

    def test_remove_group_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            group_id, curator_id = add_group(teacher_id, '13541/3')
            remove_group(group_id)

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_remove_group_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            group_id, curator_id = add_group(teacher_id, '13541/3')
            remove_group(group_id)

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_remove_unexist_group_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            group_id, curator_id = add_group(teacher_id, '13541/3')
            remove_group(group_id)
            remove_group(group_id)

            # Must throw exception, test failed.
            assert True

        except NotExistsException:
            # Everything ok.
            pass

    def test_remove_unexist_group_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            group_id, curator_id = add_group(teacher_id, '13541/3')
            remove_group(group_id)
            remove_group(group_id)

            # Must throw exception, test failed.
            assert True

        except NotExistsException:
            # Everything ok.
            pass

    def test_remove_project_1(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            remove_project(project_id)

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_remove_project_2(self):
        try:
            teacher_id = add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            project_id = add_project(teacher_id, 'CNC Project')
            remove_project(project_id)

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_remove_unexist_project_1(self):
        try:
            teacher_id = add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            project_id = add_project(teacher_id, 'CNC Project')
            remove_project(project_id)
            remove_project(project_id)

            # Must throw exception, test failed.
            assert True

        except NotExistsException:
            # Everything ok.
            pass

    def test_remove_unexist_project_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            remove_project(project_id)
            remove_project(project_id)

            # Must throw exception, test failed.
            assert True

        except NotExistsException:
            # Everything ok.
            pass

    def test_remove_file_1(self):
        try:
            teacher_id = add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            project_id = add_project(teacher_id, 'CNC Project')
            file_id = add_file(project_id, 'source', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')
            remove_file(file_id)

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_remove_file_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            file_id = add_file(project_id, 'source2', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')
            remove_file(file_id)

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_remove_file_3(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            project_id = add_project(teacher_id, 'Artificial Intelligence Project')
            file_id = add_file(project_id, 'source3', 'pml', b'')
            remove_file(file_id)

            # Everything ok.
            pass

        except DatabaseError:
            # Something goes wrong.
            assert True

    def test_remove_unexist_file_1(self):
        try:
            teacher_id = add_teacher('segeysergeev80@gmail.com', 'se1980', 'Sergey', 'Sergeev', 'Professor')
            project_id = add_project(teacher_id, 'CNC Project')
            file_id = add_file(project_id, 'source', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')
            remove_file(file_id)
            remove_file(file_id)

            # Must throw exception, test failed.
            assert True

        except NotExistsException:
            # Everything ok.
            pass

    def test_remove_unexist_file_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            file_id = add_file(project_id, 'source2', 'pml', b'6c9834f2772acbed4fef98a3f317e3962f1f2f80acc17f')
            remove_file(file_id)
            remove_file(file_id)

            # Must throw exception, test failed.
            assert True

        except NotExistsException:
            # Everything ok.
            pass
