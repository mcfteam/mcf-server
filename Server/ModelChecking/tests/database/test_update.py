from django.test import TestCase

from ModelChecking.scripts.database.add import *
from ModelChecking.scripts.database.update import *


class DatabaseUpdateTestCase(TestCase):
    def test_update_student_1(self):
        try:
            student_id = add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            is_updated = update_student(student_id, first_name='Mihail')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_student_2(self):
        try:
            student_id = add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            is_updated = update_student(student_id, password='2342342344234', first_name='Mihail', last_name='Petrov')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_student_3(self):
        try:
            student_id = add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            is_updated = update_student(student_id)

            # Update should not have happened.
            if is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_student_4(self):
        try:
            student_id = add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            is_updated = update_student(student_id, first_name='Vladimir')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_student_5(self):
        try:
            student_id = add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            is_updated = update_student(student_id, password='vla2030vla', first_name='Vladimir', last_name='Mikhaelovitch')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_student_6(self):
        try:
            student_id = add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            is_updated = update_student(student_id)

            # Update should not have happened.
            if is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_student_empty_field_1(self):
        try:
            student_id = add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            update_student(student_id, password='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_student_empty_field_2(self):
        try:
            student_id = add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            update_student(student_id, first_name='', last_name='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_student_empty_field_3(self):
        try:
            student_id = add_student('example@gmail.com', '123456', 'Ivan', 'Ivanov')
            update_student(student_id, password='', first_name='', last_name='Petrov')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_student_empty_field_4(self):
        try:
            student_id = add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            update_student(student_id, password='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_student_empty_field_5(self):
        try:
            student_id = add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            update_student(student_id, first_name='', last_name='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_student_empty_field_6(self):
        try:
            student_id = add_student('ivanivanov@gmail.com', '123456', 'Ivan', 'Ivanov')
            update_student(student_id, password='', first_name='', last_name='Mikhaelovitch')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_teacher_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Ivan', 'Ivanov', 'Professor')
            is_updated = update_teacher(teacher_id, first_name='Mihail')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_teacher_2(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Ivan', 'Ivanov', 'Professor')
            is_updated = update_teacher(teacher_id, password='2342342344234', first_name='Mihail', last_name='Petrov')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_teacher_3(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Ivan', 'Ivanov', 'Professor')
            is_updated = update_teacher(teacher_id)

            # Update should not have happened.
            if is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_teacher_4(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            is_updated = update_teacher(teacher_id, password='sese17')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_teacher_5(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            is_updated = update_teacher(teacher_id, first_name='Segy')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_teacher_6(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            is_updated = update_teacher(teacher_id, password='sese17', first_name='Segy')

            # Update should have happened.
            if not is_updated:
                assert True

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_teacher_empty_field_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Ivan', 'Ivanov', 'Professor')
            update_teacher(teacher_id, password='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_teacher_empty_field_2(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Ivan', 'Ivanov', 'Professor')
            update_teacher(teacher_id, password='2342342344234', first_name='', last_name='Petrov')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_teacher_empty_field_3(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Ivan', 'Ivanov', 'Professor')
            update_teacher(teacher_id, password='2342342344234', first_name='Mihail', last_name='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_teacher_empty_field_4(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Ivan', 'Ivanov', 'Professor')
            update_teacher(teacher_id, password='2342342344234', first_name='', last_name='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_teacher_empty_field_5(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            update_teacher(teacher_id, password='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_teacher_empty_field_6(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            update_teacher(teacher_id, password='sese17', first_name='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_teacher_empty_field_7(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            update_teacher(teacher_id, password='', first_name='')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_group_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            group_id, curator_id = add_group(teacher_id, '13541/3')
            update_group(group_id, '13544/1')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_group_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            group_id, curator_id = add_group(teacher_id, '13544/1')
            update_group(group_id, '45301/1')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_group_3(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            group_id, curator_id = add_group(teacher_id, '13544/2')
            update_group(group_id, '45301/2')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_group_empty_field_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            group_id, curator_id = add_group(teacher_id, '13541/3')
            update_group(group_id, '')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_group_empty_field_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            group_id, curator_id = add_group(teacher_id, '13544/1')
            update_group(group_id, '')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_group_empty_field_3(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            group_id, curator_id = add_group(teacher_id, '13544/2')
            update_group(group_id, '')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_project_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            project_id = add_project(teacher_id, 'CNC Project')
            update_project(project_id, 'Computer Numerical Control Project')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_project_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            update_project(project_id, 'Sorting Pieces using Image Processing Project')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_project_3(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            project_id = add_project(teacher_id, 'Artificial Intelligence Project')
            update_project(project_id, 'Neural Networks Project')

            # Everything ok.
            pass

        except DatabaseException:
            # Something goes wrong.
            assert True

    def test_update_project_empty_field_1(self):
        try:
            teacher_id = add_teacher('example@gmail.com', '123456', 'Sergey', 'Sergeev', 'Professor')
            project_id = add_project(teacher_id, 'CNC Project')
            update_project(project_id, '')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_project_empty_field_2(self):
        try:
            teacher_id = add_teacher('georgealexandrovich10@gmail.com', 'G20201', 'George', 'Alexandrovich', 'Assistant Teacher')
            project_id = add_project(teacher_id, 'Image Processing Project')
            update_project(project_id, '')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass

    def test_update_project_empty_field_3(self):
        try:
            teacher_id = add_teacher('andreypeter@gmail.com', '110022', 'Andrey', 'Nickolaev', 'Professor')
            project_id = add_project(teacher_id, 'Artificial Intelligence Project')
            update_project(project_id, '')

            # Must throw exception , test failed.
            assert True

        except EmptyException:
            # Everything ok.
            pass
