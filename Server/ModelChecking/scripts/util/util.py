"""
Useful functions for widely usage.
"""

import logging
from enum import Enum
from sys import platform

from Configuration.settings import DEBUG


def is_windows():
    """
        Check, that the server based on windows.

        :return bool

        :author Boyarkin Nikita
    """
    if platform == "win32" or platform == "cygwin":
        return True

    return False


def is_linux():
    """
        Check, that the server based on linux.

        :return bool

        :author Boyarkin Nikita
    """
    if platform == "linux" or platform == "linux2":
        return True

    return False


class LogFile(Enum):
    TEST = 'ModelChecking/logs/test.log'
    SPIN = 'ModelChecking/logs/spin.log'
    TEACHER = 'ModelChecking/logs/teacher.log'
    STUDENT = 'ModelChecking/logs/student.log'
    PROJECT = 'ModelChecking/logs/project.log'
    GROUP = 'ModelChecking/logs/group.log'
    REGISTRATION = 'ModelChecking/logs/registration.log'



def log(file, level, message):
    """
        Shell for the logger.log function, which print message into the file with time, and level.

        :param file: Path to logging the file.
        :type file: LogFile enum value.

        :param level: Logging level.
        :type level: 0, 10, 20, 30, 40, 50 (constants from logging class).

        :param message: Logging message.
        :type message: str or unicode.

        :author Boyarkin Nikita
    """

    if not isinstance(file, LogFile):
        raise AttributeError()

    # If DEBUG mode isn't enabled, don't print debug logs.
    if not DEBUG and level <= logging.DEBUG:
        pass

    logger = logging.getLogger()
    handler = logging.FileHandler(str(file.value))

    # Set format for log message.
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    logger.setLevel(level)

    logger.log(level, message)

    handler.close()
    logger.removeHandler(handler)
    pass
