"""
Context definition for usage in spin.html template.
"""
import os
import re
import sys
import uuid
from shutil import Error
from shutil import copyfile
from shutil import rmtree
from subprocess import CalledProcessError
from subprocess import TimeoutExpired
from subprocess import check_output

from django.http import HttpResponseRedirect

from Configuration.settings import DYNAMIC_URL
from Configuration.settings import STATIC_URL
from ModelChecking.scripts.authentication.authentication import get_user_attempt
from ModelChecking.scripts.database.add import add_file
from ModelChecking.scripts.database.exceptions import DatabaseException, AlreadyExistsException, NotExistsException
from ModelChecking.scripts.database.get import get_complete_projects_information, get_file_information
from ModelChecking.scripts.database.update import update_file
from ModelChecking.scripts.database.util import find_file_id
from ModelChecking.scripts.util.util import *

# Timeout for spin process in seconds.
SUBPROCESS_TIMEOUT = 10

# Make directory mode (all permissions for all users).
LINUX_PERMISSIONS_MODE = 0o777

# Full paths to dynamic and static content folders.
DYNAMIC_PATH = 'ModelChecking' + DYNAMIC_URL
STATIC_PATH = 'ModelChecking' + STATIC_URL

# Verification spin flag.
SPIN_VERIFICATION_FLAG = '-a'

# Gcc compiler flag for disabling warnings.
GCC_DISABLE_WARNINGS_FLAG = '-w'

# Gcc compiler flag for creating new executable.
GCC_OUTPUT_FLAG = '-o'

# Promela source filename.
PROMELA_SOURCE_FILENAME = 'source.pml'

# Interim source filename (for gcc compiler).
INTERIM_SOURCE_FILENAME = 'pan.c'

# Spin executable filename.
SPIN_EXECUTABLE_FILENAME = 'spin'

# Gcc compiler executable filename.
GCC_EXECUTABLE_FILENAME = 'gcc'

# Interim executable filename (for gcc compiler).
INTERIM_EXECUTABLE_FILENAME = 'pan'

# Relatively executor filename.
RELATIVELY_EXECUTOR_FILENAME = 'relatively_executor.py'

# Path to spin executable.
SPIN_EXECUTABLE_PATH = STATIC_PATH + 'bin/' + SPIN_EXECUTABLE_FILENAME

# Path to relatively executor.
RELATIVELY_EXECUTOR_PATH = STATIC_PATH + 'py/' + RELATIVELY_EXECUTOR_FILENAME


def spin_request_handler(context, request):
    """
        Handle GET/POST requests from client and generate context variables for client.
        
        :param context: Dictionary with variables for client.
        :type context: dict.

        :param request: Request from client.
        :type request: HttpRequest.

        :return HttpResponseRedirect if necessary.

        :author Boyarkin Nikita
    """

    ip_address = request.environ['REMOTE_ADDR']
    log(LogFile.SPIN, logging.DEBUG, ip_address + ': ' + request.method + ' \'' + request.path + '\'')

    viewer = get_user_attempt(request)
    if viewer is not None:
        viewer_id = viewer.user_id

        try:
            projects_information = get_complete_projects_information(viewer_id)
            context['projectsInformation'] = projects_information
        except DatabaseException:
            pass

    # Get file by id.
    if request.method == 'GET' and 'id' in request.GET:
        viewer = get_user_attempt(request)
        if viewer is None:
            context['spinErrorCode'] = 0x16
            return

        viewer_id = viewer.user_id

        try:
            file_id = int(str(request.GET['id']).strip())
        except ValueError:
            context['spinErrorCode'] = 0x17
            return

        if file_id is None or file_id < 0:
            context['spinErrorCode'] = 0x18
            return

        try:
            file_name, file_extension, file_bytes = get_file_information(viewer_id, file_id)
        except NotExistsException:
            context['spinErrorCode'] = 0x19
            return

        if file_name is None or file_extension is None or file_bytes is None:
            context['spinErrorCode'] = 0x1A
            return

        is_constructor = True if file_extension == '.smcf' else (False if file_extension == '.pml' else None)
        if is_constructor is None:
            context['spinErrorCode'] = 0x1B
            return

        context['spinErrorCode'] = 0x0
        context['constructorMode'] = 'true' if is_constructor else 'false'

        content = file_bytes.tobytes().decode('utf-8')
        if is_constructor:
            context['savedConstructor'] = content
        else:
            context['savedSource'] = content

        return

    errcode = 0x0

    if request.method == 'POST':
        if 'project-id' in request.POST and 'file-name' in request.POST and \
                        'file-extension' in request.POST and 'bytes' in request.POST:
            project_id = str(request.POST['project-id']).strip()
            file_name = str(request.POST['file-name']).strip()
            file_extension = str(request.POST['file-extension']).strip()
            file_bytes = str(request.POST['bytes']).encode('utf-8')

            try:
                project_id = int(project_id)
                file_id = find_file_id(project_id, file_name, file_extension)
                update_file(file_id, name=file_name, extension=file_extension, byte_array=file_bytes)
                return HttpResponseRedirect("/project?id=" + str(project_id))
            except NotExistsException:
                try:
                    add_file(project_id, file_name, file_extension, file_bytes)
                    return HttpResponseRedirect("/project?id=" + str(project_id))
                except DatabaseException:
                    pass
            except ValueError:
                pass
            except DatabaseException:
                pass

        constructor_mode = None

        # If 'constructor-mode' is 'true', expect one more parameter 'constructor'.
        if 'constructor-mode' in request.POST:
            constructor_mode = str(request.POST['constructor-mode']).strip()

            if constructor_mode != 'true' and constructor_mode != 'false':
                errcode = 0x1
                log(LogFile.SPIN, logging.ERROR, ip_address + ': Undefined constructor mode: ' + constructor_mode + '.')
            else:
                context['constructorMode'] = constructor_mode
                constructor_mode = constructor_mode == 'true'
        else:
            errcode = 0xF
            log(LogFile.SPIN, logging.ERROR, ip_address + ': No constructor mode parameter into the post request.')

        # Return 'options' back to client.
        if errcode == 0x0:
            if 'options' in request.POST:
                options = str(request.POST['options']).strip()
                # TODO Check options, or not?
                context['savedOptions'] = options
            else:
                errcode = 0x10
                log(LogFile.SPIN, logging.ERROR, ip_address + ': No options parameter into the post request.')

        # If 'constructor-mode' defined and 'true', return 'constructor' back to client.
        if errcode == 0x0:
            if 'constructor' in request.POST:
                constructor = str(request.POST['constructor']).strip()

                if not constructor_mode and constructor:
                    # If constructor is defined, but constructor mode switched off.
                    errcode = 0x2
                    log(LogFile.SPIN, logging.ERROR, ip_address + ': Constructor is defined, but constructor mode switched off.')
                elif constructor_mode:
                    # TODO Check constructor, or not?
                    context['savedConstructor'] = constructor
            elif constructor_mode:
                errcode = 0x11
                log(LogFile.SPIN, logging.ERROR, ip_address + ': No constructor parameter into the post request.')

        verification_mode = None

        # 'verification-mode' parameter must be into the post request.
        if errcode == 0x0:
            if 'verification-mode' in request.POST:
                verification_mode = str(request.POST['verification-mode']).strip()

                if verification_mode != 'true' and verification_mode != 'false':
                    errcode = 0x12
                    log(LogFile.SPIN, logging.ERROR, ip_address + ': Undefined verification mode: ' + verification_mode + '.')
                else:
                    context['verificationMode'] = verification_mode
                    verification_mode = verification_mode == 'true'
            else:
                errcode = 0x13
                log(LogFile.SPIN, logging.ERROR, ip_address + ': No verification mode parameter into the post request.')

        # Try to execute promela source code.
        if errcode == 0x0:
            if 'promela-source' in request.POST and 'command-line' in request.POST:
                # If defined 'promela-source' and 'command-line' at the same time.
                source_code = str(request.POST['promela-source']).strip()
                command_line = str(request.POST['command-line']).strip()

                if source_code and command_line:
                    context['savedSource'] = source_code
                    context['savedFlags'] = command_line

                    gcc_flags = None
                    spin_flags = None

                    try:
                        gcc_flags, spin_flags = flags_splitter(command_line, verification_mode)
                    except ValueError:
                        log(LogFile.SPIN, logging.ERROR, ip_address + ': Impossible to get spin and gcc flags from command line.')
                        errcode = 0x15

                    if errcode == 0x0:
                        if verification_mode:
                            # Verify promela source code with command line flags.
                            errcode, result = verification_request(source_code, gcc_flags, spin_flags, ip_address)
                        else:
                            # Simulate promela source code with command line flags.
                            errcode, result = simulation_request(source_code, spin_flags, ip_address)

                        if errcode == 0x0:
                            context['operationResult'] = result

                else:
                    # If source code or command line are empty.
                    errcode = 0xD
                    log(LogFile.SPIN, logging.ERROR, ip_address + ': Source code or command line are empty.')
            elif bool('promela-source' in request.POST) ^ bool('command-line' in request.POST):
                # If only one parameter is defined, but requested two.
                errcode = 0xE
                log(LogFile.SPIN, logging.ERROR, ip_address + ': Only one parameter is defined, but requested two: source code and command line.')
            else:
                errcode = 0x14
                log(LogFile.SPIN, logging.ERROR, ip_address + ': No executable parameters into the post request.')

        context['spinErrorCode'] = errcode


def verification_request(source_code, gcc_flags, spin_flags, ip_address='ANONYMOUS'):
    """
        Verify promela source code with special flags.
    
        :param source_code: Promela source code.
        :type source_code: str or unicode.
        
        :param gcc_flags: Flags for gcc compiler. 
        :type gcc_flags: str or unicode.
        
        :param spin_flags: Flags for spin executable.
        :type spin_flags: str or unicode.
        
        :param ip_address: Remote ip address. Default value is 'ANONYMOUS'.
        :type ip_address: str or unicode.
        
        :return error_code, result
        
        :author Boyarkin Nikita
    """

    log(LogFile.SPIN, logging.DEBUG, ip_address + ': ' + SPIN_EXECUTABLE_FILENAME + ' ' + SPIN_VERIFICATION_FLAG + ' ' + PROMELA_SOURCE_FILENAME + ' \\n ' +
        GCC_EXECUTABLE_FILENAME + ' ' + gcc_flags + ' ' + GCC_DISABLE_WARNINGS_FLAG + ' ' + GCC_OUTPUT_FLAG + ' ' + INTERIM_EXECUTABLE_FILENAME + ' ' +
        INTERIM_SOURCE_FILENAME + ' \\n ' + './' + INTERIM_EXECUTABLE_FILENAME + ' ' + spin_flags)

    # Create new unique directory for session and prepare all files for execute.
    errcode, path = prepare_executable(source_code, ip_address)
    if errcode != 0x0:
        return errcode, None

    # TODO Try to check spin and gcc flags by optparse.OptionParser

    # Create interim source code.
    commands = ['./' + SPIN_EXECUTABLE_FILENAME, SPIN_VERIFICATION_FLAG, PROMELA_SOURCE_FILENAME]
    errcode, result = execute_commands(path, commands, False, ip_address)
    if errcode != 0x0:
        return errcode, None

    # Create interim executable.
    commands = gcc_flags.split()
    commands.insert(0, GCC_EXECUTABLE_FILENAME)
    commands.append(GCC_DISABLE_WARNINGS_FLAG)
    commands.append(GCC_OUTPUT_FLAG)
    commands.append(INTERIM_EXECUTABLE_FILENAME)
    commands.append(INTERIM_SOURCE_FILENAME)
    errcode, result = execute_commands(path, commands, False, ip_address)
    if errcode != 0x0:
        return errcode, None

    # Execute interim executable with special flags.
    commands = spin_flags.split()
    commands.insert(0, './' + INTERIM_EXECUTABLE_FILENAME)
    errcode, result = execute_commands(path, commands, True, ip_address)
    if errcode != 0x0:
        return errcode, None

    return 0x0, result


def simulation_request(source_code, spin_flags, ip_address='ANONYMOUS'):
    """
        Simulates promela source code with special flags.

        :param source_code: Promela source code.
        :type source_code: str or unicode.

        :param spin_flags: Flags for spin executable.
        :type spin_flags: str or unicode.

        :param ip_address: Remote ip address. Default value is 'ANONYMOUS'.
        :type ip_address: str or unicode.

        :return error_code, result

        :author Boyarkin Nikita
    """

    log(LogFile.SPIN, logging.DEBUG, ip_address + ': ' + SPIN_EXECUTABLE_FILENAME + ' ' + spin_flags + ' ' + PROMELA_SOURCE_FILENAME)

    # Create new unique directory for session and prepare all files for execute.
    errcode, path = prepare_executable(source_code, ip_address)
    if errcode != 0x0:
        return errcode, None

    # TODO Try to check spin flags by optparse.OptionParser

    # Prepare command list.
    commands = spin_flags.split()
    commands.insert(0, './' + SPIN_EXECUTABLE_FILENAME)
    commands.append(PROMELA_SOURCE_FILENAME)

    return execute_commands(path, commands, True, ip_address)


def prepare_executable(source_code, ip_address='ANONYMOUS'):
    """
        Creates new unique directory for session and prepares all files for execute.
        
        :param source_code: Promela source code.
        :type source_code: str or unicode.
        
        :param ip_address: Remote ip address. Default value is 'ANONYMOUS'. 
        :type ip_address: str or unicode.
        
        :return error_code, path
        
        :author Boyarkin Nikita
    """

    if is_linux():
        condition = True
        path = None

        # Generate directory name.
        while condition:
            directory = str(uuid.uuid4())
            path = DYNAMIC_PATH + directory + '/'
            condition = os.path.exists(path)

        # Try to make directory.
        try:
            os.mkdir(path, LINUX_PERMISSIONS_MODE)
        except OSError:
            log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'OSError in function mkdir.')
            return 0x3, None
        except NotImplementedError:
            log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'NotImplementedError in function mkdir.')
            return 0x4, None

        log(LogFile.SPIN, logging.DEBUG, ip_address + ': ' + 'Directory \'' + path + '\' has been created.')

        # Try to create promela source file.
        try:
            source = open(path + PROMELA_SOURCE_FILENAME, 'w')
            source.write(source_code)
            source.close()
        except IOError:
            log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'IOError in function open.')
            rmtree(path)
            return 0x5, None

        promela = path + PROMELA_SOURCE_FILENAME
        os.chmod(promela, LINUX_PERMISSIONS_MODE)
        os.chown(promela, os.getuid(), os.getgid())

        log(LogFile.SPIN, logging.DEBUG, ip_address + ': ' + 'File \'' + promela + '\' has been created.')

        # Try to copy executable spin file into obtained path.
        try:
            copyfile(SPIN_EXECUTABLE_PATH, path + SPIN_EXECUTABLE_FILENAME)
        except Error:
            log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'Error in function copyfile.')
            rmtree(path)
            return 0x6, None
        except IOError:
            log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'IOError in function copyfile.')
            rmtree(path)
            return 0x7, None

        spin = path + SPIN_EXECUTABLE_FILENAME
        os.chmod(spin, LINUX_PERMISSIONS_MODE)
        os.chown(spin, os.getuid(), os.getgid())

        log(LogFile.SPIN, logging.DEBUG, ip_address + ': ' + 'File \'' + spin + '\' has been created.')
        return 0x0, path

    return 0xC, None


def execute_commands(path, commands, release_on_success, ip_address='ANONYMOUS'):
    """
        Executes commands and returns result of operation.
        
        :param path: Path to execution directory.
        :type path: str or unicode.
        
        :param commands: Array of flags and other commands.
        :type commands: str array.
        
        :param release_on_success: True, if path directory must be deleted on success.
        :type release_on_success: bool.
        
        :param ip_address: Remote ip address. Default value is 'ANONYMOUS'. 
        :type ip_address: str or unicode.
        
        :return error_code, result
        
        :author Boyarkin Nikita
    """

    relatively_commands = commands[:]

    # Use relatively executor, cuz impossible to use os.chdir(path) in this instance.
    relatively_commands.insert(0, path)
    relatively_commands.insert(0, RELATIVELY_EXECUTOR_PATH)
    relatively_commands.insert(0, sys.executable)

    # Try to execute commands.
    try:
        result = str(check_output(relatively_commands, shell=False, timeout=SUBPROCESS_TIMEOUT, universal_newlines=True))
        if release_on_success:
            rmtree(path)
        log(LogFile.SPIN, logging.DEBUG, ip_address + ': ' + 'Process has been successfully finished.')
        return 0x0, result
    except FileNotFoundError:
        log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'FileNotFoundError in function check_output.')
        rmtree(path)
        return 0x8, None
    except PermissionError:
        log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'PermissionError in function check_output.')
        rmtree(path)
        return 0x9, None
    except CalledProcessError as normal_exception:
        log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'CalledProcessError in function check_output.')
        rmtree(path)
        return normal_exception.returncode, None
    except TimeoutExpired:
        log(LogFile.SPIN, logging.ERROR, ip_address + ': ' + 'TimeoutExpired in function check_output.')
        rmtree(path)
        return 0xB, None


def flags_splitter(command_line, verification_mode):
    """
        Returns gcc and spin flags from command line.
        
        :param command_line: Command line string value.
        :type command_line: str or unicode.
        
        :param verification_mode: True in verification mode, false in simulation mode.
        :type verification_mode: bool.
              
        :return gcc_flags, spin_flags
         
        :raise ValueError - If impossible to get gcc or spin flags.
        
        :author Boyarkin Nikita
    """

    analyzable = command_line.replace('\r', '').replace('\n', ' ').replace('\t', ' ')

    if verification_mode:
        result = re.split(SPIN_EXECUTABLE_FILENAME + ' ' + SPIN_VERIFICATION_FLAG + ' ' + PROMELA_SOURCE_FILENAME + ' ' +
                          GCC_EXECUTABLE_FILENAME + ' ', analyzable)

        if len(result) != 2 or len(result[0]) != 0:
            raise ValueError

        result = re.split(' ' + GCC_DISABLE_WARNINGS_FLAG + ' ' + GCC_OUTPUT_FLAG + ' ' + INTERIM_EXECUTABLE_FILENAME + ' ' +
                          INTERIM_SOURCE_FILENAME + ' \./' + INTERIM_EXECUTABLE_FILENAME, result[1])

        if len(result) != 2:
            raise ValueError

        return result[0], result[1]

    else:
        if not analyzable.startswith(SPIN_EXECUTABLE_FILENAME) or not analyzable.endswith(PROMELA_SOURCE_FILENAME):
            raise ValueError

        result = analyzable[len(SPIN_EXECUTABLE_FILENAME):-len(PROMELA_SOURCE_FILENAME)]

        if len(result) == 0:
            raise ValueError

        return None, result
