"""
Context definition for usage in test.html template.
"""
from ModelChecking.scripts.authentication.authentication import authentication_request_handler
from ModelChecking.scripts.util.util import *


def test_request_handler(request):
    log(LogFile.TEST, logging.DEBUG, request.environ['REMOTE_ADDR'] + ': ' + request.method + ' \'' + request.path + '\'')

    context = {}
    authentication_request_handler(context, request)
    return context
