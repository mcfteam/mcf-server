"""
Context definition for usage in student.html template.
"""
from ModelChecking.models import UserType
from ModelChecking.scripts.authentication.authentication import get_user_attempt
from ModelChecking.scripts.database.exceptions import NotExistsException, LogicException
from ModelChecking.scripts.database.get import get_student_information
from ModelChecking.scripts.util.util import *


def student_request_handler(context, request):
    log(LogFile.STUDENT, logging.DEBUG, request.environ['REMOTE_ADDR'] + ': ' + request.method + ' \'' + request.path + '\'')

    context['cabinetErrorCode'] = 0x6

    if request.method == 'GET':
        viewer = get_user_attempt(request)
        if viewer is None:
            context['cabinetErrorCode'] = 0x2
            return

        viewer_id = viewer.user_id

        if 'id' not in request.GET:
            if viewer.user_type == UserType.STUDENT:
                student_id = viewer_id
            else:
                context['cabinetErrorCode'] = 0x1
                return
        else:
            try:
                student_id = int(str(request.GET['id']).strip())
            except ValueError:
                context['cabinetErrorCode'] = 0x3
                return

            if student_id is None or student_id < 0:
                context['cabinetErrorCode'] = 0x4
                return

        try:
            student_info, projects_info, groups_info = get_student_information(viewer_id, student_id)
        except NotExistsException:
            context['cabinetErrorCode'] = 0x5
            return
        except LogicException:
            context['cabinetErrorCode'] = 0x5
            return

        context['cabinetErrorCode'] = 0x0

        if student_info is not None:
            context['studentInformation'] = student_info

        if projects_info is not None:
            context['projectsInformation'] = projects_info

        if groups_info is not None:
            context['groupsInformation'] = groups_info
