"""
Context definition for usage in group.html template.
"""
from ModelChecking.scripts.authentication.authentication import get_user_attempt
from ModelChecking.scripts.database.exceptions import NotExistsException
from ModelChecking.scripts.database.get import get_group_information
from ModelChecking.scripts.util.util import *


def group_request_handler(context, request):
    log(LogFile.GROUP, logging.DEBUG, request.environ['REMOTE_ADDR'] + ': ' + request.method + ' \'' + request.path + '\'')

    context['cabinetErrorCode'] = 0x6

    if request.method == 'GET':
        if 'id' not in request.GET:
            context['cabinetErrorCode'] = 0x1
            return

        viewer = get_user_attempt(request)
        if viewer is None:
            context['cabinetErrorCode'] = 0x2
            return

        viewer_id = viewer.user_id

        try:
            group_id = int(str(request.GET['id']).strip())
        except ValueError:
            context['cabinetErrorCode'] = 0x3
            return

        if group_id is None or group_id < 0:
            context['cabinetErrorCode'] = 0x4
            return

        try:
            group_info, curators_info, members_info = get_group_information(viewer_id, group_id)
        except NotExistsException:
            context['cabinetErrorCode'] = 0x5
            return

        context['cabinetErrorCode'] = 0x0

        if group_info is not None:
            context['groupInformation'] = group_info

        if members_info is not None:
            context['membersInformation'] = members_info

        if curators_info is not None:
            context['curatorsInformation'] = curators_info
