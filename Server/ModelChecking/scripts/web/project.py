"""
Context definition for usage in project.html template.
"""
from ModelChecking.scripts.authentication.authentication import get_user_attempt
from ModelChecking.scripts.database.exceptions import NotExistsException
from ModelChecking.scripts.database.get import get_project_information
from ModelChecking.scripts.util.util import *


def project_request_handler(context, request):
    log(LogFile.PROJECT, logging.DEBUG, request.environ['REMOTE_ADDR'] + ': ' + request.method + ' \'' + request.path + '\'')

    context['cabinetErrorCode'] = 0x6

    if request.method == 'GET':
        if 'id' not in request.GET:
            context['cabinetErrorCode'] = 0x1
            return

        viewer = get_user_attempt(request)
        if viewer is None:
            context['cabinetErrorCode'] = 0x2
            return

        viewer_id = viewer.user_id

        try:
            project_id = int(str(request.GET['id']).strip())
        except ValueError:
            context['cabinetErrorCode'] = 0x3
            return

        if project_id is None or project_id < 0:
            context['cabinetErrorCode'] = 0x4
            return

        try:
            project_info, files_info = get_project_information(viewer_id, project_id)
        except NotExistsException:
            context['cabinetErrorCode'] = 0x5
            return

        context['cabinetErrorCode'] = 0x0

        if project_info is not None:
            context['projectInformation'] = project_info

        if files_info is not None:
            context['filesInformation'] = files_info
