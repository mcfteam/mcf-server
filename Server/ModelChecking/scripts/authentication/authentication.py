from django.contrib.auth import authenticate, login, logout

from ModelChecking.models import UserType
from ModelChecking.scripts.database.add import add_student, add_teacher
from ModelChecking.scripts.database.exceptions import *


def login_attempt(request, email, password):
    """
        TODO

        :param request: 
        :type request:

        :param email: 
        :type email:

        :param password: 
        :type password:

        :return: 

        :author Boyarkin Nikita
    """

    user = authenticate(username=email, password=password)
    if user is not None and user.is_active:
        login(request, user)
        return user

    return None


def logout_attempt(request):
    """
        TODO

        :param request: 
        :type request:

        :author Boyarkin Nikita
    """

    logout(request)


def get_user_attempt(request):
    """
        TODO

        :param request: 
        :type request:
        
        :return: 

        :author Boyarkin Nikita
    """

    if is_authenticated(request):
        return request.user

    return None


def is_authenticated(request):
    """
        TODO

        :param request: 
        :type request:
        
        :return: 

        :author Boyarkin Nikita
    """

    return request.user.is_authenticated()


def authentication_request_handler(context, request):
    """
        TODO

        :param context: 
        :type context:
        
        :param request: 
        :type request:
        
        :return:

        :author Boyarkin Nikita
    """

    if request.method == 'POST':
        if 'register-first-name' in request.POST and 'register-last-name' in request.POST and 'register-email' in request.POST and \
                        'register-password' in request.POST and 'register-teacher-position' in request.POST:
            email = str(request.POST['register-email'])
            password = str(request.POST['register-password'])
            first_name = str(request.POST['register-first-name'])
            last_name = str(request.POST['register-last-name'])
            user_type = UserType.TEACHER if 'register-is-teacher' in request.POST else UserType.STUDENT
            position = str(request.POST['register-teacher-position'])
            return register_user(context, request, email, password, user_type, first_name, last_name, position)

        if 'login-email' in request.POST and 'login-password' in request.POST:
            email = str(request.POST['login-email'])
            password = str(request.POST['login-password'])
            return login_user(context, request, email, password)

        if 'logout' in request.POST:
            return logout_user(context, request)

    if is_authenticated(request):
        user = request.user
        if user is not None:
            context['userErrorCode'] = 0x0
            context['userType'] = 'student' if user.user_type == UserType.STUDENT else 'teacher'
            context['userEmail'] = user.email
            return False
        else:
            context['userErrorCode'] = 0x9
            return False

    context['userErrorCode'] = 0x0
    return False


def register_user(context, request, email, password, user_type, first_name, last_name, position):
    """
        TODO

        :param context: 
        :type context:

        :param request: 
        :type request:
        
        :param email: 
        :type email:

        :param password: 
        :type password:

        :param user_type: 
        :type user_type:

        :param first_name: 
        :type first_name:

        :param last_name: 
        :type last_name:
        
        :param position: 
        :type position:
        
        :return: 

        :author Boyarkin Nikita
    """

    try:
        if user_type == UserType.STUDENT:
            add_student(email, password, first_name, last_name)
        else:
            add_teacher(email, password, first_name, last_name, position)
    except InstanceException:
        context['userErrorCode'] = 0x1
        return False
    except WrongTypeException:
        context['userErrorCode'] = 0x2
        return False
    except EmptyException:
        context['userErrorCode'] = 0x3
        return False
    except LimitException:
        context['userErrorCode'] = 0x4
        return False
    except ValidationException:
        context['userErrorCode'] = 0x5
        return False
    except AlreadyExistsException:
        context['userErrorCode'] = 0x6
        return False
    except UnknownException:
        context['userErrorCode'] = 0x7
        return False

    return login_user(context, request, email, password)


def login_user(context, request, email, password):
    """
        TODO

        :param context: 
        :type context:

        :param request: 
        :type request:

        :param email: 
        :type email:

        :param password: 
        :type password:
        
        :return:

        :author Boyarkin Nikita
    """

    if is_authenticated(request):
        logout_attempt(request)

    user = login_attempt(request, email, password)
    if user is not None:
        context['userErrorCode'] = 0x0
        context['userType'] = 'student' if user.user_type == UserType.STUDENT else 'teacher'
        context['userEmail'] = email
        return True
    else:
        context['userErrorCode'] = 0x8
        return False


def logout_user(context, request):
    """
        TODO

        :param context: 
        :type context:

        :param request: 
        :type request:
        
        :return: 

        :author Boyarkin Nikita
    """

    if is_authenticated(request):
        logout_attempt(request)

    context['userErrorCode'] = 0x0
    return True
