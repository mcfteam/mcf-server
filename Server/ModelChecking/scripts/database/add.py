from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.validators import validate_email
from django.db import DatabaseError

from ModelChecking.models import *
from ModelChecking.scripts.database.exceptions import *


def add_student(email, password, first_name, last_name):
    """
        TODO
        
        :param email: 
        :type email:
        
        :param password: 
        :type password:
        
        :param first_name: 
        :type first_name:
        
        :param last_name: 
        :type last_name:
        
        :raises:
        
        :return: 
        
        :author Boyarkin Nikita
    """

    return add_user(email=email, password=password, user_type=UserType.STUDENT, first_name=first_name, last_name=last_name, position='Student')


def add_teacher(email, password, first_name, last_name, position):
    """
        TODO

        :param email: 
        :type email:

        :param password: 
        :type password:

        :param first_name: 
        :type first_name:

        :param last_name: 
        :type last_name:
        
        :param position: 
        :type position:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    return add_user(email=email, password=password, user_type=UserType.TEACHER, first_name=first_name, last_name=last_name, position=position)


def add_user(email, password, user_type, first_name, last_name, position):
    """
        TODO

        :param email: 
        :type email:

        :param password: 
        :type password:

        :param user_type: 
        :type user_type:

        :param first_name: 
        :type first_name:

        :param last_name: 
        :type last_name:
        
        :param position: 
        :type position:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    if not isinstance(email, str) or not isinstance(password, str) or not isinstance(user_type, int) or not isinstance(first_name, str) or \
            not isinstance(last_name, str) or not isinstance(position, str):
        raise InstanceException()

    if not user_type == UserType.STUDENT and not user_type == UserType.TEACHER:
        raise WrongTypeException()

    if not email or not password or not first_name or not last_name or not position:
        raise EmptyException()

    if len(email) > EMAIL_LENGTH or len(password) > PASSWORD_LENGTH or len(first_name) > NAME_LENGTH or \
                    len(last_name) > NAME_LENGTH or len(position) > POSITION_LENGTH:
        raise LimitException()

    try:
        validate_email(email)
    except ValidationError:
        raise ValidationException()

    if User.objects.filter(email=email).exists():
        raise AlreadyExistsException()

    try:
        user = User(email=email, password=password, user_type=user_type, first_name=first_name, last_name=last_name, position=position)
        user.save()
        return user.user_id
    except DatabaseError:
        raise UnknownException()


def add_group(owner_id, title):
    """
        TODO

        :param owner_id: 
        :type owner_id:

        :param title: 
        :type title:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    if not isinstance(owner_id, int) or not isinstance(title, str):
        raise InstanceException()

    if not title:
        raise EmptyException()

    if len(title) > TITLE_LENGTH:
        raise LimitException()

    try:
        teacher = User.objects.get(user_id=owner_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if teacher.user_type != UserType.TEACHER:
        raise WrongTypeException()

    try:
        group = Group(title=title)
        group.save()
    except DatabaseError:
        raise UnknownException()

    try:
        curator = Curator(user_id=teacher, group_id=group, is_owner=True, is_notification=False)
        curator.save()
        return group.group_id, curator.curator_id
    except DatabaseError:
        group.delete()
        raise UnknownException()


def add_member(group_id, student_id):
    """
        TODO
        
        :param group_id: 
        :type group_id:

        :param student_id: 
        :type student_id:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    if not isinstance(student_id, int) or not isinstance(group_id, int):
        raise InstanceException()

    try:
        student = User.objects.get(user_id=student_id)
        group = Group.objects.get(group_id=group_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if student.user_type != UserType.STUDENT:
        raise WrongTypeException()

    if Member.objects.filter(user_id=student, group_id=group).exists():
        raise AlreadyExistsException()

    try:
        member = Member(user_id=student, group_id=group, is_notification=True)
        member.save()
        return member.member_id
    except DatabaseError:
        raise UnknownException()


def add_curator(group_id, teacher_id):
    """
        TODO

        :param group_id: 
        :type group_id:

        :param teacher_id: 
        :type teacher_id:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    if not isinstance(teacher_id, int) or not isinstance(group_id, int):
        raise InstanceException()

    try:
        teacher = User.objects.get(user_id=teacher_id)
        group = Group.objects.get(group_id=group_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if teacher.user_type != UserType.TEACHER:
        raise WrongTypeException()

    if Curator.objects.filter(user_id=teacher, group_id=group).exists():
        raise AlreadyExistsException()

    try:
        curator = Curator(user_id=teacher, group_id=group, is_owner=False, is_notification=True)
        curator.save()
        return curator.curator_id
    except DatabaseError:
        raise UnknownException()


def add_project(owner_id, title):
    """
        TODO

        :param owner_id: 
        :type owner_id:

        :param title: 
        :type title:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """
    if not isinstance(owner_id, int) or not isinstance(title, str):
        raise InstanceException()

    if not title:
        raise EmptyException()

    if len(title) > TITLE_LENGTH:
        raise LimitException()

    try:
        owner = User.objects.get(user_id=owner_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    try:
        project = Project(user_id=owner, title=title)
        project.save()
        return project.project_id
    except DatabaseError:
        raise UnknownException()


def add_file(project_id, name, extension, byte_array):
    """
        TODO

        :param project_id: 
        :type project_id:

        :param name: 
        :type name:
        
        :param extension: 
        :type extension:
        
        :param byte_array: 
        :type byte_array:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """
    if not isinstance(project_id, int) or not isinstance(name, str) or not isinstance(extension, str) or not isinstance(byte_array, bytes):
        raise InstanceException()

    if not name or not extension:
        raise EmptyException()

    if len(name) > FILENAME_LENGTH or len(extension) > EXTENSION_LENGTH:
        raise LimitException()

    try:
        project = Project.objects.get(project_id=project_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    file = File.objects.filter(project_id=project, name=name, extension=extension)
    if file.exists():
        raise AlreadyExistsException()

    try:
        file = File(project_id=project, name=name, extension=extension, byte_array=byte_array)
        file.save()
        return file.file_id
    except DatabaseError:
        raise UnknownException()
