from django.core.exceptions import ObjectDoesNotExist
from django.db import DatabaseError

from ModelChecking.models import *
from ModelChecking.scripts.database.exceptions import *


def remove_student(student_id):
    """
        TODO

        :param student_id: 
        :type student_id:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(student_id, int):
        raise InstanceException()

    try:
        student = User.objects.get(user_id=student_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if not student.user_type == UserType.STUDENT:
        raise WrongTypeException()

    try:
        student.delete()
    except DatabaseError:
        raise UnknownException()


def remove_teacher(teacher_id):
    """
        TODO

        :param teacher_id: 
        :type teacher_id:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(teacher_id, int):
        raise InstanceException()

    try:
        teacher = User.objects.get(user_id=teacher_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if not teacher.user_type == UserType.TEACHER:
        raise WrongTypeException()

    owner_set = Curator.objects.filter(user_id=teacher, is_owner=True)

    group_set = None
    if owner_set.exists():
        group_set = []
        for own in owner_set:
            try:
                group = own.group_id
            except ObjectDoesNotExist:
                continue

            group_set.append(group)

    try:
        teacher.delete()
    except DatabaseError:
        raise UnknownException()

    if group_set is not None:
        try:
            for group in group_set:
                group.delete()
        except DatabaseError:
            raise UnknownException()


def remove_group(group_id):
    """
        TODO

        :param group_id: 
        :type group_id:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(group_id, int):
        raise InstanceException()

    try:
        Group.objects.get(group_id=group_id).delete()
    except ObjectDoesNotExist:
        raise NotExistsException()
    except DatabaseError:
        raise UnknownException()


def remove_member(group_id, student_id):
    """
        TODO

        :param group_id: 
        :type group_id:

        :param student_id: 
        :type student_id:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(student_id, int) or not isinstance(group_id, int):
        raise InstanceException()

    try:
        student = User.objects.get(user_id=student_id)
        group = Group.objects.get(group_id=group_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if student.user_type != UserType.STUDENT:
        raise WrongTypeException()

    try:
        Member.objects.get(user_id=student, group_id=group).delete()
    except ObjectDoesNotExist:
        raise NotExistsException()
    except DatabaseError:
        raise UnknownException()


def remove_curator(group_id, teacher_id):
    """
        TODO

        :param group_id: 
        :type group_id:

        :param teacher_id: 
        :type teacher_id:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(teacher_id, int) or not isinstance(group_id, int):
        raise InstanceException()

    try:
        teacher = User.objects.get(user_id=teacher_id)
        group = Group.objects.get(group_id=group_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if teacher.user_type != UserType.TEACHER:
        raise WrongTypeException()

    try:
        Curator.objects.get(user_id=teacher, group_id=group, is_owner=False).delete()
    except ObjectDoesNotExist:
        raise NotExistsException()
    except DatabaseError:
        raise UnknownException()


def remove_project(project_id):
    """
        TODO

        :param project_id: 
        :type project_id:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(project_id, int):
        raise InstanceException()

    try:
        Project.objects.get(project_id=project_id).delete()
    except ObjectDoesNotExist:
        raise NotExistsException()
    except DatabaseError:
        raise UnknownException()


def remove_file(file_id):
    """
        TODO

        :param file_id: 
        :type file_id:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(file_id, int):
        raise InstanceException()

    try:
        File.objects.get(file_id=file_id).delete()
    except ObjectDoesNotExist:
        raise NotExistsException()
    except DatabaseError:
        raise UnknownException()
