class DatabaseException(Exception):
    pass


class InstanceException(DatabaseException):
    pass


class EmptyException(DatabaseException):
    pass


class ValidationException(DatabaseException):
    pass


class LimitException(DatabaseException):
    pass


class NotExistsException(DatabaseException):
    pass


class AlreadyExistsException(DatabaseException):
    pass


class WrongTypeException(DatabaseException):
    pass


class LogicException(DatabaseException):
    pass


class UnknownException(DatabaseException):
    pass
