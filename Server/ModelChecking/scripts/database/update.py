from typing import overload

from django.core.exceptions import ObjectDoesNotExist
from django.db import DatabaseError

from ModelChecking.models import *
from ModelChecking.scripts.database.exceptions import *


def update_student(student_id, password=None, first_name=None, last_name=None):
    """
        TODO

        :param student_id: 
        :type student_id:

        :param password: 
        :type password:

        :param first_name: 
        :type first_name:

        :param last_name: 
        :type last_name:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    if not isinstance(student_id, int):
        raise InstanceException()

    try:
        student = User.objects.get(user_id=student_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if not student.user_type == UserType.STUDENT:
        raise WrongTypeException()

    return update_user(student, password=password, first_name=first_name, last_name=last_name)


def update_teacher(teacher_id, password=None, first_name=None, last_name=None, position=None):
    """
        TODO

        :param teacher_id: 
        :type teacher_id:

        :param password: 
        :type password:

        :param first_name: 
        :type first_name:

        :param last_name: 
        :type last_name:

        :param position: 
        :type position:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    if not isinstance(teacher_id, int):
        raise InstanceException()

    try:
        teacher = User.objects.get(user_id=teacher_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if not teacher.user_type == UserType.TEACHER:
        raise WrongTypeException()

    return update_user(teacher, password=password, first_name=first_name, last_name=last_name, position=position)


def update_user(user, password=None, first_name=None, last_name=None, position=None):
    """
        TODO

        :param user: 
        :type user:

        :param password: 
        :type password:

        :param first_name: 
        :type first_name:

        :param last_name: 
        :type last_name:

        :param position: 
        :type position:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    to_update = []

    if password is not None:
        if not isinstance(password, str):
            raise InstanceException()

        if not password:
            raise EmptyException()

        if len(password) > PASSWORD_LENGTH:
            raise LimitException()

        user.password = password
        to_update.append('password')

    if first_name is not None:
        if not isinstance(first_name, str):
            raise InstanceException()

        if not first_name:
            raise EmptyException()

        if len(first_name) > NAME_LENGTH:
            raise LimitException()

        user.first_name = first_name
        to_update.append('first_name')

    if last_name is not None:
        if not isinstance(last_name, str):
            raise InstanceException()

        if not last_name:
            raise EmptyException()

        if len(last_name) > NAME_LENGTH:
            raise LimitException()

        user.last_name = last_name
        to_update.append('last_name')

    if position is not None:
        if not isinstance(position, str):
            raise InstanceException()

        if not position:
            raise EmptyException()

        if len(position) > POSITION_LENGTH:
            raise LimitException()

        user.position = position
        to_update.append('position')

    try:
        user.save(update_fields=to_update)
        return not to_update

    except DatabaseError:
        raise UnknownException()


def update_group(group_id, title):
    """
        TODO

        :param group_id: 
        :type group_id:

        :param title: 
        :type title:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(group_id, int) or not isinstance(title, str):
        raise InstanceException()

    if not title:
        raise EmptyException()

    if len(title) > TITLE_LENGTH:
        raise LimitException()

    try:
        group = Group.objects.get(group_id=group_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    try:
        group.title = title
        group.save(update_fields=['title'])
    except DatabaseError:
        raise UnknownException()


def update_member(student_id, group_id, is_notification):
    """
        TODO

        :param student_id: 
        :type student_id:

        :param group_id: 
        :type group_id:

        :param is_notification:
        :type is_notification:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(student_id, int) or not isinstance(group_id, int) or not isinstance(is_notification, bool):
        raise InstanceException()

    if is_notification:
        raise LogicException()

    try:
        student = User.objects.get(user_id=student_id)
        group = Group.objects.get(group_id=group_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if not student.user_type == UserType.STUDENT:
        raise WrongTypeException()

    try:
        member = Member.objects.get(user_id=student, group_id=group)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if not member.is_notification:
        raise LogicException()

    try:
        member.is_notification = is_notification
        member.save(update_fields=['is_notification'])
    except DatabaseError:
        raise UnknownException()


def update_curator(teacher_id, group_id, is_notification):
    """
        TODO

        :param teacher_id: 
        :type teacher_id:

        :param group_id: 
        :type group_id:

        :param is_notification:
        :type is_notification:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(teacher_id, int) or not isinstance(group_id, int) or not isinstance(is_notification, bool):
        raise InstanceException()

    if is_notification:
        raise LogicException()

    try:
        teacher = User.objects.get(user_id=teacher_id)
        group = Group.objects.get(group_id=group_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if not teacher.user_type == UserType.TEACHER:
        raise WrongTypeException()

    try:
        curator = Curator.objects.get(user_id=teacher, group_id=group)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if not curator.is_notification:
        raise LogicException()

    try:
        curator.is_notification = is_notification
        curator.save(update_fields=['is_notification'])
    except DatabaseError:
        raise UnknownException()


def update_project(project_id, title):
    """
        TODO

        :param project_id: 
        :type project_id:

        :param title: 
        :type title:

        :raises:

        :author Boyarkin Nikita
    """

    if not isinstance(project_id, int) or not isinstance(title, str):
        raise InstanceException()

    if not title:
        raise EmptyException()

    if len(title) > TITLE_LENGTH:
        raise LimitException()

    try:
        project = Project.objects.get(project_id=project_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    try:
        project.title = title
        project.save(update_fields=['title'])
    except DatabaseError:
        raise UnknownException()


def update_file(file_id, name=None, extension=None, byte_array=None):
    """
        TODO

        :param file_id: 
        :type file_id:

        :param name: 
        :type name:

        :param extension: 
        :type extension:
        
        :param byte_array: 
        :type byte_array:

        :raises:
        
        :return

        :author Boyarkin Nikita
    """

    if not isinstance(file_id, int):
        raise InstanceException()

    try:
        file = File.objects.get(file_id=file_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    to_update = []

    if name is not None:
        if not isinstance(name, str):
            raise InstanceException()

        if not name:
            raise EmptyException()

        if len(name) > FILENAME_LENGTH:
            raise LimitException()

        file.name = name
        to_update.append('name')

    if extension is not None:
        if not isinstance(extension, str):
            raise InstanceException()

        if not extension:
            raise EmptyException()

        if len(extension) > EXTENSION_LENGTH:
            raise LimitException()

        file.extension = extension
        to_update.append('extension')

    if byte_array is not None:
        if not isinstance(byte_array, bytes):
            raise InstanceException()

        file.byte_array = byte_array
        to_update.append('byte_array')

    try:
        if to_update:
            to_update.append('modified_time')
            file.save(update_fields=to_update)
            return True

        return False

    except DatabaseError:
        raise UnknownException()
