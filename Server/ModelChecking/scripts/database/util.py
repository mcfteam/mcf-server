from django.core.exceptions import ObjectDoesNotExist

from ModelChecking.models import *
from ModelChecking.scripts.database.exceptions import *


def find_file_id(project_id, name, extension):
    """
        TODO

        :param project_id: 
        :type project_id:

        :param name: 
        :type name:

        :param extension: 
        :type extension:

        :raises:

        :return: 

        :author Boyarkin Nikita
    """

    if not isinstance(project_id, int) or not isinstance(name, str) or not isinstance(extension, str):
        raise InstanceException()

    if not name or not extension:
        raise EmptyException()

    if len(name) > FILENAME_LENGTH or len(extension) > EXTENSION_LENGTH:
        raise LimitException()

    try:
        project = Project.objects.get(project_id=project_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    file = File.objects.filter(project_id=project, name=name, extension=extension)
    if not file.exists():
        raise NotExistsException()

    return file[0].file_id
