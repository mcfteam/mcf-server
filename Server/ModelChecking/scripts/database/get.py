from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist

from ModelChecking.models import *
from ModelChecking.scripts.database.exceptions import *

JSON_FORMAT_KEY = 'json'


def get_student_information(viewer_id, student_id):
    """
        TODO

        :param viewer_id: 
        :type viewer_id:

        :param student_id: 
        :type student_id:

        :raises:
        
        :return:

        :author Boyarkin Nikita
    """

    if not isinstance(viewer_id, int) or not isinstance(student_id, int):
        raise InstanceException()

    try:
        viewer = User.objects.get(user_id=viewer_id)
        student = User.objects.get(user_id=student_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if student.user_type != UserType.STUDENT:
        raise LogicException()

    student_serializable = serializers.serialize(JSON_FORMAT_KEY, [student])
    return student_serializable, get_user_projects(student), get_user_groups(student)


def get_teacher_information(viewer_id, teacher_id):
    """
        TODO

        :param viewer_id: 
        :type viewer_id:

        :param teacher_id: 
        :type teacher_id:

        :raises:

        :return:

        :author Boyarkin Nikita
    """

    if not isinstance(viewer_id, int) or not isinstance(teacher_id, int):
        raise InstanceException()

    try:
        viewer = User.objects.get(user_id=viewer_id)
        teacher = User.objects.get(user_id=teacher_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    if teacher.user_type != UserType.TEACHER:
        raise LogicException()

    teacher_serializable = serializers.serialize(JSON_FORMAT_KEY, [teacher])
    return teacher_serializable, get_user_projects(teacher), get_user_groups(teacher)


def get_user_projects(user):
    """
        TODO

        :param user:
        :type user:

        :return:

        :author Boyarkin Nikita
    """

    project_set = Project.objects.filter(user_id=user)
    if project_set.exists():
        result = []
        for project in project_set:
            result.append(project)

        return serializers.serialize(JSON_FORMAT_KEY, result)

    else:
        return serializers.serialize(JSON_FORMAT_KEY, [])


def get_user_groups(user):
    """
        TODO

        :param user: 
        :type user:

        :return:

        :author Boyarkin Nikita
    """

    if user.user_type == UserType.STUDENT:
        member_set = Member.objects.filter(user_id=user, is_notification=False)
        if not member_set.exists():
            return serializers.serialize(JSON_FORMAT_KEY, [])

        result = []
        for member in member_set:
            result.append(member.group_id)

        return serializers.serialize(JSON_FORMAT_KEY, result)

    else:
        curator_set = Curator.objects.filter(user_id=user, is_notification=False)
        if not curator_set.exists():
            return serializers.serialize(JSON_FORMAT_KEY, [])

        result = []
        for curator in curator_set:
            result.append(curator.group_id)

        return serializers.serialize(JSON_FORMAT_KEY, result)


def get_project_information(viewer_id, project_id):
    """
        TODO

        :param viewer_id: 
        :type viewer_id:

        :param project_id: 
        :type project_id:

        :raises:

        :return:

        :author Boyarkin Nikita
    """

    if not isinstance(viewer_id, int) or not isinstance(project_id, int):
        raise InstanceException()

    try:
        viewer = User.objects.get(user_id=viewer_id)
        project = Project.objects.get(project_id=project_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    project_serializable = serializers.serialize(JSON_FORMAT_KEY, [project])

    if viewer.user_id == project.user_id.user_id:
        return project_serializable, get_project_files(project)
    elif viewer.user_type == UserType.TEACHER:
        if project.user_id.user_type == UserType.TEACHER:
            return project_serializable, None

        student = project.user_id

        curator_set = Curator.objects.filter(user_id=viewer, is_notification=False)
        if not curator_set.exists():
            return project_serializable, None

        member_set = Member.objects.filter(user_id=student, is_notification=False)
        if not member_set.exists():
            return project_serializable, None

        for curator in curator_set:
            for member in member_set:
                if curator.group_id.group_id == member.group_id.group_id:
                    return project_serializable, get_project_files(project)

        return project_serializable, None

    elif viewer.user_type == UserType.STUDENT:
        return project_serializable, None


def get_project_files(project):
    """
        TODO

        :param project: 
        :type project:

        :return:

        :author Boyarkin Nikita
    """

    file_set = File.objects.filter(project_id=project)
    if file_set.exists():
        result = []
        for file in file_set:
            result.append(file)

        return serializers.serialize(JSON_FORMAT_KEY, result)

    else:
        return serializers.serialize(JSON_FORMAT_KEY, [])


def get_group_information(viewer_id, group_id):
    """
        TODO

        :param viewer_id: 
        :type viewer_id:

        :param group_id: 
        :type group_id:

        :raises:

        :return:

        :author Boyarkin Nikita
    """

    if not isinstance(viewer_id, int) or not isinstance(group_id, int):
        raise InstanceException()

    try:
        viewer = User.objects.get(user_id=viewer_id)
        group = Group.objects.get(group_id=group_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    group_serializable = serializers.serialize(JSON_FORMAT_KEY, [group])

    curator_set = Curator.objects.filter(group_id=group, is_notification=False)
    curator_result = []
    if curator_set.exists():
        for curator in curator_set:
            curator_result.append(curator.user_id)

    member_set = Member.objects.filter(group_id=group, is_notification=False)
    member_result = []
    if member_set.exists():
        for member in member_set:
            member_result.append(member.user_id)

    curator_serializable = serializers.serialize(JSON_FORMAT_KEY, curator_result)
    member_serializable = serializers.serialize(JSON_FORMAT_KEY, member_result)

    return group_serializable, curator_serializable, member_serializable


def get_complete_projects_information(viewer_id):
    """
        TODO

        :param viewer_id:
        :type viewer_id:

        :return:

        :author Boyarkin Nikita
    """

    if not isinstance(viewer_id, int):
        raise InstanceException()

    try:
        viewer = User.objects.get(user_id=viewer_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    project_set = Project.objects.filter(user_id=viewer)
    if project_set.exists():
        result = []
        for project in project_set:
            result.append(project)

            files_set = File.objects.filter(project_id=project)
            if files_set.exists():
                for file in files_set:
                    result.append(file)

        return serializers.serialize(JSON_FORMAT_KEY, result)
    else:
        return serializers.serialize(JSON_FORMAT_KEY, [])


def get_file_information(viewer_id, file_id):
    """
        TODO

        :param viewer_id: 
        :type viewer_id:

        :param file_id: 
        :type file_id:

        :raises:

        :return:

        :author Boyarkin Nikita
    """

    if not isinstance(viewer_id, int) or not isinstance(file_id, int):
        raise InstanceException()

    try:
        viewer = User.objects.get(user_id=viewer_id)
        file = File.objects.get(file_id=file_id)
    except ObjectDoesNotExist:
        raise NotExistsException()

    project = file.project_id

    if viewer.user_id == project.user_id.user_id:
        return file.name, file.extension, file.byte_array
    elif viewer.user_type == UserType.TEACHER:
        if project.user_id.user_type == UserType.TEACHER:
            return None, None, None

        student = project.user_id

        curator_set = Curator.objects.filter(user_id=viewer, is_notification=False)
        if not curator_set.exists():
            return None, None, None

        member_set = Member.objects.filter(user_id=student, is_notification=False)
        if not member_set.exists():
            return None, None, None

        for curator in curator_set:
            for member in member_set:
                if curator.group_id.group_id == member.group_id.group_id:
                    return file.name, file.extension, file.byte_array

        return None, None, None

    elif viewer.user_type == UserType.STUDENT:
        return None, None, None
