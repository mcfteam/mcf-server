import django
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import ugettext_lazy as _

from ModelChecking.managers import UserManager


class UserType:
    SUPERUSER = 0
    STUDENT = 1
    TEACHER = 2


FILENAME_LENGTH = 511
EMAIL_LENGTH = 255
PASSWORD_LENGTH = 127
EXTENSION_LENGTH = 255
NAME_LENGTH = 127
TITLE_LENGTH = 127
POSITION_LENGTH = 127


class User(AbstractBaseUser, PermissionsMixin):
    user_id = models.BigAutoField(db_column='user_id', primary_key=True, unique=True, null=False, serialize=True)
    groups = models.ManyToManyField(django.contrib.auth.models.Group, verbose_name=_('groups'), unique=False, blank=True,
                                    related_name='user_set', related_query_name='user', serialize=False)
    user_permissions = models.ManyToManyField(django.contrib.auth.models.Permission, verbose_name=_('user permissions'), unique=False, blank=True,
                                              related_name='user_set', related_query_name='user', serialize=False)
    email = models.EmailField(db_column='email', max_length=EMAIL_LENGTH, unique=True, null=False, serialize=True)
    password = models.CharField(db_column='password', max_length=PASSWORD_LENGTH, unique=False, null=False, serialize=False)
    joined_time = models.DateTimeField(db_column='joined_time', auto_now_add=True, auto_now=False, unique=False, null=False, serialize=True)
    last_login = models.DateTimeField(db_column='last_login', auto_now_add=False, auto_now=False, unique=False, null=True, blank=True, serialize=False)
    is_superuser = models.BooleanField(db_column='is_superuser', default=False, unique=False, null=False, serialize=False)
    is_active = models.BooleanField(db_column='is_active', default=True, unique=False, null=False, serialize=False)
    is_staff = models.BooleanField(db_column='is_staff', default=False, unique=False, null=False, serialize=False)
    user_type = models.SmallIntegerField(db_column='user_type', default=UserType.SUPERUSER, unique=False, null=False, serialize=True)
    first_name = models.CharField(db_column='first_name', max_length=NAME_LENGTH, unique=False, null=False, blank=True, serialize=True)
    last_name = models.CharField(db_column='last_name', max_length=NAME_LENGTH, unique=False, null=False, blank=True, serialize=True)
    position = models.CharField(db_column='position', max_length=POSITION_LENGTH, unique=False, null=False, blank=True, serialize=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        db_table = 'user'

    def get_full_name(self):
        """
            Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
            Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
            Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)


class Group(models.Model):
    group_id = models.BigAutoField(db_column='group_id', primary_key=True, unique=True, null=False, serialize=True)
    title = models.CharField(db_column='title', max_length=TITLE_LENGTH, unique=False, null=False, serialize=True)

    class Meta:
        db_table = 'group'


class Curator(models.Model):
    curator_id = models.BigAutoField(db_column='curator_id', primary_key=True, unique=True, null=False, serialize=False)
    user_id = models.ForeignKey(User, db_column='user_id', unique=False, null=False, on_delete=models.CASCADE, serialize=False)
    group_id = models.ForeignKey(Group, db_column='group_id', unique=False, null=False, on_delete=models.CASCADE, serialize=False)
    is_owner = models.BooleanField(db_column='is_owner', unique=False, null=False, serialize=False)
    is_notification = models.BooleanField(db_column='is_notification', unique=False, null=False, serialize=False)

    class Meta:
        db_table = 'curator'


class Member(models.Model):
    member_id = models.BigAutoField(db_column='member_id', primary_key=True, unique=True, null=False, serialize=False)
    user_id = models.ForeignKey(User, db_column='user_id', unique=False, null=False, on_delete=models.CASCADE, serialize=False)
    group_id = models.ForeignKey(Group, db_column='group_id', unique=False, null=False, on_delete=models.CASCADE, serialize=False)
    is_notification = models.BooleanField(db_column='is_notification', unique=False, null=False, serialize=False)

    class Meta:
        db_table = 'member'


class Project(models.Model):
    project_id = models.BigAutoField(db_column='project_id', primary_key=True, unique=True, null=False, serialize=True)
    user_id = models.ForeignKey(User, db_column='user_id', unique=False, null=False, on_delete=models.CASCADE, serialize=False)
    title = models.CharField(db_column='title', max_length=TITLE_LENGTH, unique=False, null=False, serialize=True)

    class Meta:
        db_table = 'project'


class File(models.Model):
    file_id = models.BigAutoField(db_column='file_id', primary_key=True, unique=True, null=False, serialize=True)
    project_id = models.ForeignKey(Project, db_column='project_id', unique=False, null=False, on_delete=models.CASCADE, serialize=False)
    name = models.CharField(db_column='name', max_length=FILENAME_LENGTH, unique=False, null=False, serialize=True)
    extension = models.CharField(db_column='extension', max_length=EXTENSION_LENGTH, unique=False, null=False, serialize=True)
    created_time = models.DateTimeField(db_column='created_time', auto_now_add=True, auto_now=False, unique=False, null=False, serialize=True)
    modified_time = models.DateTimeField(db_column='modified_time', auto_now_add=False, auto_now=True, unique=False, null=False, serialize=True)
    byte_array = models.BinaryField(db_column='byte_array', unique=False, null=False, serialize=False)

    class Meta:
        db_table = 'file'
