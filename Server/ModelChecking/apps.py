from django.apps import AppConfig


class ModelCheckingConfig(AppConfig):
    name = 'ModelChecking'
