import os
import sys
from subprocess import CalledProcessError
from subprocess import TimeoutExpired
from subprocess import check_output

"""
    Relatively executor is used for changing context with 'os.chdir(path)' function.
    It's impossible to use 'os.chdir(path)' function on server application.
    
    :author Boyarkin Nikita
"""

length = len(sys.argv)

path = sys.argv[1]
commands = sys.argv[2:length]

os.chdir(path)

try:
    print(str(check_output(commands, shell=False, universal_newlines=True)))
    sys.exit(0x0)
except FileNotFoundError:
    sys.exit(0x8)
except PermissionError:
    sys.exit(0x9)
except CalledProcessError:
    sys.exit(0xA)
except TimeoutExpired:
    sys.exit(0xB)
