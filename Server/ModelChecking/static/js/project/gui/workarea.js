let Workarea = {
    filesList: null,

    initialize: function(filesJson) {
        this.removeHandlers();
        this.filesFormer(filesJson);
        this.addHandlers();
    },

    filesFormer: function(filesJson) {
        if (this.filesList !== null)
            return;

        this.filesList = [];
        for (let fileInformation of filesJson) {
            let item = $("#clonable-file-item").clone();
            item.removeAttr("id");

            // TODO: Filter to human-readable.

            let fileIdentifier = fileInformation["pk"];
            if (fileIdentifier !== undefined)
                item.attr("data-id", fileIdentifier);

            let fileName = fileInformation["fields"]["name"];
            if (fileName !== undefined)
                item.attr("data-name", fileName);

            let fileExtension = fileInformation["fields"]["extension"];
            if (fileExtension !== undefined)
                item.attr("data-extension", fileExtension);

            let fileCreatedTime = fileInformation["fields"]["created_time"];
            if (fileCreatedTime !== undefined)
                item.attr("data-created", fileCreatedTime);

            let fileModifiedTime = fileInformation["fields"]["modified_time"];
            if (fileModifiedTime !== undefined)
                item.attr("data-modified", fileModifiedTime);

            if (fileName !== undefined && fileExtension !== undefined)
                item.find("figcaption").text(fileName + fileExtension);

            item.appendTo($("#file-content"));
            this.filesList.push(item);
        }
    },

    addHandlers: function() {
        if (this.filesList === null)
            return;

        let workarea = $("#file-content");
        workarea.on("click", this.onLeftWorkareaClick);
        workarea.on("contextmenu", this.onRightWorkareaClick);

        for (let currentItem of this.filesList) {
            currentItem.on("click", this.onLeftClick);
            currentItem.on("contextmenu", this.onRightClick);
            currentItem.on("dblclick", this.onDoubleClick);
        }
    },

    removeHandlers: function() {
        if (this.filesList === null)
            return;

        let workarea = $("#file-content");
        workarea.off("click");
        workarea.off("contextmenu");

        for (let currentItem of this.filesList) {
            currentItem.off("click");
            currentItem.off("contextmenu");
            currentItem.off("dblclick");
        }
    },

    onLeftWorkareaClick(event) {
        let element = $(this);

        element.children().removeClass("active-item");
        $("#about-file").hide();

        event.stopPropagation();
    },

    onRightWorkareaClick(event) {
        let element = $(this);

        // TODO: Show context menu.

        event.preventDefault();
        event.stopPropagation();
    },

    onLeftClick(event) {
        let element = $(this);

        let isSelected = element.hasClass("active-item");
        element.parent().children().removeClass("active-item");

        let about = $("#about-file");
        if (!isSelected) {
            element.addClass("active-item");

            let fileId = element.attr("data-id");
            $("#about-file-identifier").text((fileId !== undefined) ? "#" + fileId : "");

            let fileName = element.attr("data-name");
            let fileExtension = element.attr("data-extension");
            $("#about-file-name").text((fileName !== undefined && fileExtension !== undefined) ? fileName + fileExtension : "");

            let fileCreated = element.attr("data-created");
            $("#about-file-created-time").text((fileCreated !== undefined) ? fileCreated : "");

            let fileModified = element.attr("data-modified");
            $("#about-file-modified-time").text((fileModified !== undefined) ? fileModified : "");

            // TODO: Buttons.

            about.show();
        } else {
            about.hide();
        }

        event.stopPropagation();
    },

    onRightClick(event) {
        let element = $(this);

        // TODO: Show context menu.

        event.preventDefault();
        event.stopPropagation();
    },

    onDoubleClick(event) {
        let element = $(this);

        let fileId = element.attr("data-id");
        if (fileId !== undefined)
            window.location.href = "/?id=" + fileId;

        event.stopPropagation();
    }
};
