let Interaction = {
    projectInformation: null,
    filesInformation: null,

    initializeProject(projectInformation, filesInformation) {
        this.projectInformation = (projectInformation) ? projectInformation.toString() : null;
        this.filesInformation = (filesInformation) ? filesInformation.toString() : null;
    },

    onWindowLoad: function() {
        this.restoreContext();
    },
    
    restoreContext: function() {
        if (this.projectInformation !== null) {
            let projectJson = JSON.parse(this.projectInformation.toString())[0];

            let projectTitle = projectJson["fields"]["title"];
            if (projectTitle)
                $("#information-project-title").text(projectTitle);

            let projectId = projectJson["pk"];
            if (projectId)
                $("#information-project-identifier").text("#" + projectId);
        }

        if (this.filesInformation !== null) {
            let filesJson = JSON.parse(this.filesInformation.toString());

            Workarea.initialize(filesJson);
        } else {
            $("#file-no-access-error").show();
        }
    }
};