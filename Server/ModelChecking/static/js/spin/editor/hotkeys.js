let Hotkeys = {
    NEW_FILE_KEY: 0x4E,
    OPEN_FILE_KEY: 0x4F,
    SAVE_FILE_KEY: 0x53,
    CLOSE_FILE_KEY: 0x51,
    UNDO_KEY: 0x5A,
    REDO_KEY: 0x59,
    RENAME_KEY: 0x52,
    DELETE_KEY: 0x2E,
    NEW_PROCESS_KEY: 0x50,
    NEW_STATE_KEY: 0x47,
    NEW_TRANSITION_KEY: 0x54,
    INITIAL_STATE_KEY: 0x49,
    CHANGE_CONTEXT_KEY: 0x42,

    newFileEnabled: null,
    newFileLink: null,
    openFileEnabled: null,
    openFileLink: null,
    saveFileEnabled: null,
    saveFileLink: null,
    closeFileEnabled: null,
    closeFileLink: null,
    undoEnabled: null,
    undoLink: null,
    redoEnabled: null,
    redoLink: null,
    renameEnabled: null,
    renameLink: null,
    deleteEnabled: null,
    deleteLink: null,
    newProcessEnabled: null,
    newProcessLink: null,
    newStateEnabled: null,
    newStateLink: null,
    newTransitionEnabled: null,
    newTransitionLink: null,
    initialEnabled: null,
    initialLink: null,
    changeContextEnabled: null,
    changeContextLink: null,

    resetVariables: function() {
        this.newFileEnabled = true;
        this.newFileLink = $("#menu-file-new-option")[0];

        this.openFileEnabled = true;
        this.openFileLink = $("#menu-file-open-option")[0];

        this.saveFileEnabled = false;
        this.saveFileLink = $("#menu-file-save-option")[0];

        this.closeFileEnabled = false;
        this.closeFileLink = $("#menu-file-close-option")[0];

        this.undoEnabled = false;
        this.undoLink = $("#menu-edit-undo-option")[0];

        this.redoEnabled = false;
        this.redoLink = $("#menu-edit-redo-option")[0];

        this.renameEnabled = false;
        this.renameLink = $("#menu-edit-rename-option")[0];

        this.deleteEnabled = false;
        this.deleteLink = $("#menu-edit-delete-option")[0];

        this.newProcessEnabled = false;
        this.newProcessLink = $("#menu-constructor-new-process-option")[0];

        this.newStateEnabled = false;
        this.newStateLink = $("#menu-constructor-new-state-option")[0];

        this.newTransitionEnabled = false;
        this.newTransitionLink = $("#menu-constructor-new-transition-option")[0];

        this.initialEnabled = false;
        this.initialLink = $("#menu-constructor-set-initial-option")[0];

        this.changeContextEnabled = false;
        this.changeContextLink = $("#menu-constructor-change-context-option")[0];
    },

    onKeyPressed: function(event) {
        switch (event.keyCode) {
            case Hotkeys.NEW_FILE_KEY:
                if (!event.altKey)
                    break;

                if (Hotkeys.newFileEnabled === true)
                    Hotkeys.newFileLink.click();
                break;

            case Hotkeys.OPEN_FILE_KEY:
                if (!event.altKey)
                    break;

                if (Hotkeys.openFileEnabled === true)
                    Hotkeys.openFileLink.click();
                break;

            case Hotkeys.SAVE_FILE_KEY:
                if (!event.altKey)
                    break;

                if (Hotkeys.saveFileEnabled === true)
                    Hotkeys.saveFileLink.click();
                break;

            case Hotkeys.CLOSE_FILE_KEY:
                if (!event.altKey)
                    break;

                if (Hotkeys.closeFileEnabled === true)
                    Hotkeys.closeFileLink.click();
                break;

            case Hotkeys.ESCAPE_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true)
                    break;

                Constructor.deselectAll();
                break;

            case Hotkeys.UNDO_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true || !(event.altKey ^ event.ctrlKey))
                    break;

                if (Hotkeys.undoEnabled === true)
                    Hotkeys.undoLink.click();
                break;

            case Hotkeys.REDO_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true || !(event.altKey ^ event.ctrlKey))
                    break;

                if (Hotkeys.redoEnabled === true)
                    Hotkeys.redoLink.click();
                break;

            case Hotkeys.RENAME_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true || !event.altKey)
                    break;

                if (Hotkeys.renameEnabled === true)
                    Hotkeys.renameLink.click();
                break;

            case Hotkeys.DELETE_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true)
                    break;

                if (Hotkeys.deleteEnabled === true)
                    Hotkeys.deleteLink.click();
                break;

            case Hotkeys.NEW_PROCESS_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true || !event.altKey)
                    break;

                if (Hotkeys.newProcessEnabled === true)
                    Hotkeys.newProcessLink.click();
                break;

            case Hotkeys.NEW_STATE_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true || !event.altKey)
                    break;

                if (Hotkeys.newStateEnabled === true)
                    Hotkeys.newStateLink.click();
                break;

            case Hotkeys.NEW_TRANSITION_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true || !event.altKey)
                    break;

                if (Hotkeys.newTransitionEnabled === true)
                    Hotkeys.newTransitionLink.click();
                break;

            case Hotkeys.INITIAL_STATE_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true || !event.altKey)
                    break;

                if (Hotkeys.initialEnabled === true)
                    Hotkeys.initialLink.click();
                break;

            case Hotkeys.CHANGE_CONTEXT_KEY:
                if (Environment.constructorMode !== true || Constructor.enabled !== true || !event.altKey)
                    break;

                if (Hotkeys.changeContextEnabled === true)
                    Hotkeys.changeContextLink.click();
                break;

            default:
                break;
        }
    }
};
