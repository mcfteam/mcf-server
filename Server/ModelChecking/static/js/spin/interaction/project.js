let Project = {
    projectsInformation: null,
    projectsMap: null,

    initializeProject(projectsInformation) {
        this.projectsInformation = (projectsInformation) ? projectsInformation.toString() : null;
    },

    onWindowLoad: function() {
        this.parseProjectsInformation();
    },

    parseProjectsInformation: function() {
        if (this.projectsInformation !== null) {
            let projectsJson = JSON.parse(this.projectsInformation.toString());

            this.projectsMap = new Map();

            let lastProject = null;
            let lastFiles = null;
            for (let currentItem of projectsJson) {
                let isProject = currentItem["model"] === "ModelChecking.project";

                if (isProject && lastProject !== currentItem) {
                    if (lastProject !== null && lastFiles !== null) {
                        this.projectsMap.set(lastProject, lastFiles);
                    }

                    lastProject = currentItem;
                    lastFiles = [];
                }

                if (!isProject && lastFiles !== null) {
                    lastFiles.push(currentItem);
                }
            }

            if (lastProject !== null && lastFiles !== null) {
                this.projectsMap.set(lastProject, lastFiles);
            }
        }
    }
};