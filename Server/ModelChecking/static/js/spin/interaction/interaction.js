let Interaction = {
    /**
     *
     * TODO
     *
     * Shows and hides some html blocks, based on environment variables.
     * Calls, when window is loaded (after Environment.initializeEnvironment).
     *
     * @author Boyarkin Nikita
     *
     */
    onWindowLoad: function() {
        Menu.initializeListeners();
        Editor.initializeListeners();
        Hotkeys.resetVariables();
        Project.onWindowLoad();
        Interaction.restoreContext();
    },

    /**
     *
     * Creates data fields for submitting form.
     * Saves some text fields for restoring after POST request.
     *
     * @returns {Boolean}
     *
     * @author Boyarkin Nikita
     *
     */
    saveContext: function() {
        let constructorModeElement = $("#hidden-constructor-mode"),
            verificationModeElement = $("#hidden-verification-mode"),
            constructorElement = $("#hidden-constructor"),
            optionsElement = $("#hidden-options"),
            commandLineElement = $("#command-line"),
            submitErrorElement = $("#submit-error");

        if (Environment.constructorMode === null) {
            log(LogLevel.ERROR, "Impossible situation: constructor mode is null, but the form is visible.");
            return false;
        }

        constructorModeElement.val(Environment.constructorMode.toString());
        verificationModeElement.val(Environment.verificationMode.toString());

        if (Environment.constructorMode === true) {
            // Save constructor scheme into string field.
            constructorElement.val(Constructor.automaton.toSmcf());
        }

        // Check if field is empty.
        if (!Editor.getText().trim()) {
            submitErrorElement.html("It makes no sense to send form: promela source code is empty.");
            submitErrorElement.show();
            log(LogLevel.INFO, "It makes no sense to send form: promela source code is empty.");
            return false;
        }

        // Check if field is empty.
        if (!commandLineElement.val().trim()) {
            submitErrorElement.html("It makes no sense to send form: spin command line is empty.");
            submitErrorElement.show();
            log(LogLevel.INFO, "It makes no sense to send form: spin command line is empty.");
            return false;
        }

        // Save options into string field.
        optionsElement.val(this.saveAllOptions());

        log(LogLevel.DEBUG, "Form submit parameters: " + constructorModeElement.val() + ", " + verificationModeElement.val() + ", " + constructorElement.val() + ", " +
            optionsElement.val() + ", " + Editor.getText() + ", " + commandLineElement.val() + ".");
        return true;
    },

    /**
     *
     * Shows and hides some html blocks, based on environment variables.
     * Restores some text fields, which were saved before form submitting.
     *
     * @author Boyarkin Nikita
     *
     */
    restoreContext: function() {
        let contentBlock = $("#content-block"),
            startScreenBlock = $("#start-screen"),
            constructorBlock = $("#constructor-block"),
            instrumentsBlock = $("#instruments-block"),
            resultBlock = $("#result-block"),
            commandLineElement = $("#command-line"),
            operationResultElement = $("#operation-result"),
            submitErrorElement = $("#submit-error"),
            constructorOverlayElement = $("#constructor-overlay"),
            menuEditHeader = $("#menu-edit-header"),
            menuConstructorHeader = $("#menu-constructor-header"),
            fileSaveAsMenuOption = $("#menu-file-save-option"),
            fileCloseMenuOption = $("#menu-file-close-option"),
            editUndoMenuOption = $("#menu-edit-undo-option"),
            editRedoMenuOption = $("#menu-edit-redo-option"),
            editDeleteMenuOption = $("#menu-edit-delete-option"),
            editRenameMenuOption = $("#menu-edit-rename-option"),
            constructorChangeContextMenuOption = $("#menu-constructor-change-context-option"),
            constructorNewStateMenuOption = $("#menu-constructor-new-state-option"),
            constructorSetInitialMenuOption = $("#menu-constructor-set-initial-option"),
            constructorNewTransitionMenuOption = $("#menu-constructor-new-transition-option"),
            contextGlobal = $("#application-global-context"),
            contextInitStart = $("#application-init-start"),
            contextInit = $("#application-init-content"),
            contextInitEnd = $("#application-init-end");

        Hotkeys.resetVariables();

        if (Environment.constructorMode === null) {
            // Show start screen.
            startScreenBlock.show();
            contentBlock.hide();

            // Hide constructor menu tabs.
            menuConstructorHeader.hide();
            menuEditHeader.hide();

            // Disable menu items.
            fileSaveAsMenuOption.addClass("unavailable-option");
            fileCloseMenuOption.addClass("unavailable-option");
            return;
        }

        // Clear error message.
        submitErrorElement.html("");
        submitErrorElement.hide();

        // Make menu items available.
        fileSaveAsMenuOption.removeClass("unavailable-option");
        fileCloseMenuOption.removeClass("unavailable-option");

        // Show content area if Environment.constructorMode not null.
        startScreenBlock.hide();
        contentBlock.show();

        if (Environment.constructorMode === true) {
            // Block all edit elements.
            editUndoMenuOption.addClass("unavailable-option");
            editRedoMenuOption.addClass("unavailable-option");
            editDeleteMenuOption.addClass("unavailable-option");
            editRenameMenuOption.addClass("unavailable-option");
            constructorChangeContextMenuOption.addClass("unavailable-option");
            constructorSetInitialMenuOption.addClass("unavailable-option");

            // Without focus on process, this options must be disabled.
            constructorNewStateMenuOption.addClass("unavailable-option");
            constructorNewTransitionMenuOption.addClass("unavailable-option");

            // Show constructor menu tabs.
            menuConstructorHeader.show();
            menuEditHeader.show();

            // Hide overlay.
            constructorOverlayElement.hide();

            // Show constructor and instrument blocks.
            constructorBlock.show();
            instrumentsBlock.show();

            // Set initial values fot context.
            contextInitStart.text("init {\n");
            contextInitEnd.text("}\n");

            if (!Constructor.automaton) {
                // Reset all constructor variables.
                if (Environment.savedConstructor === null)
                    Constructor.resetSvg();

                // Initialize SVG constructor element.
                Constructor.initializeAutomation();

                if (Environment.savedConstructor !== null)
                    Constructor.automaton.fromSmcf(Environment.savedConstructor);
                else {
                    // Set initial values fot context.
                    contextGlobal.text("/*\n\tDefine global variables here.\n*/\n\n");
                    contextInit.text("\t/*\n\t\tThe entry point of application.\n\t\tDefine channels, run processes here.\n\t*/\n");
                }
            }
            else
                Constructor.shiftSvg();

            Hotkeys.newProcessEnabled = true;
        }
        else if (Environment.constructorMode === false) {
            // Reset all constructor variables.
            Constructor.resetSvg();

            // Hide constructor menu tabs.
            menuConstructorHeader.hide();
            menuEditHeader.hide();

            // Hide constructor and instrument blocks.
            constructorBlock.hide();
            instrumentsBlock.hide();
        }

        // Restore promela source code.
        Editor.setText((Environment.savedSource !== null) ? Environment.savedSource : "");

        // Restore command line flags.
        commandLineElement.val((Environment.savedFlags !== null) ? Environment.savedFlags : "");

        if (Environment.verificationMode === true)
            Gui.activateVerificationMode(false);
        else
            Gui.activateSimulationMode(false);

        if (Environment.savedOptions === null) {
            let defaultOptions = "10011001000101002:1015:123454:100000";
            this.restoreAllOptions(defaultOptions);
        }
        else
            this.restoreAllOptions(Environment.savedOptions);

        Gui.commandLineFormer();

        // Hide result block by default.
        operationResultElement.removeClass("error");

        if (Environment.errorCode === null && Environment.operationResult === null) {
            // Not need to do something.
            resultBlock.hide();

            operationResultElement.val("");
        }
        if (Environment.errorCode === null && Environment.operationResult !== null) {
            // Impossible situation: result isn't null but error is null.
            resultBlock.show();

            log(LogLevel.ERROR, "Impossible situation: result isn't null but error is null.");

            // Set error style for text field and print error message.
            operationResultElement.addClass("error");
            operationResultElement.val("Impossible situation: result isn't null but error is null.");
        }
        else if (Environment.errorCode === 0 && Environment.operationResult === null) {
            resultBlock.hide();
        }
        else if (Environment.errorCode === 0 && Environment.operationResult !== null) {
            // Successfully operation.
            resultBlock.show();

            operationResultElement.val(Environment.operationResult);

            let rows = Environment.operationResult.split(/\r\n|\r|\n/).length;
            operationResultElement.attr("rows", (rows > Gui.TEXTAREA_MAX_LENGTH) ? Gui.TEXTAREA_MAX_LENGTH : rows);

            if (Environment.constructorMode === true && Environment.verificationMode === false) {
                // TODO Operation result -> constructor simulation.
            }
        }
        else if (Environment.errorCode > 0) {
            // Operation fail.
            resultBlock.show();

            // Set error style for text field.
            operationResultElement.addClass("error");
            operationResultElement.val(this.getErrorMessage());
            operationResultElement.attr("rows", "1");
        }

        Hotkeys.saveFileEnabled = true;
        Hotkeys.closeFileEnabled = true;

        if (Environment.constructorMode === true && Environment.savedConstructor !== null && Environment.operationResult !== null &&
            Environment.savedSource !== null && Constructor.automaton !== null)
            Constructor.automaton.startSimulation(Environment.savedSource, Environment.operationResult);
    },

    /**
     *
     * Converts all options (radiobuttons, checkboxes and numbers) to compact mode.
     *
     * @returns {String}
     *
     * @author Boyarkin Nikita
     *
     */
    saveAllOptions: function() {
        let result = "";

        result += this.saveOption($("#safety-radiobutton"));
        result += this.saveOption($("#liveness-non-progress-radiobutton"));
        result += this.saveOption($("#liveness-acceptable-radiobutton"));
        result += this.saveOption($("#invalid-endstates-checkbox"));
        result += this.saveOption($("#assertion-violations-checkbox"));
        result += this.saveOption($("#non-progress-fairness-checkbox"));
        result += this.saveOption($("#acceptable-fairness-checkbox"));

        result += this.saveOption($("#exhaustive-radiobutton"));
        result += this.saveOption($("#hash-compact-radiobutton"));
        result += this.saveOption($("#bitstate-radiobutton"));
        result += this.saveOption($("#collapse-compression-checkbox"));

        result += this.saveOption($("#depth-first-radiobutton"));
        result += this.saveOption($("#breadth-first-radiobutton"));
        result += this.saveOption($("#depth-first-partial-order-checkbox"));
        result += this.saveOption($("#iterative-search-checkbox"));
        result += this.saveOption($("#bounded-context-switching-checkbox"));
        result += this.saveNumericOption($("#maximum-context-switches"));

        result += this.saveOption($("#report-unreachable-checkbox"));

        result += this.saveNumericOption($("#seed"));
        result += this.saveNumericOption($("#maximum-steps"));
        result += this.saveOption($("#track-data-values-checkbox"));
        result += this.saveOption($("#lose-new-messages-checkbox"));

        return result;
    },

    /**
     *
     * Restores all options (radiobuttons, checkboxes and numbers) from compact mode.
     *
     * @param options - Saved options special string (in compact mode).
     * @type {String}
     *
     * @author Boyarkin Nikita
     *
     */
    restoreAllOptions: function(options) {
        let result = options;

        result = this.restoreOption($("#safety-radiobutton"), result);
        result = this.restoreOption($("#liveness-non-progress-radiobutton"), result);
        result = this.restoreOption($("#liveness-acceptable-radiobutton"), result);
        result = this.restoreOption($("#invalid-endstates-checkbox"), result);
        result = this.restoreOption($("#assertion-violations-checkbox"), result);
        result = this.restoreOption($("#non-progress-fairness-checkbox"), result);
        result = this.restoreOption($("#acceptable-fairness-checkbox"), result);

        result = this.restoreOption($("#exhaustive-radiobutton"), result);
        result = this.restoreOption($("#hash-compact-radiobutton"), result);
        result = this.restoreOption($("#bitstate-radiobutton"), result);
        result = this.restoreOption($("#collapse-compression-checkbox"), result);

        result = this.restoreOption($("#depth-first-radiobutton"), result);
        result = this.restoreOption($("#breadth-first-radiobutton"), result);
        result = this.restoreOption($("#depth-first-partial-order-checkbox"), result);
        result = this.restoreOption($("#iterative-search-checkbox"), result);
        result = this.restoreOption($("#bounded-context-switching-checkbox"), result);
        result = this.restoreNumericOption($("#maximum-context-switches"), result);

        result = this.restoreOption($("#report-unreachable-checkbox"), result);

        result = this.restoreNumericOption($("#seed"), result);
        result = this.restoreNumericOption($("#maximum-steps"), result);
        result = this.restoreOption($("#track-data-values-checkbox"), result);
        this.restoreOption($("#lose-new-messages-checkbox"), result);
    },

    /**
     *
     * Converts next option (checkbox or radiobutton) to compact mode.
     *
     * @param element - Next checkbox or radiobutton (jQuery element).
     * @type {Object}
     *
     * @returns {String}
     *
     * @author Boyarkin Nikita
     *
     */
    saveOption: function(element) {
        return (element.prop("checked") ? "1" : "0");
    },

    /**
     *
     * Converts next option (number) to compact mode.
     *
     * @param element - Next number field (jQuery element).
     * @type {Object}
     *
     * @returns {String}
     *
     * @author Boyarkin Nikita
     *
     */
    saveNumericOption: function(element) {
        let value = element.val();
        value = ((!value) ? "0" : value);

        return value.length.toString() + ":" + value;
    },

    /**
     *
     * Restores next option (checkbox or radiobutton) from compact mode.
     *
     * @param element - Next checkbox or radiobutton (jQuery element).
     * @type {Object}
     *
     * @param options - Saved options special string (in compact mode).
     * @type {String}
     *
     * @author Boyarkin Nikita
     *
     */
    restoreOption: function(element, options) {
        element.prop("checked", options.charAt(0) === '1');

        if (element.attr("type") === "checkbox")
            Gui.checkboxIconChanged(element[0]);
        else
            Gui.radiobuttonIconChanged(element[0]);

        return options.substr(1);
    },

    /**
     *
     * Restores next option (number) from compact mode.
     *
     * @param element - Next number field (jQuery element).
     * @type {Object}
     *
     * @param options - Saved options special string (in compact mode).
     * @type {String}
     *
     * @returns {String}
     *
     * @author Boyarkin Nikita
     *
     */
    restoreNumericOption: function(element, options) {
        let delimiterIndex = options.indexOf(":");
        let numericLength = parseInt(options.substr(0, delimiterIndex));
        let numericValue = options.substr(delimiterIndex + 1, numericLength);

        element.val(numericValue);

        return options.substr(delimiterIndex + 1 + numericLength);
    },

    /**
     *
     * Returns error message, based on current error code.
     *
     * @return {String}
     *
     * @author Boyarkin Nikita
     *
     */
    getErrorMessage: function() {
        switch (Environment.errorCode) {
            case 0x1:
                return "0x0001 - Package error: Undefined constructor mode.";
            case 0x2:
                return "0x0002 - Package error: Constructor is defined, but constructor mode switched off.";
            case 0x3:
                return "0x0003 - Execution error: It's impossible to create working directory.";
            case 0x4:
                return "0x0004 - Execution error: It's impossible to create working directory.";
            case 0x5:
                return "0x0005 - Execution error: It's impossible to create file with promela source.";
            case 0x6:
                return "0x0006 - Execution error: It's impossible to copy spin executable.";
            case 0x7:
                return "0x0007 - Execution error: It's impossible to copy spin executable.";
            case 0x8:
                return "0x0008 - Execution error: File not found error.";
            case 0x9:
                return "0x0009 - Execution error: Permission error.";
            case 0xA:
                return "0x000A - Execution error: Wrong input parameters (promela source or command line).";
            case 0xB:
                return "0x000B - Execution error: Timeout expired.";
            case 0xC:
                return "0x000C - Execution error: Server started on wrong operating system.";
            case 0xD:
                return "0x000D - Source code error: Source code or command line are empty.";
            case 0xE:
                return "0x000E - Package error: Only one parameter is defined, but requested two (source code and command line).";
            case 0xF:
                return "0x000F - Package error: No constructor mode parameter into the post request.";
            case 0x10:
                return "0x0010 - Package error: No options parameter into the post request.";
            case 0x11:
                return "0x0011 - Package error: No constructor parameter into the post request.";
            case 0x12:
                return "0x0012 - Package error: Undefined verification mode.";
            case 0x13:
                return "0x0013 - Package error: No verification mode parameter into the post request.";
            case 0x14:
                return "0x0014 - Package error: No executable parameters into the post request.";
            case 0x15:
                return "0x0015 - Package error: It's impossible to get spin and gcc flags from command line.";
            case 0x16:
                return "0x0016 - Query error: User is not logged in.";
            case 0x17:
                return "0x0017 - Query error: Field 'id' must be a number.";
            case 0x18:
                return "0x0018 - Query error: Field 'id' must be a positive.";
            case 0x19:
                return "0x0019 - Query error: This page does not yet exist.";
            case 0x1A:
                return "0x001A - Query error: You have no permissions.";
            case 0x1B:
                return "0x001B - Query error: Impossible to load file with this extension.";
            default:
                return null;
        }
    }
};
