/**
 *
 * If 'true', blocks some gui features.
 * @type {boolean}
 *
 */
const TEST = false;


let Environment = {
    /**
     *
     * Environment variables, used for save, restore and forward data on POST requests.
     *
     */
    constructorMode: null,
    errorCode: null,
    operationResult: null,
    verificationMode: null,
    savedConstructor: null,
    savedSource: null,
    savedOptions: null,
    savedFlags: null,

    /**
     *
     * Application entry point. Initializes environment variables.
     *
     * @param constructorMode - If not defined, showing start screen for user. If constructorMode is true, showing special constructor block.
     * @type {String}
     *
     * @param errorCode - Operation error code from server.
     * @type {String}
     *
     * @param operationResult - Result of spin executable.
     * @type {String}
     *
     * @param verificationMode - True, if verification mode is enabled. False, if simulation mode if enabled.
     * @type {String}
     *
     * @param savedConstructor - Saved constructor special string. Make sense if constructorMode is true.
     * @type {String}
     *
     * @param savedSource - Saved promela source code.
     * @type {String}
     *
     * @param savedOptions - Saved options special string (in compact mode).
     * @type {String}
     *
     * @param savedFlags - Saved command line flags (for spin executable and gcc compiler).
     * @type {String}
     *
     * @author Boyarkin Nikita
     *
     */
    initializeEnvironment: function(constructorMode, errorCode, operationResult, verificationMode, savedConstructor, savedSource, savedOptions, savedFlags) {
        this.constructorMode = (constructorMode === "false") ? false : (constructorMode === "true") ? true : null;
        this.errorCode = ($.isNumeric(errorCode)) ? parseInt(errorCode) : null;
        this.operationResult = (operationResult) ? operationResult.toString() : null;
        this.verificationMode = (verificationMode === "false") ? false : (verificationMode === "true") ? true : null;
        this.savedConstructor = (this.constructorMode !== true) ? null : savedConstructor.toString();
        this.savedSource = (savedSource) ? savedSource.toString() : null;
        this.savedOptions = (savedOptions) ? savedOptions.toString() : null;
        this.savedFlags = (savedFlags) ? savedFlags.toString() : null;

        log(LogLevel.DEBUG, "Initialized values: " + this.constructorMode + ", " + this.errorCode + ", " + this.operationResult + ", " + this.verificationMode + ", " +
            this.savedConstructor + ", " + this.savedSource + ", " + this.savedOptions + ", " + this.savedFlags + ".");
    },

    /**
     *
     * Resets environment variables to null.
     *
     * @author Boyarkin Nikita
     *
     */
    resetEnvironment: function() {
        this.constructorMode = null;
        this.errorCode = null;
        this.operationResult = null;
        this.verificationMode = null;
        this.savedConstructor = null;
        this.savedSource = null;
        this.savedOptions = null;
        this.savedFlags = null;
    }
};