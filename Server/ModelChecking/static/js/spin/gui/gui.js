let Gui = {
    TEXTAREA_MAX_LENGTH: 50,

    /**
     *
     * Activates verification mode (Environment.verificationMode = true).
     *
     * @param optimization - Boolean optimization value.
     * @type {Boolean}
     *
     * @author Boyarkin Nikita
     *
     */
    activateVerificationMode: function(optimization) {
        if (optimization && Environment.verificationMode === true)
            return;

        $("#verification-content").show();
        $("#simulation-content").hide();

        $("#verification-tab").addClass("active-tab");
        $("#simulation-tab").removeClass("active-tab");

        $("#submit-button").val("Verification");

        $("#command-line").attr("rows", "3");

        Environment.verificationMode = true;
        this.commandLineFormer();
    },

    /**
     *
     * Activates simulation mode (Environment.verificationMode = false).
     *
     * @param optimization - Boolean optimization value.
     * @type {Boolean}
     *
     * @author Boyarkin Nikita
     *
     */
    activateSimulationMode: function(optimization) {
        if (optimization && Environment.verificationMode === false)
            return;

        $("#verification-content").hide();
        $("#simulation-content").show();

        $("#verification-tab").removeClass("active-tab");
        $("#simulation-tab").addClass("active-tab");

        $("#submit-button").val("Simulation");

        $("#command-line").attr("rows", "1");

        Environment.verificationMode = false;
        this.commandLineFormer();
    },

    /**
     *
     * Sets the checkbox appearance.
     *
     * @param element - DOM checkbox element on which method called.
     * @type {HTMLInputElement}
     *
     * @author Boyarkin Nikita
     *
     */
    checkboxIconChanged: function(element) {
        if (element.checked) {
            $(element).parent().find("span").addClass("checkbox-icon-checked");

            let bound = $(element).data("bound");
            if (bound !== undefined)
                $("#" + bound).show();
        }
        else {
            $(element).parent().find("span").removeClass("checkbox-icon-checked");

            let bound = $(element).data("bound");
            if (bound !== undefined)
                $("#" + bound).hide();
        }

        this.commandLineFormer();
    },

    /**
     *
     * Sets the radiobutton appearance.
     *
     * @param element - DOM radiobutton element on which method called.
     * @type {HTMLInputElement}
     *
     * @author Boyarkin Nikita
     *
     */
    radiobuttonIconChanged: function(element) {
        if (!element.checked)
            return;

        let elementName = $(element).attr("name");

        jQuery("input[name='" + elementName + "']").each(function() {
            $(this).parent().find("span").removeClass("radiobutton-icon-checked");

            let bound = $(this).data("bound");
            if (bound !== undefined)
                $("#" + bound).hide();
        });

        $(element).parent().find("span").addClass("radiobutton-icon-checked");

        let bound = $(element).data("bound");
        if (bound !== undefined)
            $("#" + bound).show();

        this.commandLineFormer();
    },

    /**
     *
     * Creates full command line value on every option changed.
     *
     * @author Boyarkin Nikita
     *
     */
    commandLineFormer: function() {
        if (Environment.verificationMode === true) {
            let gccFlags = "",
                compileFlags = "";

            if ($("#safety-radiobutton").prop("checked")) {
                gccFlags += "-DSAFETY ";

                if (!$("#invalid-endstates-checkbox").prop("checked"))
                    compileFlags += "-E ";

                if (!$("#assertion-violations-checkbox").prop("checked"))
                    compileFlags += "-A ";
            }
            else if ($("#liveness-non-progress-radiobutton").prop("checked")) {
                gccFlags += "-DNP ";
                compileFlags += "-l ";

                if ($("#non-progress-fairness-checkbox").prop("checked"))
                    compileFlags += "-f ";
            }
            else if ($("#liveness-acceptable-radiobutton").prop("checked")) {
                compileFlags += "-a ";

                if ($("#acceptable-fairness-checkbox").prop("checked"))
                    compileFlags += "-f ";
            }

            if ($("#exhaustive-radiobutton").prop("checked")) {
                if ($("#collapse-compression-checkbox").prop("checked"))
                    gccFlags += "-DCOLLAPSE ";
            }
            else if ($("#hash-compact-radiobutton").prop("checked")) {
                gccFlags += "-DHC4 ";
            }
            else if ($("#bitstate-radiobutton").prop("checked")) {
                gccFlags += "-DBITSTATE ";
                compileFlags += "-k3 -w20 ";
            }

            if ($("#depth-first-radiobutton").prop("checked")) {
                if (!$("#depth-first-partial-order-checkbox").prop("checked"))
                    gccFlags += "-DNOREDUCE ";

                if ($("#iterative-search-checkbox").prop("checked"))
                    compileFlags += "-i ";

                if ($("#bounded-context-switching-checkbox").prop("checked")) {
                    gccFlags += "-DBCS ";

                    let value = $("#maximum-context-switches").val();
                    compileFlags += "-L" + ((!value) ? "0" : value) + " ";
                }
            }
            else if ($("#breadth-first-radiobutton").prop("checked")) {
                gccFlags += "-DBFS ";

                if (!$("#breadth-first-partial-order-checkbox").prop("checked"))
                    gccFlags += "-DNOREDUCE ";
            }

            if (!$("#report-unreachable-checkbox").prop("checked"))
                compileFlags += "-n ";

            $("#command-line").val("spin -a source.pml\ngcc -DMEMLIM=1024 -O2 -DXUSAFE -DNOCLAIM " + gccFlags + "-w -o pan pan.c\n./pan -m10000 " + compileFlags);
        }
        else if (Environment.verificationMode === false) {
            let result = "spin -p -s -r -X -v -T ";

            let value = $("#seed").val();
            result += "-n" + ((!value) ? "0" : value) + " ";

            if ($("#track-data-values-checkbox").prop("checked"))
                result += "-l -g ";

            if ($("#lose-new-messages-checkbox").prop("checked"))
                result += "-m ";

            value = $("#maximum-steps").val();
            result += "-u" + ((!value) ? "0" : value) + " ";

            result += "source.pml";
            $("#command-line").val(result);
        }
    },

    /**
     *
     * TODO
     *
     */
    convertToPromela: function() {
        let submitErrorField = $("#to-promela-error");

        submitErrorField.text("");
        submitErrorField.css("display", "none");

        if (!Constructor.automaton) {
            // TODO log
            submitErrorField.text("Constructor not yet initialized.");
            submitErrorField.css("display", "block");
            return;
        }

        let promela = Constructor.automaton.toPromela();
        switch (promela) {
            case 0x1:
                // TODO log
                submitErrorField.text("There is not a single state.");
                submitErrorField.css("display", "block");
                break;

            case 0x2:
                // TODO log
                submitErrorField.text("There is not a single initial state.");
                submitErrorField.css("display", "block");
                break;

            default:
                Editor.setText(promela);
                break;
        }
    },

    /**
     * TODO
     * @param value
     */
    blockUndo: function(value) {
        if (TEST === true)
            return;

        if (value) {
            $("#menu-edit-undo-option").addClass("unavailable-option");
            $("#on-svg-context-menu-undo").addClass("unavailable-option");
            $("#on-area-context-menu-undo").addClass("unavailable-option");

            Hotkeys.undoEnabled = false;
        }
        else {
            $("#menu-edit-undo-option").removeClass("unavailable-option");
            $("#on-svg-context-menu-undo").removeClass("unavailable-option");
            $("#on-area-context-menu-undo").removeClass("unavailable-option");

            Hotkeys.undoEnabled = true;
        }
    },

    /**
     * TODO
     * @param value
     */
    blockRedo: function(value) {
        if (TEST === true)
            return;

        if (value) {
            $("#menu-edit-redo-option").addClass("unavailable-option");
            $("#on-svg-context-menu-redo").addClass("unavailable-option");
            $("#on-area-context-menu-redo").addClass("unavailable-option");

            Hotkeys.redoEnabled = false;
        }
        else {
            $("#menu-edit-redo-option").removeClass("unavailable-option");
            $("#on-svg-context-menu-redo").removeClass("unavailable-option");
            $("#on-area-context-menu-redo").removeClass("unavailable-option");

            Hotkeys.redoEnabled = true;
        }
    },

    /**
     * TODO
     */
    unblockSelectedOptionsByProcess: function(processName) {
        if (TEST === true)
            return;

        $("#menu-edit-rename-option").removeClass("unavailable-option");
        $("#menu-edit-delete-option").removeClass("unavailable-option");
        $("#menu-constructor-change-context-option").removeClass("unavailable-option");

        $("#menu-edit-rename-option").attr("href", "javascript:javascript:Menu.constructorRenameProcess(null, '" + processName + "')");
        $("#menu-edit-delete-option").attr("href", "javascript:Constructor.removeProcess('" + processName + "')");
        $("#menu-constructor-change-context-option").attr("href", "javascript:Menu.constructorChangeProcessContext(null, '" + processName + "')");

        Hotkeys.renameEnabled = true;
        Hotkeys.deleteEnabled = true;
        Hotkeys.changeContextEnabled = true;
    },

    /**
     * TODO
     */
    unblockSelectedOptionsByState: function(processName, stateName) {
        if (TEST === true)
            return;

        $("#menu-edit-rename-option").removeClass("unavailable-option");
        $("#menu-edit-delete-option").removeClass("unavailable-option");
        $("#menu-constructor-set-initial-option").removeClass("unavailable-option");

        $("#menu-edit-rename-option").attr("href", "javascript:javascript:Menu.constructorRenameState(null, '" + processName + "', '" + stateName + "')");
        $("#menu-edit-delete-option").attr("href", "javascript:Constructor.removeState('" + processName + "', '" + stateName + "')");
        $("#menu-constructor-set-initial-option").attr("href", "javascript:Constructor.changeStateInitial('" + processName + "', '" + stateName + "')");

        Hotkeys.renameEnabled = true;
        Hotkeys.deleteEnabled = true;
        Hotkeys.initialEnabled = true;
    },

    /**
     * TODO
     */
    unblockSelectedOptionsByTransition: function(processName, firstStateName, secondStateName, condition) {
        if (TEST === true)
            return;

        $("#menu-edit-rename-option").removeClass("unavailable-option");
        $("#menu-edit-delete-option").removeClass("unavailable-option");

        $("#menu-edit-rename-option").attr("href", "javascript:Menu.constructorChangeConditionForTransition(null, '" + processName + "', '" + firstStateName + "', '" + secondStateName + "', '" + JSON.parse(condition) + "')");
        $("#menu-edit-delete-option").attr("href", "javascript:Constructor.removeTransition('" + processName + "', '" + firstStateName + "', '" + secondStateName + "', '" + JSON.parse(condition) + "')");

        Hotkeys.renameEnabled = true;
        Hotkeys.deleteEnabled = true;
    },

    /**
     * TODO
     */
    blockSelectedOptions: function() {
        if (TEST === true)
            return;

        $("#menu-edit-rename-option").addClass("unavailable-option");
        $("#menu-edit-delete-option").addClass("unavailable-option");
        $("#menu-constructor-set-initial-option").addClass("unavailable-option");
        $("#menu-constructor-change-context-option").addClass("unavailable-option");

        Hotkeys.renameEnabled = false;
        Hotkeys.deleteEnabled = false;
        Hotkeys.initialEnabled = false;
        Hotkeys.changeContextEnabled = false;
    },

    /**
     * TODO
     */
    blockNewOptions: function() {
        if (TEST === true)
            return;

        $("#menu-constructor-new-transition-option").addClass("unavailable-option");
        $("#menu-constructor-new-state-option").addClass("unavailable-option");
        $("#new-state-button").addClass("unavailable-button");
        $("#new-transition-button").addClass("unavailable-button");

        Hotkeys.newStateEnabled = false;
        Hotkeys.newTransitionEnabled = false;
    },

    /**
     * TODO
     */
    unblockNewOptions: function(processName) {
        if (TEST === true)
            return;

        $("#menu-constructor-new-state-option").removeClass("unavailable-option");
        $("#menu-constructor-new-transition-option").removeClass("unavailable-option");
        $("#new-state-button").removeClass("unavailable-button");
        $("#new-transition-button").removeClass("unavailable-button");

        $("#menu-constructor-new-state-option").attr("href", "javascript:Menu.constructorNewState(null, '" + processName + "')");
        $("#menu-constructor-new-transition-option").attr("href", "javascript:Menu.constructorNewTransition(null, '" + processName + "')");
        $("#new-state-button").attr("onclick", "Menu.constructorNewState(null, '" + processName + "')");
        $("#new-transition-button").attr("onclick", "Menu.constructorNewTransition(null, '" + processName + "')");

        Hotkeys.newStateEnabled = true;
        Hotkeys.newTransitionEnabled = true;
    },

    /**
     * TODO
     */
    blockConstructorOptions: function() {
        $("#menu-edit-undo-option").addClass("unavailable-option");
        $("#menu-edit-redo-option").addClass("unavailable-option");
        $("#menu-edit-rename-option").addClass("unavailable-option");
        $("#menu-edit-delete-option").addClass("unavailable-option");

        $("#menu-constructor-new-process-option").addClass("unavailable-option");
        $("#menu-constructor-change-context-option").addClass("unavailable-option");
        $("#menu-constructor-new-state-option").addClass("unavailable-option");
        $("#menu-constructor-set-initial-option").addClass("unavailable-option");
        $("#menu-constructor-new-transition-option").addClass("unavailable-option");

        $("#new-process-button").addClass("unavailable-button");
        $("#new-state-button").addClass("unavailable-button");
        $("#new-transition-button").addClass("unavailable-button");
    },

    /**
     *
     * TODO
     *
     */
    clearSelection: function() {
        if (TEST === true)
            return;

        if (window.getSelection) {
            if (window.getSelection().empty)
                window.getSelection().empty();
            else if (window.getSelection().removeAllRanges)
                window.getSelection().removeAllRanges();
        }
        else if (document.selection)
            document.selection.empty();
    },

    showCounter: function() {
        $("#constructor-step-counter").text("0");
        $("#constructor-step-counter-body").show();
    },

    setCounter: function(tick) {
        $("#constructor-step-counter-body").show();
        $("#constructor-step-counter").text(tick.toString());
    },

    hideCounter: function() {
        $("#constructor-step-counter-body").hide();
        $("#constructor-step-counter").text("0");
    },
};