let Menu = {
    /**
     *
     * Hides menu dialog.
     *
     * @author Boyarkin Nikita
     *
     **/
    hideMenu: function() {
        this.hideContextMenu();

        $("#menu-content").hide();
        $("#menu-file-new-dialog").hide();
        $("#menu-file-save-as-dialog").hide();
        $("#menu-file-close-dialog").hide();
        $("#menu-file-open-from-dialog").hide();
        $("#menu-file-save-to-dialog").hide();
        $("#menu-file-open-from-project-dialog").hide();
        $("#menu-file-save-to-project-dialog").hide();
        $("#menu-constructor-new-state-dialog").hide();
        $("#menu-constructor-new-transition-dialog").hide();
        $("#menu-constructor-new-transition-from-state-dialog").hide();
        $("#menu-constructor-rename-state-dialog").hide();
        $("#menu-constructor-change-condition-for-transition-dialog").hide();
        $("#menu-constructor-new-process-dialog").hide();
        $("#menu-constructor-rename-process-dialog").hide();
        $("#menu-constructor-change-process-context-dialog").hide();

        Dialog.dialogDisplayed = false;
        Dialog.dialogSubmitButton = null;
    },

    /**
     *
     * When the user clicks anywhere outside of the menu dialog, close it.
     *
     * @author Boyarkin Nikita
     *
     */
    onWindowClick: function(event) {
        if (event.target === $("#menu-content")[0])
            this.hideMenu();

        if (event.target === $("#context-block")[0])
            this.hideContextMenu();
    },

    /**
     * TODO
     */
    initializeListeners: function() {
        $("#context-block").on("contextmenu", (event) => {
            Menu.hideContextMenu();
            event.preventDefault()
        });
    },

    /**
     *
     * If option === null, shows menu dialog.
     * If option === 0x1, sets Environment.constructorMode to true and other environment variables to null. After that hides menu and restores context.
     * If option === 0x2, sets Environment.constructorMode to false and other environment variables to null. After that hides menu and restores context.
     *
     * @param option - Integer selector.
     * @type {Number}
     *
     * @author Boyarkin Nikita
     *
     **/
    fileNew: function(option) {
        this.hideContextMenu();

        if (!option) {
            $("#menu-content").show();
            $("#menu-file-new-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = null;
            return;
        }

        Constructor.resetSvg();
        Environment.resetEnvironment();

        if (option === 0x1) {
            Environment.constructorMode = true;
            Environment.verificationMode = true;

            log(LogLevel.INFO, "Create new file in constructor mode.");
        }
        else if (option === 0x2) {
            Environment.constructorMode = false;
            Environment.verificationMode = true;

            log(LogLevel.INFO, "Create new file in source mode.");
        }

        this.hideMenu();
        Interaction.restoreContext();
    },

    /**
     *
     * TODO
     *
     */
    fileOpen: function() {
        let downloadBlock = $("#download-block"),
            uploadField = $("#upload-href");

        this.hideContextMenu();

        uploadField.remove();

        uploadField = $("<input>");
        uploadField.attr("id", "upload-href");
        uploadField.attr("type", "file");
        uploadField.attr("accept", ".pml,.smcf");

        downloadBlock.append(uploadField);

        uploadField[0].click();

        uploadField.off("change");
        uploadField.on("change", (changeEvent) => {
            let file = uploadField.prop("files");

            if (!file || file.length !== 1) {
                // TODO log
                return;
            }

            file = file[0];

            let created = false;
            if (!Constructor.automaton) {
                Constructor.resetSvg();
                Constructor.initializeAutomation();
                created = true;
            }

            let reader = new FileReader();

            let extension = file.name.substring(file.name.lastIndexOf(".") + 1);
            switch (extension) {
                case "pml":
                    reader.readAsText(file);
                    reader.onload = (loadEvent) => {
                        let promelaSource = loadEvent.target.result;

                        let errcode = Constructor.automaton.fromPromela(promelaSource);
                        if (errcode === 0x0) {
                            Environment.resetEnvironment();

                            Environment.constructorMode = true;
                            Environment.verificationMode = true;

                            // TODO log

                            Interaction.restoreContext();
                        }
                        else {
                            Constructor.resetSvg();
                            Environment.resetEnvironment();

                            Environment.constructorMode = false;
                            Environment.verificationMode = true;
                            Environment.savedSource = promelaSource;

                            // TODO log

                            Interaction.restoreContext();
                        }
                    };

                    break;

                case "smcf":
                    reader.readAsText(file);
                    reader.onload = (loadEvent) => {
                        let smcfSource = loadEvent.target.result;

                        let result = Constructor.automaton.fromSmcf(smcfSource);
                        if (result === 0x0) {
                            Environment.resetEnvironment();

                            Environment.constructorMode = true;
                            Environment.verificationMode = true;

                            // TODO log

                            Interaction.restoreContext();
                        }
                        else if (created) {
                            // TODO log

                            Constructor.resetSvg();
                        }
                    };

                    break;

                default:
                    // TODO log

                    if (created)
                        Constructor.resetSvg();

                    break;
            }
        });
    },

    /**
     *
     * TODO
     *
     */
    fileSaveAs: function(option) {
        let downloadField = $("#download-href"),
            submitErrorField = $("#menu-save-as-error");

        this.hideContextMenu();

        if (!option) {
            $("#menu-content").show();
            $("#menu-file-save-as-dialog").show();

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = null;
            return;
        }

        switch (option) {
            case 0x1:
                let resultPromela = Constructor.automaton.toPromela();
                switch (resultPromela) {
                    case 0x1:
                        // TODO log
                        submitErrorField.text("There is not a single state.");
                        submitErrorField.css("display", "inline-block");
                        return;

                    case 0x2:
                        // TODO log
                        submitErrorField.text("There is not a single initial state.");
                        submitErrorField.css("display", "inline-block");
                        return;

                    default:
                        // TODO log
                        downloadField.attr("href", "data:text/pml;charset=utf-8," + encodeURIComponent(resultPromela));
                        downloadField.attr("download", "source.pml");
                        downloadField[0].click();

                        this.hideMenu();
                        break;
                }

                break;

            case 0x2:
                let resultSmcf = Constructor.automaton.toSmcf();

                // TODO log

                downloadField.attr("href", "data:text/smcf;charset=utf-8," + encodeURIComponent(resultSmcf));
                downloadField.attr("download", "source.smcf");
                downloadField[0].click();

                this.hideMenu();
                break;

            case 0x3:
                downloadField.attr("href", "data:text/pml;charset=utf-8," + encodeURIComponent(Editor.getText()));
                downloadField.attr("download", "source.pml");
                downloadField[0].click();
                break;
        }

        // TODO Save into file.
    },

    /**
     *
     * TODO
     *
     */
    fileSaveTo: function(option) {
        let submitErrorField = $("#menu-save-to-error");

        this.hideContextMenu();

        if (!option) {
            $("#menu-content").show();
            $("#menu-file-save-to-dialog").show();

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = null;
            return;
        }

        if (option === 0x1) {
            this.hideMenu();
            this.fileSaveAs(Environment.constructorMode === true ? null : 0x3);
            return;
        }

        if (option === 0x2) {
            if (Authorization.userEmail === null) {
                submitErrorField.text("User is not authenticated.");
                submitErrorField.css("display", "inline-block");
                return;
            }

            this.hideMenu();
            this.fileSaveToProject(null);
        }
    },

    /**
     *
     * TODO
     *
     */
    fileSaveToProject: function(option) {
        let radioSelectorBlock = $("#menu-file-save-to-project-radio-block"),
            filenameEditBlock = $("#menu-file-save-to-project-name-block"),
            modelRadiobuttonField = $("#menu-file-save-to-project-type-radiobutton-smcf"),
            textRadiobuttonField = $("#menu-file-save-to-project-type-radiobutton-pml"),
            areaField = $("#menu-file-save-to-project-area"),
            filenameField = $("#menu-file-save-to-project-name"),
            extensionField = $("#menu-file-save-to-project-extension"),
            submitErrorField = $("#menu-save-to-project-error");

        this.hideContextMenu();

        if (!option) {
            $("#menu-content").show();
            $("#menu-file-save-to-project-dialog").show();

            radioSelectorBlock.hide();
            filenameEditBlock.hide();
            areaField.hide();

            if (Authorization.userEmail === null) {
                submitErrorField.text("User is not authenticated.");
                submitErrorField.css("display", "inline-block");
            } else if (Project.projectsMap === null || Project.projectsMap.size === 0) {
                submitErrorField.text("The user has no projects.");
                submitErrorField.css("display", "inline-block");
            } else {
                submitErrorField.text("");
                submitErrorField.css("display", "none");
                radioSelectorBlock.show();
                filenameEditBlock.show();
                areaField.show();
            }

            if (Environment.constructorMode === true) {
                modelRadiobuttonField.prop("checked", true);
                Gui.radiobuttonIconChanged(modelRadiobuttonField[0]);
                extensionField.text(".smcf");

                modelRadiobuttonField.change(function() {
                    Gui.radiobuttonIconChanged(this);
                    if (modelRadiobuttonField.prop("checked")) {
                        extensionField.text(".smcf");
                    }
                });

                textRadiobuttonField.change(function() {
                    Gui.radiobuttonIconChanged(this);
                    if (textRadiobuttonField.prop("checked")) {
                        extensionField.text(".pml");
                    }
                });

                radioSelectorBlock.show();
            } else if (Environment.constructorMode === false) {
                radioSelectorBlock.hide();
                extensionField.text(".pml");
            }

            filenameField.val("");
            areaField.empty();

            if (Project.projectsMap !== null && Project.projectsMap.size !== 0) {
                for (let currentProject of Project.projectsMap.keys()) {
                    let item = $("#clonable-project-item").clone();
                    item.removeAttr("id");

                    let projectIdentifier = currentProject["pk"];
                    if (projectIdentifier !== undefined)
                        item.attr("data-id", projectIdentifier);

                    let projectTitle = currentProject["fields"]["title"];
                    if (projectTitle !== undefined)
                        item.find("figcaption").text(projectTitle);

                    item.appendTo(areaField);

                    item.on("click", (event) => {
                        let target = item;

                        let fileName = filenameField.val();
                        let match = fileName.match(/[A-Za-z]\w{0,15}/i);
                        if (!match || match.length !== 1 || match[0] !== fileName) {
                            submitErrorField.text("Filename not match pattern.");
                            submitErrorField.css("display", "inline-block");
                            return;
                        }

                        let fileExtension = extensionField.text();
                        if (fileExtension !== ".pml" && fileExtension !== ".smcf") {
                            submitErrorField.text("Wrong file extension.");
                            submitErrorField.css("display", "inline-block");
                            return;
                        }

                        let projectIdentifier = target.attr("data-id");
                        if (projectIdentifier === undefined) {
                            submitErrorField.text("Project have no id field.");
                            submitErrorField.css("display", "inline-block");
                            return;
                        }

                        let fileBytes = null;
                        if (fileExtension === ".pml") {
                            fileBytes = Editor.getText();
                        } else if (Constructor.automaton) {
                            fileBytes = Constructor.automaton.toSmcf();
                        } else {
                            submitErrorField.text("Constructor is undefined.");
                            submitErrorField.css("display", "inline-block");
                            return;
                        }

                        $("#hidden-project-id").val(projectIdentifier);
                        $("#hidden-file-name").val(fileName);
                        $("#hidden-file-extension").val(fileExtension);
                        $("#hidden-bytes").val(fileBytes);
                        $("#menu-file-save-to-project-submit-form").submit();
                    });
                }
            }

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = null;
            return;
        }
    },

    /**
     *
     * TODO
     *
     */
    fileOpenFrom: function(option) {
        let submitErrorField = $("#menu-open-from-error");

        this.hideContextMenu();

        if (!option) {
            $("#menu-content").show();
            $("#menu-file-open-from-dialog").show();

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = null;
            return;
        }

        if (option === 0x1) {
            this.hideMenu();
            this.fileOpen();
            return;
        }

        if (option === 0x2) {
            if (Authorization.userEmail === null) {
                submitErrorField.text("User is not authenticated.");
                submitErrorField.css("display", "inline-block");
                return;
            }

            this.hideMenu();
            this.fileOpenFromProject(null)
        }
    },

    /**
     *
     * TODO
     *
     */
    fileOpenFromProject: function(option) {
        let backButtonField = $("#menu-open-from-project-back-button"),
            areaField = $("#menu-open-from-project-area"),
            submitErrorField = $("#menu-open-from-project-error");

        this.hideContextMenu();

        if (!option) {
            $("#menu-content").show();
            $("#menu-file-open-from-project-dialog").show();

            areaField.hide();

            if (Authorization.userEmail === null) {
                submitErrorField.text("User is not authenticated.");
                submitErrorField.css("display", "inline-block");
            } else if (Project.projectsMap === null || Project.projectsMap.size === 0) {
                submitErrorField.text("The user has no projects.");
                submitErrorField.css("display", "inline-block");
            } else {
                submitErrorField.text("");
                submitErrorField.css("display", "none");
                areaField.show();
            }

            areaField.empty();

            if (Project.projectsMap !== null && Project.projectsMap.size !== 0) {
                this.setProjectObjects(areaField, backButtonField);
            }

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = null;
            return;
        }
    },

    setProjectObjects: function(areaField, backButtonField) {
        areaField.empty();
        backButtonField.hide();
        for (let currentProject of Project.projectsMap.keys()) {
            let item = $("#clonable-project-item").clone();
            item.removeAttr("id");

            let projectIdentifier = currentProject["pk"];
            if (projectIdentifier !== undefined)
                item.attr("data-id", projectIdentifier);

            let projectTitle = currentProject["fields"]["title"];
            if (projectTitle !== undefined)
                item.find("figcaption").text(projectTitle);

            item.appendTo(areaField);

            item.on("dblclick", (event) => {
                let projectId = item.attr("data-id");
                this.setFileObjects(areaField, backButtonField, projectId);

                item.off("dblclick");
            });
        }
    },

    setFileObjects: function(areaField, backButtonField, projectId) {
        areaField.empty();
        backButtonField.show();

        backButtonField.on("click", (event) => {
            this.setProjectObjects(areaField, backButtonField);
        });

        for (let [key, value] of Project.projectsMap) {
            if (key["pk"].toString() === projectId) {
                for (let currentFile of value) {
                    let item = $("#clonable-file-item").clone();
                    item.removeAttr("id");

                    let fileIdentifier = currentFile["pk"];
                    if (fileIdentifier !== undefined)
                        item.attr("data-id", fileIdentifier);

                    let fileName = currentFile["fields"]["name"];
                    let fileExtension = currentFile["fields"]["extension"];
                    if (fileName !== undefined && fileExtension !== undefined)
                        item.find("figcaption").text(fileName + fileExtension);

                    item.appendTo(areaField);

                    item.on("dblclick", (event) => {
                        let fileId = item.attr("data-id");
                        window.location.href = "/?id=" + fileId;
                    });
                }
            }
        }
    },


    /**
     *
     * If option === null, shows menu dialog.
     * If option !== null, sets all environment variables to null. After that hides menu and restores context.
     *
     * @param option - Integer selector.
     * @type {Number}
     *
     * @author Boyarkin Nikita
     *
     **/
    fileClose: function(option) {
        this.hideContextMenu();

        if (!option) {
            $("#menu-content").show();
            $("#menu-file-close-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = null;
            return;
        }

        log(LogLevel.INFO, "Close file.");

        Constructor.resetSvg();
        Environment.resetEnvironment();

        this.hideMenu();

        Interaction.restoreContext();
    },

    /**
     *
     * TODO
     *
     */
    constructorNewProcess: function(option) {
        let processNameField = $("#menu-constructor-new-process-name"),
            submitErrorField = $("#menu-constructor-new-process-submit-error");

        this.hideContextMenu();

        if (!option) {
            processNameField.val("");

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            $("#menu-content").show();
            $("#menu-constructor-new-process-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = $("#menu-constructor-new-process-submit-button");
            return;
        }

        let processName = processNameField.val();

        let check = Constructor.checkProcessName(processName);
        switch (check) {
            case 0x0:
                Constructor.createProcess(processName);
                this.hideMenu();
                break;

            case 0x1:
                submitErrorField.text("Wrong process name.");
                submitErrorField.css("display", "inline-block");
                break;

            case 0x2:
                submitErrorField.text("Process with this name already exists.");
                submitErrorField.css("display", "inline-block");
                break;
        }
    },

    /**
     *
     * TODO
     *
     */
    constructorRenameProcess: function(option, processName) {
        let beforeProcessField = $("#menu-constructor-rename-process-old-name"),
            renameProcessField = $("#menu-constructor-rename-process-new-name"),
            submitErrorField = $("#menu-constructor-rename-process-submit-error");

        this.hideContextMenu();

        if (!option && processName) {
            beforeProcessField.text(processName);

            renameProcessField.val(processName);

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            $("#menu-content").show();
            $("#menu-constructor-rename-process-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = $("#menu-constructor-rename-process-submit-button");
            return;
        }

        let oldProcessName = beforeProcessField.text();
        let newProcessName = renameProcessField.val();

        let check = Constructor.checkProcessName(newProcessName);
        switch (check) {
            case 0x0:
                Constructor.renameProcess(oldProcessName, newProcessName);
                this.hideMenu();
                break;

            case 0x1:
                submitErrorField.text("Wrong process name.");
                submitErrorField.css("display", "inline-block");
                break;

            case 0x2:
                submitErrorField.text("Process with this name already exists.");
                submitErrorField.css("display", "inline-block");
                break;
        }
    },

    /**
     *
     * TODO
     *
     */
    constructorChangeProcessContext: function(option, processName) {
        let captionField = $("#menu-constructor-change-process-context-caption"),
            textContainerField = $("#menu-constructor-change-process-context-text-container"),
            processHeaderField = $("#menu-constructor-change-process-context-process-header"),
            processParametersField = $("#menu-constructor-change-process-context-process-parameters"),
            startBracketField = $("#menu-constructor-change-process-context-start-bracket"),
            processContextField = $("#menu-constructor-change-process-context-process-context"),
            automatonSourceField = $("#menu-constructor-change-process-context-automaton-source"),
            submitErrorField = $("#menu-constructor-change-process-context-submit-error"),
            submitButtonField = $("#menu-constructor-change-process-context-submit-button");

        this.hideContextMenu();

        if (!option && processName) {
            captionField.text(processName);

            textContainerField.show();
            submitButtonField.show();

            let context = Constructor.automaton.contextFormer(processName);
            switch (context[0]) {
                case 0x0:
                    processHeaderField.text(context[1]);
                    processParametersField.text(context[2]);
                    startBracketField.text(context[3]);
                    processContextField.text(context[4]);
                    automatonSourceField.text(context[5]);

                    submitErrorField.text("");
                    submitErrorField.css("display", "none");
                    break;

                case 0x1:
                    submitErrorField.text("Impossible to find process.");
                    submitErrorField.css("display", "inline-block");
                    textContainerField.hide();
                    submitButtonField.hide();
                    break;

                case 0x2:
                    submitErrorField.text("There is not a single initial state.");
                    submitErrorField.css("display", "inline-block");
                    textContainerField.hide();
                    submitButtonField.hide();
                    break;
            }

            $("#menu-content").show();
            $("#menu-constructor-change-process-context-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = $("#menu-constructor-change-process-context-submit-button");
            return;
        }

        processName = captionField.text();
        let processParameters = processParametersField.text();
        let processContext = processContextField.text();

        Constructor.changeProcessContext(processName, processParameters, processContext);
        this.hideMenu();
    },

    /**
     *
     * TODO
     *
     */
    constructorNewState: function(option, processName) {
        let captionElement = $("#menu-constructor-new-state-caption"),
            stateNameField = $("#menu-constructor-new-state-name"),
            submitErrorField = $("#menu-constructor-new-state-submit-error");

        this.hideContextMenu();

        if (!option && processName) {
            captionElement.text(processName);

            stateNameField.val("");

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            $("#menu-content").show();
            $("#menu-constructor-new-state-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = $("#menu-constructor-new-state-submit-button");
            return;
        }

        processName = captionElement.text();
        let stateName = stateNameField.val();

        let check = Constructor.checkStateName(processName, stateName);
        switch (check) {
            case 0x0:
                Constructor.createState(processName, stateName);
                this.hideMenu();
                break;

            case 0x1:
                submitErrorField.text("Wrong state name.");
                submitErrorField.css("display", "inline-block");
                break;

            case 0x2:
                submitErrorField.text("State with this name already exists.");
                submitErrorField.css("display", "inline-block");
                break;
        }
    },

    /**
     *
     * TODO
     *
     */
    constructorRenameState: function(option, processName, stateName) {
        let beforeStateField = $("#menu-rename-state-old-name"),
            renameStateField = $("#menu-rename-state-new-name"),
            submitErrorField = $("#menu-rename-state-submit-error");

        this.hideContextMenu();

        if (!option && processName && stateName) {
            beforeStateField.text(processName + " { " + stateName + " }");
            beforeStateField.data("process-name", processName);
            beforeStateField.data("state-name", stateName);

            renameStateField.val(stateName);

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            $("#menu-content").show();
            $("#menu-constructor-rename-state-dialog").show();
            return;
        }

        processName = beforeStateField.data("process-name");
        let oldStateName = beforeStateField.data("state-name");
        let newStateName = renameStateField.val();

        let check = Constructor.checkStateName(processName, newStateName);
        switch (check) {
            case 0x0:
                Constructor.renameState(processName, oldStateName, newStateName);
                this.hideMenu();
                break;

            case 0x1:
                submitErrorField.text("Wrong state name.");
                submitErrorField.css("display", "inline-block");
                break;

            case 0x2:
                submitErrorField.text("State with this name already exists.");
                submitErrorField.css("display", "inline-block");
                break;
        }
    },

    /**
     *
     * TODO
     *
     */
    constructorNewTransition: function(option, processName) {
        let captionElement = $("#menu-constructor-new-transition-caption"),
            comboboxFirstState = $("#menu-constructor-new-transition-from-state-combobox"),
            comboboxSecondState = $("#menu-constructor-new-transition-to-state-combobox"),
            conditionField = $("#menu-constructor-new-transition-condition"),
            submitErrorField = $("#menu-constructor-new-transition-submit-error");

        this.hideContextMenu();

        if (!option && processName) {
            captionElement.text(processName);

            comboboxFirstState.empty();
            comboboxSecondState.empty();

            comboboxFirstState.append(new Option("Select first state...", 0));
            comboboxSecondState.append(new Option("Select second state...", 0));

            let index = 0;
            let states = Constructor.automaton.findAllStatesForProcess(processName);
            for (let current of states) {
                ++index;
                comboboxFirstState.append(new Option(current.stateName, index));
                comboboxSecondState.append(new Option(current.stateName, index));
            }

            conditionField.val("");

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            $("#menu-content").show();
            $("#menu-constructor-new-transition-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = $("#menu-constructor-new-transition-submit-button");
            return;
        }

        if (comboboxFirstState.val() === "0" || comboboxSecondState.val() === "0") {
            submitErrorField.text("Please select a state.");
            submitErrorField.css("display", "inline-block");
            return;
        }

        processName = captionElement.text();
        let firstStateName = comboboxFirstState.children(":selected").text();
        let secondStateName = comboboxSecondState.children(":selected").text();

        let condition = conditionField.val();
        let check = Constructor.checkTransition(processName, firstStateName, secondStateName, condition);
        switch (check) {
            case 0x0:
                Constructor.createTransition(processName, firstStateName, secondStateName, condition);
                this.hideMenu();
                break;

            case 0x1:
                submitErrorField.text("Wrong condition.");
                submitErrorField.css("display", "inline-block");
                break;

            case 0x2:
                submitErrorField.text("This transition is already exists.");
                submitErrorField.css("display", "inline-block");
                break;
        }
    },

    /**
     *
     * TODO
     *
     */
    constructorNewTransitionFromState: function(option, processName, firstStateName) {
        let captionElement = $("#menu-constructor-new-transition-from-state-caption"),
            comboboxSecondState = $("#menu-constructor-new-transition-from-state-to-state-combobox"),
            conditionField = $("#menu-constructor-new-transition-from-state-condition"),
            submitErrorField = $("#menu-constructor-new-transition-from-state-submit-error");

        this.hideContextMenu();

        if (!option && processName && firstStateName) {
            captionElement.text(processName + " { " + firstStateName + " }");
            captionElement.data("process-name", processName);
            captionElement.data("from-state-name", firstStateName);

            comboboxSecondState.empty();
            comboboxSecondState.append(new Option("Select second state...", 0));

            let index = 0;
            let states = Constructor.automaton.findAllStatesForProcess(processName);
            for (let current of states)
                if (current.stateName !== firstStateName)
                    comboboxSecondState.append(new Option(current.stateName, ++index));

            conditionField.val("");

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            $("#menu-content").show();
            $("#menu-constructor-new-transition-from-state-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = $("#menu-constructor-new-transition-from-state-submit-button");
            return;
        }

        if (comboboxSecondState.val() === "0") {
            submitErrorField.text("Please select a state.");
            submitErrorField.css("display", "inline-block");
            return;
        }

        processName = captionElement.data("process-name");
        firstStateName = captionElement.data("from-state-name");
        let secondStateName = comboboxSecondState.children(":selected").text();
        let condition = conditionField.val();

        let check = Constructor.checkTransition(processName, firstStateName, secondStateName, condition);
        switch (check) {
            case 0x0:
                Constructor.createTransition(processName, firstStateName, secondStateName, condition);
                this.hideMenu();
                break;

            case 0x1:
                submitErrorField.text("Wrong condition.");
                submitErrorField.css("display", "inline-block");
                break;

            case 0x2:
                submitErrorField.text("This transition is already exists.");
                submitErrorField.css("display", "inline-block");
                break;
        }
    },

    /**
     *
     * TODO
     *
     */
    constructorChangeConditionForTransition: function(option, processName, firstStateName, secondStateName, condition) {
        let beforeConditionField = $("#menu-constructor-change-condition-for-transition-old-condition"),
            afterConditionField = $("#menu-constructor-change-condition-for-transition-new-condition"),
            submitErrorField = $("#menu-constructor-change-condition-for-transition-submit-error");

        this.hideContextMenu();

        if (!option && processName && firstStateName && secondStateName && condition) {
            beforeConditionField.text(processName + " { " + firstStateName + " -> " + secondStateName + " : " + condition + " }");
            beforeConditionField.data("process-name", processName);
            beforeConditionField.data("first-state-name", firstStateName);
            beforeConditionField.data("second-state-name", secondStateName);
            beforeConditionField.data("condition", condition);

            afterConditionField.val(condition);

            submitErrorField.text("");
            submitErrorField.css("display", "none");

            $("#menu-content").show();
            $("#menu-constructor-change-condition-for-transition-dialog").show();

            Dialog.dialogDisplayed = true;
            Dialog.dialogSubmitButton = $("#menu-constructor-change-condition-for-transition-submit-button");
            return;
        }

        processName = beforeConditionField.data("process-name");
        firstStateName = beforeConditionField.data("first-state-name");
        secondStateName = beforeConditionField.data("second-state-name");
        let oldCondition = beforeConditionField.data("condition");
        let newCondition = afterConditionField.val();

        let check = Constructor.checkTransition(processName, firstStateName, secondStateName, newCondition);
        switch (check) {
            case 0x0:
                Constructor.changeConditionForTransition(processName, firstStateName, secondStateName, oldCondition, newCondition);
                this.hideMenu();
                break;

            case 0x1:
                submitErrorField.text("Wrong condition.");
                submitErrorField.css("display", "inline-block");
                break;

            case 0x2:
                submitErrorField.text("This transition is already exists.");
                submitErrorField.css("display", "inline-block");
                break;
        }
    },

    /**
     *
     * TODO
     *
     */
    hideContextMenu: function() {
        $("#context-block").hide();
        $("#on-process-context-menu").hide();
        $("#on-state-context-menu").hide();
        $("#on-transition-context-menu").hide();
        $("#on-svg-context-menu").hide();
        $("#on-area-context-menu").hide();

        Dialog.contextDisplayed = false;
    },

    /**
     *
     * TODO
     *
     */
    showProcessContextMenu: function(x, y, processName) {
        let contextMenu = $("#on-process-context-menu"),
            contextBlock = $("#context-block");

        $("#on-process-context-menu-change-context").attr("href", "javascript:Menu.constructorChangeProcessContext(null, '" + processName + "')");
        $("#on-process-context-menu-delete").attr("href", "javascript:Constructor.removeProcess('" + processName + "')");
        $("#on-process-context-menu-rename").attr("href", "javascript:Menu.constructorRenameProcess(null, '" + processName + "')");

        // TODO context

        contextMenu.css({left: x.toString() + "px", top: y.toString() + "px"});
        contextMenu.show();
        contextBlock.show();

        Dialog.contextDisplayed = true;
    },

    /**
     *
     * TODO
     *
     */
    showStateContextMenu: function(x, y, processName, stateName) {
        let contextMenu = $("#on-state-context-menu"),
            contextBlock = $("#context-block");

        $("#on-state-context-menu-new-transition").attr("href", "javascript:Menu.constructorNewTransitionFromState(null, '" + processName + "', '" + stateName + "')");
        $("#on-state-context-menu-delete").attr("href", "javascript:Constructor.removeState('" + processName + "', '" + stateName + "')");
        $("#on-state-context-menu-initial").attr("href", "javascript:Constructor.changeStateInitial('" + processName + "', '" + stateName + "')");
        $("#on-state-context-menu-rename").attr("href", "javascript:Menu.constructorRenameState(null, '" + processName + "', '" + stateName + "')");

        contextMenu.css({left: x.toString() + "px", top: y.toString() + "px"});
        contextMenu.show();
        contextBlock.show();

        Dialog.contextDisplayed = true;
    },

    /**
     *
     * TODO
     *
     */
    showTransitionContextMenu: function(x, y, processName, firstStateName, secondStateName, condition) {
        let contextMenu = $("#on-transition-context-menu"),
            contextBlock = $("#context-block");

        $("#on-transition-context-menu-delete").attr("href", "javascript:Constructor.removeTransition('" + processName + "', '" + firstStateName + "', '" + secondStateName + "', '" + JSON.parse(condition) + "')");
        $("#on-transition-context-menu-change-condition").attr("href", "javascript:Menu.constructorChangeConditionForTransition(null, '" + processName + "', '" + firstStateName + "', '" + secondStateName + "', '" + JSON.parse(condition) + "')");

        contextMenu.css({left: x.toString() + "px", top: y.toString() + "px"});
        contextMenu.show();
        contextBlock.show();

        Dialog.contextDisplayed = true;
    },

    /**
     *
     * TODO
     *
     */
    showSvgContextMenu: function(x, y, processName) {
        let contextMenu = $("#on-svg-context-menu"),
            contextBlock = $("#context-block");

        $("#on-svg-context-menu-new-state").attr("href", "javascript:Menu.constructorNewState(null, '" + processName + "')");
        $("#on-svg-context-menu-new-transition").attr("href", "javascript:Menu.constructorNewTransition(null, '" + processName + "')");

        contextMenu.css({left: x.toString() + "px", top: y.toString() + "px"});
        contextMenu.show();
        contextBlock.show();

        Dialog.contextDisplayed = true;
    },

    /**
     *
     * TODO
     *
     */
    showAreaContextMenu: function(x, y) {
        let contextMenu = $("#on-area-context-menu"),
            contextBlock = $("#context-block");

        contextMenu.css({left: x.toString() + "px", top: y.toString() + "px"});
        contextMenu.show();
        contextBlock.show();

        Dialog.contextDisplayed = true;
    }
};