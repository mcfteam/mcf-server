const TEST = true;


let Test = {
    PROCESS_DIMENSION: 10,
    STATE_DIMENSION: 30,
    TRANSITION_DIMENSION: 50,

    compareAutomation: function(firstAutomation, secondAutomation) {
        if (firstAutomation.processes.size !== secondAutomation.processes.size)
            return false;

        for (let firstProcess of firstAutomation.processes.values()) {
            let secondProcess = secondAutomation.findProcess(firstProcess.processName);
            if (secondProcess === null)
                return false;

            if (firstProcess.x !== secondProcess.x || firstProcess.y !== secondProcess.y ||
                firstProcess.isCreated !== secondProcess.isCreated)
                return false;

            if (firstProcess.states.size !== secondProcess.states.size)
                return false;

            for (let firstState of firstProcess.states.values()) {
                let secondState = secondAutomation.findState(firstState.processName, firstState.stateName);
                if (secondState === null)
                    return false;

                if (firstState.x !== secondState.x || firstState.y !== secondState.y || firstState.isCreated !== secondState.isCreated ||
                    firstState.isInitial !== secondState.isInitial)
                    return false;
            }

            if (firstProcess.transitions.size !== secondProcess.transitions.size)
                return false;

            for (let firstTransition of firstProcess.transitions.values()) {
                let secondTransition = secondAutomation.findTransition(firstTransition.processName, firstTransition.firstStateName, firstTransition.secondStateName, firstTransition.condition);
                if (secondTransition === null)
                    return false;

                if (firstTransition.firstX !== secondTransition.firstX || firstTransition.secondX !== secondTransition.secondX ||
                    firstTransition.firstY !== secondTransition.firstY || firstTransition.secondY !== secondTransition.secondY ||
                    firstTransition.isCreated !== secondTransition.isCreated || firstTransition.isInitial !== secondTransition.isInitial)
                    return false;
            }
        }

        return true;
    },

    createProcesses: function(dimension, testAutomation, mainAutomation) {
        for (let processIndex = 0; processIndex < dimension; ++processIndex) {
            let processName = "some_process_" + processIndex;
            let svgRoot = $("#constructor-root");
            let x = this.getRandomInteger(0, Constructor.AREA_SIZE - Constructor.PROCESS_DEFAULT_WIDTH);
            let y = this.getRandomInteger(0, Constructor.AREA_SIZE - Constructor.PROCESS_DEFAULT_HEIGHT);

            if (testAutomation) {
                let process = new Process(processName, x, y, svgRoot);
                process.create();
                testAutomation.processes.set(processName, process);
            }

            mainAutomation.executeCommand(new CreateProcessCommand(processName, x, y, svgRoot));
        }
    },

    createStates: function(dimension, testAutomation, mainAutomation) {
        for (let currentProcess of mainAutomation.processes.values()) {
            for (let stateIndex = 0; stateIndex < dimension; ++stateIndex) {
                let processName = currentProcess.processName;
                let stateName = "some_state_" + stateIndex;
                let svgRoot = currentProcess.insideSvg;
                let x = this.getRandomInteger(Constructor.SVG_STATE_RADIUS, Constructor.SVG_SIZE - Constructor.SVG_STATE_RADIUS);
                let y = this.getRandomInteger(Constructor.SVG_STATE_RADIUS, Constructor.SVG_SIZE - Constructor.SVG_STATE_RADIUS);

                if (testAutomation) {
                    let state = new State(processName, stateName, x, y, svgRoot);
                    state.create();

                    let process = testAutomation.findProcess(processName);
                    process.states.set(stateName, state);
                }

                mainAutomation.executeCommand(new CreateStateCommand(processName, stateName, x, y));
            }
        }
    },

    createTransitions: function(dimension, testAutomation, mainAutomation) {
        for (let currentProcess of mainAutomation.processes.values()) {
            let statesArray = mainAutomation.findAllStatesForProcess(currentProcess.processName);
            for (let transitionIndex = 0; transitionIndex < dimension; ++transitionIndex) {
                let processName = currentProcess.processName;
                let firstState = statesArray[this.getRandomInteger(0, statesArray.length - 1)];
                let secondState = statesArray[this.getRandomInteger(0, statesArray.length - 1)];
                let condition = JSON.stringify("some_condition_" + transitionIndex);
                let svgRoot = currentProcess.insideSvg;
                let selectedMarker = currentProcess.selectedMarker;
                let unselectedMarker = currentProcess.unselectedMarker;

                if (testAutomation) {
                    let transition = new Transition(processName, firstState.stateName, firstState.x, firstState.y, secondState.stateName, secondState.x, secondState.y,
                        condition, svgRoot, selectedMarker, unselectedMarker);
                    transition.create();

                    let process = testAutomation.findProcess(processName);
                    process.transitions.set(Transition.toKey(firstState.stateName, secondState.stateName, condition), transition);
                }

                mainAutomation.executeCommand(new CreateTransitionCommand(processName, firstState.stateName, secondState.stateName, condition));
            }
        }
    },

    createRandomAutomation: function(automaton, processesCount, statesInProcessCount, transitionsInProcessCount) {
        this.createProcesses(processesCount, null, automaton);
        this.createStates(statesInProcessCount, null, automaton);
        this.createTransitions(transitionsInProcessCount, null, automaton);

        let processIndex = this.getRandomInteger(0, processesCount - 1);
        let processesArray = Array.from(automaton.processes.values());
        let currentProcess = processesArray[processIndex];

        switch (this.getRandomInteger(0, 2)) {
            case 0:
                automaton.executeCommand(new SelectProcessCommand(currentProcess.processName));
                break;

            case 1:
                let statesArray = automaton.findAllStatesForProcess(currentProcess.processName);
                let stateIndex = this.getRandomInteger(0, statesInProcessCount - 1);
                let currentState = statesArray[stateIndex];
                automaton.executeCommand(new SelectStateCommand(currentState.processName, currentState.stateName));
                break;

            case 2:
                let transitionsArray = automaton.findAllTransitionsForProcess(currentProcess.processName);
                let transitionIndex = this.getRandomInteger(0, transitionsInProcessCount - 1);
                let currentTransition = transitionsArray[transitionIndex];
                automaton.executeCommand(new SelectTransitionCommand(currentTransition.processName, currentTransition.firstStateName, currentTransition.secondStateName, currentTransition.condition));
                break;
        }

        for (let currentProcess of automaton.processes.values()) {
            if (this.getRandomInteger(0, 1) === 1) {
                let statesArray = automaton.findAllStatesForProcess(currentProcess.processName);
                let stateIndex = this.getRandomInteger(0, statesInProcessCount - 1);
                let currentState = statesArray[stateIndex];
                automaton.executeCommand(new SetInitialForStateCommand(currentState.processName, currentState.stateName));
            }

            let x = this.getRandomInteger(0, Constructor.AREA_SIZE - Constructor.PROCESS_DEFAULT_WIDTH);
            let y = this.getRandomInteger(0, Constructor.AREA_SIZE - Constructor.PROCESS_DEFAULT_HEIGHT);
            Constructor.automaton.executeCommand(new MoveProcessCommand(currentProcess.processName, x, y));

            for (let currentState of currentProcess.states.values()) {
                let x = this.getRandomInteger(Constructor.SVG_STATE_RADIUS, Constructor.SVG_SIZE - Constructor.SVG_STATE_RADIUS);
                let y = this.getRandomInteger(Constructor.SVG_STATE_RADIUS, Constructor.SVG_SIZE - Constructor.SVG_STATE_RADIUS);
                Constructor.automaton.executeCommand(new MoveStateCommand(currentState.processName, currentState.stateName, x, y));
            }
        }

        return $.extend(true, Object.create(Object.getPrototypeOf(automaton)), automaton);
    },

    getRandomInteger: function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
};


QUnit.module("Spin");


QUnit.test("Create process test #1", (assert) => {
    log(LogLevel.INFO, "'Create process #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create process #1' test finished.");
});


QUnit.test("Create process test #2", (assert) => {
    log(LogLevel.INFO, "'Create process #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);

    for (let index = 0; index < Test.PROCESS_DIMENSION; ++index)
        Constructor.automaton.undo();

    for (let index = 0; index < Test.PROCESS_DIMENSION; ++index)
        Constructor.automaton.redo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create process #2' test finished.");
});


QUnit.test("Create process test #3", (assert) => {
    log(LogLevel.INFO, "'Create process #3' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, null, Constructor.automaton);

    for (let index = 0; index < Test.PROCESS_DIMENSION; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create process #3' test finished.");
});


QUnit.test("Remove process test #1", (assert) => {
    log(LogLevel.INFO, "'Remove process #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, null, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, null, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, null, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        Constructor.automaton.executeCommand(new RemoveProcessCommand(currentProcess.processName));

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove process #1' test finished.");
});


QUnit.test("Remove process test #2", (assert) => {
    log(LogLevel.INFO, "'Remove process #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        Constructor.automaton.executeCommand(new RemoveProcessCommand(currentProcess.processName));

    for (let index = 0; index < Test.PROCESS_DIMENSION; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove process #2' test finished.");
});


QUnit.test("Remove process test #3", (assert) => {
    log(LogLevel.INFO, "'Remove process #3' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, null, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, null, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, null, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        Constructor.automaton.executeCommand(new RemoveProcessCommand(currentProcess.processName));

    for (let index = 0; index < Test.PROCESS_DIMENSION; ++index)
        Constructor.automaton.undo();

    for (let index = 0; index < Test.PROCESS_DIMENSION; ++index)
        Constructor.automaton.redo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove process #3' test finished.");
});


QUnit.test("Rename process test #1", (assert) => {
    log(LogLevel.INFO, "'Rename process #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);

    let index = 0;
    let processNames = Array.from(Constructor.automaton.processes.keys());
    for (let currentProcessName of processNames)
        Constructor.automaton.executeCommand(new RenameProcessCommand(currentProcessName, "renamed_process_" + index++));

    assert.ok(!Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Rename process #1' test finished.");
});


QUnit.test("Rename process test #2", (assert) => {
    log(LogLevel.INFO, "'Rename process #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);

    let index = 0;
    let processNames = Array.from(Constructor.automaton.processes.keys());
    for (let currentProcessName of processNames)
        Constructor.automaton.executeCommand(new RenameProcessCommand(currentProcessName, "renamed_process_" + index++));

    for (let index = 0; index < Test.PROCESS_DIMENSION; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Rename process #2' test finished.");
});


QUnit.test("Rename process test #3", (assert) => {
    log(LogLevel.INFO, "'Rename process #3' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    let index = 0;
    let processNames = Array.from(Constructor.automaton.processes.keys());
    for (let currentProcessName of processNames)
        Constructor.automaton.executeCommand(new RenameProcessCommand(currentProcessName, "renamed_process_" + index++));

    for (let [processKey, processValue] of Constructor.automaton.processes) {
        if (!processKey.startsWith("renamed_process_") || processValue.processName !== processKey) {
            assert.ok(false, "Test passed!");
            Constructor.resetSvg();
            log(LogLevel.INFO, "'Rename process #3' test finished.");
            return;
        }

        for (let currentState of processValue.states.values())
            if (!currentState.processName.startsWith("renamed_process_")) {
                assert.ok(false, "Test passed!");
                Constructor.resetSvg();
                log(LogLevel.INFO, "'Rename process #3' test finished.");
                return;
            }

        for (let currentTransition of processValue.transitions.values())
            if (!currentTransition.processName.startsWith("renamed_process_")) {
                assert.ok(false, "Test passed!");
                Constructor.resetSvg();
                log(LogLevel.INFO, "'Rename process #3' test finished.");
                return;
            }
    }

    assert.ok(true, "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Rename process #3' test finished.");
});


QUnit.test("Move process test #1", (assert) => {
    log(LogLevel.INFO, "'Move process #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);

    let countOfMoves = 1e3;
    for (let currentProcess of Constructor.automaton.processes.values()) {
        for (let index = 0; index < countOfMoves; ++index) {
            let x = Test.getRandomInteger(0, Constructor.AREA_SIZE - Constructor.PROCESS_DEFAULT_WIDTH);
            let y = Test.getRandomInteger(0, Constructor.AREA_SIZE - Constructor.PROCESS_DEFAULT_HEIGHT);
            Constructor.automaton.executeCommand(new MoveProcessCommand(currentProcess.processName, x, y));
        }
    }

    for (let index = 0; index < Test.PROCESS_DIMENSION; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Move process #1' test finished.");
});


QUnit.test("Create state test #1", (assert) => {
    log(LogLevel.INFO, "'Create state #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create state #1' test finished.");
});


QUnit.test("Create state test #2", (assert) => {
    log(LogLevel.INFO, "'Create state #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);

    let commandsCount = Test.PROCESS_DIMENSION + Test.PROCESS_DIMENSION * Test.STATE_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.redo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create state #3' test finished.");
});


QUnit.test("Create state test #3", (assert) => {
    log(LogLevel.INFO, "'Create state #3' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, null, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, null, Constructor.automaton);

    let commandsCount = Test.PROCESS_DIMENSION + Test.PROCESS_DIMENSION * Test.STATE_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create state #3' test finished.");
});


QUnit.test("Remove state test #1", (assert) => {
    log(LogLevel.INFO, "'Remove state #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, null, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, null, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        for (let currentState of currentProcess.states.values())
            Constructor.automaton.executeCommand(new RemoveStateCommand(currentState.processName, currentState.stateName));

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove state #1' test finished.");
});


QUnit.test("Remove state test #2", (assert) => {
    log(LogLevel.INFO, "'Remove state #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        for (let currentState of currentProcess.states.values())
            Constructor.automaton.executeCommand(new RemoveStateCommand(currentState.processName, currentState.stateName));

    let commandsCount = Test.PROCESS_DIMENSION * Test.STATE_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove state #2' test finished.");
});


QUnit.test("Remove state test #3", (assert) => {
    log(LogLevel.INFO, "'Remove state #3' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, null, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, null, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        for (let currentState of currentProcess.states.values())
            Constructor.automaton.executeCommand(new RemoveStateCommand(currentState.processName, currentState.stateName));

    let commandsCount = Test.PROCESS_DIMENSION * Test.STATE_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.redo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove state #3' test finished.");
});


QUnit.test("Rename state test #1", (assert) => {
    log(LogLevel.INFO, "'Rename state #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    let index = 0;
    for (let currentProcess of Constructor.automaton.processes.values()) {
        let statesNames = Array.from(currentProcess.states.keys());
        for (let currentStateName of statesNames)
            Constructor.automaton.executeCommand(new RenameStateCommand(currentProcess.processName, currentStateName, "renamed_state_" + index++));
    }

    assert.ok(!Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Rename state #1' test finished.");
});


QUnit.test("Rename state test #2", (assert) => {
    log(LogLevel.INFO, "'Rename state #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    let index = 0;
    for (let currentProcess of Constructor.automaton.processes.values()) {
        let statesNames = Array.from(currentProcess.states.keys());
        for (let currentStateName of statesNames)
            Constructor.automaton.executeCommand(new RenameStateCommand(currentProcess.processName, currentStateName, "renamed_state_" + index++));
    }

    let commandsCount = Test.PROCESS_DIMENSION * Test.STATE_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Rename state #3' test finished.");
});


QUnit.test("Rename state test #3", (assert) => {
    log(LogLevel.INFO, "'Rename state #3' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    let index = 0;
    for (let currentProcess of Constructor.automaton.processes.values()) {
        let statesNames = Array.from(currentProcess.states.keys());
        for (let currentStateName of statesNames)
            Constructor.automaton.executeCommand(new RenameStateCommand(currentProcess.processName, currentStateName, "renamed_state_" + index++));
    }

    for (let currentProcess of Constructor.automaton.processes.values()) {
        for (let [stateKey, stateValue] of currentProcess.states)
            if (!stateKey.startsWith("renamed_state_") || stateKey !== stateValue.stateName) {
                assert.ok(false, "Test passed!");
                Constructor.resetSvg();
                return;
            }

        for (let [transitionKey, transitionValue] of currentProcess.transitions) {
            let parameters = Transition.fromKey(transitionKey);
            if (!parameters[0].startsWith("renamed_state_") || parameters[0] !== transitionValue.firstStateName ||
                !parameters[1].startsWith("renamed_state_") || parameters[1] !== transitionValue.secondStateName) {
                assert.ok(false, "Test passed!");
                Constructor.resetSvg();
                return;
            }
        }
    }

    assert.ok(true, "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Rename state #3' test finished.");
});


QUnit.test("Move state test #1", (assert) => {
    log(LogLevel.INFO, "'Move state #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    let countOfMoves = 1e1;
    for (let currentProcess of Constructor.automaton.processes.values())
        for (let currentState of currentProcess.states.values()) {
            for (let index = 0; index < countOfMoves; ++index) {
                let x = Test.getRandomInteger(Constructor.SVG_STATE_RADIUS, Constructor.SVG_SIZE - Constructor.SVG_STATE_RADIUS);
                let y = Test.getRandomInteger(Constructor.SVG_STATE_RADIUS, Constructor.SVG_SIZE - Constructor.SVG_STATE_RADIUS);
                Constructor.automaton.executeCommand(new MoveStateCommand(currentState.processName, currentState.stateName, x, y));
            }
        }

    let commandsCount = Test.PROCESS_DIMENSION * Test.STATE_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Move state #1' test started.");
});


QUnit.test("Create transition test #1", (assert) => {
    log(LogLevel.INFO, "'Create transition #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create transition #1' test finished.");
});


QUnit.test("Create transition test #2", (assert) => {
    log(LogLevel.INFO, "'Create transition #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    let commandsCount = Test.PROCESS_DIMENSION + Test.PROCESS_DIMENSION * (Test.STATE_DIMENSION + Test.TRANSITION_DIMENSION);

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.redo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create transition #2' test finished.");
});


QUnit.test("Create transition test #3", (assert) => {
    log(LogLevel.INFO, "'Create transition #3' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, null, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, null, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, null, Constructor.automaton);

    let commandsCount = Test.PROCESS_DIMENSION + Test.PROCESS_DIMENSION * (Test.STATE_DIMENSION + Test.TRANSITION_DIMENSION);

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Create transition #3' test finished.");
});


QUnit.test("Remove transition test #1", (assert) => {
    log(LogLevel.INFO, "'Remove transition #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, null, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        for (let currentTransition of currentProcess.transitions.values())
            Constructor.automaton.executeCommand(new RemoveTransitionCommand(currentTransition.processName, currentTransition.firstStateName, currentTransition.secondStateName, currentTransition.condition));

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove transition #1' test finished.");
});


QUnit.test("Remove transition test #2", (assert) => {
    log(LogLevel.INFO, "'Remove transition #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        for (let currentTransition of currentProcess.transitions.values())
            Constructor.automaton.executeCommand(new RemoveTransitionCommand(currentTransition.processName, currentTransition.firstStateName, currentTransition.secondStateName, currentTransition.condition));

    let commandsCount = Test.PROCESS_DIMENSION * Test.TRANSITION_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove transition #2' test finished.");
});


QUnit.test("Remove transition test #3", (assert) => {
    log(LogLevel.INFO, "'Remove transition #3' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, null, Constructor.automaton);

    for (let currentProcess of Constructor.automaton.processes.values())
        for (let currentTransition of currentProcess.transitions.values())
            Constructor.automaton.executeCommand(new RemoveTransitionCommand(currentTransition.processName, currentTransition.firstStateName, currentTransition.secondStateName, currentTransition.condition));

    let commandsCount = Test.PROCESS_DIMENSION * Test.TRANSITION_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.redo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Remove transition #3' test finished.");
});


QUnit.test("Change condition for transition test #1", (assert) => {
    log(LogLevel.INFO, "'Change condition for transition #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    let index = 0;
    for (let currentProcess of Constructor.automaton.processes.values()) {
        let transitionsKeys = Array.from(currentProcess.transitions.keys());
        for (let currentTransitionKey of transitionsKeys) {
            let parameters = Transition.fromKey(currentTransitionKey);
            Constructor.automaton.executeCommand(new RemoveTransitionCommand(currentProcess.processName, parameters[0], parameters[1],
                parameters[2], "renamed_condition_" + index++));
        }
    }

    assert.ok(!Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Change condition for transition #1' test finished.");
});


QUnit.test("Change condition for transition test #2", (assert) => {
    log(LogLevel.INFO, "'Change condition for transition #2' test started.");

    Constructor.initializeAutomation();

    let testAutomation = new FiniteStateAutomaton();
    Test.createProcesses(Test.PROCESS_DIMENSION, testAutomation, Constructor.automaton);
    Test.createStates(Test.STATE_DIMENSION, testAutomation, Constructor.automaton);
    Test.createTransitions(Test.TRANSITION_DIMENSION, testAutomation, Constructor.automaton);

    let index = 0;
    for (let currentProcess of Constructor.automaton.processes.values()) {
        let transitionsKeys = Array.from(currentProcess.transitions.keys());
        for (let currentTransitionKey of transitionsKeys) {
            let parameters = Transition.fromKey(currentTransitionKey);
            Constructor.automaton.executeCommand(new RemoveTransitionCommand(currentProcess.processName, parameters[0], parameters[1],
                parameters[2], "renamed_condition_" + index++));
        }
    }

    let commandsCount = Test.PROCESS_DIMENSION * Test.TRANSITION_DIMENSION;

    for (let index = 0; index < commandsCount; ++index)
        Constructor.automaton.undo();

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Change condition for transition #2' test finished.");
});


QUnit.test("Smcf format test #1", (assert) => {
    log(LogLevel.INFO, "'Smcf format #1' test started.");

    Constructor.initializeAutomation();

    let testAutomation = Test.createRandomAutomation(Constructor.automaton, Test.PROCESS_DIMENSION, Test.STATE_DIMENSION, Test.TRANSITION_DIMENSION);

    let source = Constructor.automaton.toSmcf();

    Constructor.automaton = new FiniteStateAutomaton();
    if (Constructor.automaton.fromSmcf(source) !== 0x0) {
        assert.ok(false, "Test passed!");
        Constructor.resetSvg();
        return;
    }

    assert.ok(Test.compareAutomation(Constructor.automaton, testAutomation), "Test passed!");

    Constructor.resetSvg();

    log(LogLevel.INFO, "'Smcf format #1' test finished.");
});