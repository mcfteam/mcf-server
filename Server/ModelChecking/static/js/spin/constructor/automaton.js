class FiniteStateAutomaton {
    constructor() {
        this.processes = new Map();
        this.commands = [];
        this.undoIndex = -1;
        this.simulation = false;
        this.timeouts = null;

        Gui.blockUndo(true);
        Gui.blockRedo(true);
    }

    executeCommand(command) {
        try {
            let commandsLength = this.commands.length;
            if (this.undoIndex < commandsLength - 1) {
                this.commands.splice(this.undoIndex + 1, commandsLength - 1 - this.undoIndex);

                Gui.blockRedo(true);
            }

            if (command instanceof MoveStateCommand && this.commands.length !== 0) {
                let lastCommand = this.commands[this.commands.length - 1];
                if (lastCommand instanceof MoveStateCommand && command.processName === lastCommand.processName && command.stateName === lastCommand.stateName) {
                    lastCommand.optimizationHook(this, command.newX, command.newY);
                    return;
                }
            }

            if (command instanceof MoveProcessCommand && this.commands.length !== 0) {
                let lastCommand = this.commands[this.commands.length - 1];
                if (lastCommand instanceof MoveProcessCommand && command.processName === lastCommand.processName) {
                    lastCommand.optimizationHook(this, command.newX, command.newY);
                    return;
                }
            }

            if (command instanceof ResizeProcessCommand && commandsLength !== 0) {
                let lastCommand = this.commands[this.commands.length - 1];
                if (lastCommand instanceof ResizeProcessCommand && command.processName === lastCommand.processName) {
                    lastCommand.optimizationHook(this, command.newWidth, command.newHeight);
                    return;
                }
            }

            command.execute(this);

            this.commands.push(command);
            ++this.undoIndex;

            Gui.blockUndo(false);
        }
        catch (error) {
            log(LogLevel.ERROR, "It's impossible to execute command.");
        }
    }

    findProcess(processName) {
        let process = this.processes.get(processName);
        return (process === undefined) ? null : process;
    }

    findState(processName, stateName) {
        let process = this.processes.get(processName);
        if (process === undefined)
            return null;

        let state = process.states.get(stateName);
        return (state === undefined) ? null : state;
    }

    findTransition(processName, firstStateName, secondStateName, condition) {
        let process = this.processes.get(processName);
        if (process === undefined)
            return null;

        let transition = process.transitions.get(Transition.toKey(firstStateName, secondStateName, condition));
        return (transition === undefined) ? null : transition;
    }

    findAllTransitionsForState(processName, stateName) {
        let process = this.processes.get(processName);
        if (process === undefined)
            return null;

        let result = [];

        for (let currentTransition of process.transitions.values())
            if (currentTransition.firstStateName === stateName || currentTransition.secondStateName === stateName)
                result.push(currentTransition);

        if (result.length === 0)
            return null;

        return result;
    }

    findAllStatesForProcess(processName) {
        let process = this.processes.get(processName);
        if (process === undefined)
            return null;

        let result = [];

        for (let currentState of process.states.values())
            result.push(currentState);

        if (result.length === 0)
            return null;

        return result;
    }

    findAllTransitionsForProcess(processName) {
        let process = this.processes.get(processName);
        if (process === undefined)
            return null;

        let result = [];

        for (let currentTransition of process.transitions.values())
            result.push(currentTransition);

        if (result.length === 0)
            return null;

        return result;
    }

    findSelectedState() {
        for (let currentProcess of this.processes.values())
            for (let currentState of currentProcess.states.values())
                if (currentState.isSelected)
                    return currentState;

        return null;
    }

    findSelectedTransition() {
        for (let currentProcess of this.processes.values())
            for (let currentTransition of currentProcess.transitions.values())
                if (currentTransition.isSelected)
                    return currentTransition;

        return null;
    }

    findSelectedProcess() {
        for (let currentProcess of this.processes.values())
            if (currentProcess.isSelected)
                return currentProcess;

        return null;
    }

    findInitialState(processName) {
        let process = this.processes.get(processName);
        if (process === undefined)
            return null;

        for (let currentState of process.states.values())
            if (currentState.isInitial)
                return currentState;

        return null;
    }

    undo() {
        if (this.undoIndex !== -1)
            this.commands[this.undoIndex--].unexecute(this);

        Gui.blockRedo(false);
        Gui.blockUndo(this.undoIndex <= -1);
    }

    redo() {
        if (this.undoIndex < this.commands.length - 1)
            this.commands[++this.undoIndex].execute(this);

        Gui.blockUndo(false);
        Gui.blockRedo(this.undoIndex >= this.commands.length - 1);
    }

    toPromela() {
        let result = "";

        result += $("#application-global-context").text() + $("#application-init-start").text() + $("#application-init-content").text() + $("#application-init-end").text();

        for (let currentProcess of this.processes.values()) {
            let processContext = this.contextFormer(currentProcess.processName);
            if (processContext[0] !== 0x0)
                return processContext[0];

            result += "\n" + processContext[1] + processContext[2] + processContext[3] + processContext[4] + processContext[5];
        }

        return result;
    }

    fromPromela(promelaSource) {
        // TODO
    }

    fromTau(tauSource) {
        // TODO
    }

    toSmcf() {
        let scrollElement = $("#constructor");
        let globalContextElement = $("#application-global-context");
        let initElement = $("#application-init-content");

        let root = {
            "p": {},
            "g": globalContextElement.text(),
            "i": initElement.text(),
            "l": scrollElement.scrollLeft(),
            "t": scrollElement.scrollTop()
        };

        for (let currentProcess of this.processes.values()) {
            let currentProcessDictionary = {
                "s": {},
                "v": {},
                "p": currentProcess.processParameters,
                "c": currentProcess.processContext,
                "x": currentProcess.x,
                "y": currentProcess.y,
                "w": currentProcess.containerElement.width(),
                "h": currentProcess.containerElement.height(),
                "l": currentProcess.areaElement.scrollLeft(),
                "t": currentProcess.areaElement.scrollTop()
            };

            /*if(currentProcess.isSelected)
             currentProcessDictionary["e"] = 1;*/

            root["p"][currentProcess.processName] = currentProcessDictionary;

            for (let currentState of currentProcess.states.values()) {
                let currentStateDictionary = {
                    "x": currentState.x,
                    "y": currentState.y
                };

                /*if(currentState.isSelected)
                 currentStateDictionary["e"] = 1;*/

                if (currentState.isInitial)
                    currentStateDictionary["i"] = 1;

                root["p"][currentState.processName]["s"][currentState.stateName] = currentStateDictionary;
            }

            let index = 0;
            for (let currentTransition of currentProcess.transitions.values()) {
                let currentTransitionDictionary = {
                    "f": currentTransition.firstStateName,
                    "t": currentTransition.secondStateName,
                    "c": currentTransition.condition
                };

                /*if(currentTransition.isSelected)
                 currentTransitionDictionary["e"] = 1;*/

                root["p"][currentTransition.processName]["v"][(index++).toString()] = currentTransitionDictionary;
            }
        }

        return JSON.stringify(root);
    }

    fromSmcf(source) {
        let rootElement = $("#constructor-root");
        let scrollElement = $("#constructor");
        let globalContextElement = $("#application-global-context");
        let initElement = $("#application-init-content");

        let isNumeric = (value) => {
            return (typeof value === "number") && !isNaN(value) && isFinite(value);
        };

        let json;
        try {
            json = JSON.parse(source);
        }
        catch (error) {
            return 0x1;
        }

        if (json["p"] === undefined || json["g"] === undefined || json["i"] === undefined || json["l"] === undefined || json["t"] === undefined)
            return 0x2;

        if (!isNumeric(json["l"]) || !isNumeric(json["t"]))
            return 0x3;

        let savedContext = this.saveContext();

        globalContextElement.text(json["g"]);
        initElement.text(json["i"]);
        scrollElement.scrollLeft(json["l"]);
        scrollElement.scrollTop(json["t"]);
        scrollElement.data("scroll-left", json["l"].toString());
        scrollElement.data("scroll-top", json["t"].toString());

        let processesDictionary = json["p"];
        for (let currentProcessName in processesDictionary) {
            if (!processesDictionary.hasOwnProperty(currentProcessName))
                continue;

            let jsonProcess = processesDictionary[currentProcessName];
            if (jsonProcess["s"] === undefined || jsonProcess["v"] === undefined || jsonProcess["p"] === undefined || jsonProcess["c"] === undefined || jsonProcess["x"] === undefined ||
                jsonProcess["y"] === undefined || jsonProcess["w"] === undefined || jsonProcess["h"] === undefined || jsonProcess["l"] === undefined || jsonProcess["t"] === undefined) {
                this.restoreContext(savedContext);
                return 0x4;
            }

            if (!isNumeric(jsonProcess["x"]) || !isNumeric(jsonProcess["y"]) || !isNumeric(jsonProcess["w"]) ||
                !isNumeric(jsonProcess["h"]) || !isNumeric(jsonProcess["l"]) || !isNumeric(jsonProcess["t"])) {
                this.restoreContext(savedContext);
                return 0x5;
            }

            let createProcessCommand = new CreateProcessCommand(currentProcessName, jsonProcess["x"], jsonProcess["y"], rootElement);
            createProcessCommand.execute(this);

            let process = this.findProcess(currentProcessName);
            process.resize(jsonProcess["w"], jsonProcess["h"]);
            process.scroll(jsonProcess["l"], jsonProcess["t"]);
            process.areaElement.data("scroll-left", jsonProcess["l"].toString());
            process.areaElement.data("scroll-top", jsonProcess["t"].toString());
            process.processContext = jsonProcess["c"];
            process.processParameters = jsonProcess["p"];

            process.setFocus();

            /*if(jsonProcess["e"] !== undefined) {
             let selectProcessCommand = new SelectProcessCommand(currentProcessName);
             selectProcessCommand.execute(this);
             }*/

            let statesDictionary = jsonProcess["s"];
            for (let currentStateName in statesDictionary) {
                if (!statesDictionary.hasOwnProperty(currentStateName))
                    continue;

                let jsonState = statesDictionary[currentStateName];
                if (jsonState["x"] === undefined || jsonState["y"] === undefined) {
                    this.restoreContext(savedContext);
                    return 0x6;
                }

                if (!isNumeric(jsonState["x"]) || !isNumeric(jsonState["y"])) {
                    this.restoreContext(savedContext);
                    return 0x7;
                }

                let createStateCommand = new CreateStateCommand(currentProcessName, currentStateName, jsonState["x"], jsonState["y"]);
                createStateCommand.execute(this);

                /*if(jsonState["e"] !== undefined) {
                 let selectStateCommand = new SelectStateCommand(currentProcessName, currentStateName);
                 selectStateCommand.execute(this);
                 }*/

                if (jsonState["i"] !== undefined) {
                    let setInitialStateCommand = new SetInitialForStateCommand(currentProcessName, currentStateName);
                    setInitialStateCommand.execute(this);
                }
            }

            let transitionsDictionary = jsonProcess["v"];
            for (let currentTransitionIndex in transitionsDictionary) {
                if (!transitionsDictionary.hasOwnProperty(currentTransitionIndex))
                    continue;

                let jsonTransition = transitionsDictionary[currentTransitionIndex];
                if (jsonTransition["f"] === undefined || jsonTransition["t"] === undefined || jsonTransition["c"] === undefined) {
                    this.restoreContext(savedContext);
                    return 0x8;
                }

                let createTransitionCommand = new CreateTransitionCommand(currentProcessName, jsonTransition["f"], jsonTransition["t"], jsonTransition["c"]);
                createTransitionCommand.execute(this);

                /*if(jsonTransition["e"] !== undefined) {
                 let selectTransitionCommand = new SelectTransitionCommand(currentProcessName, jsonTransition["f"], jsonTransition["t"], jsonTransition["c"]);
                 selectTransitionCommand.execute(this);
                 }*/
            }
        }

        return 0x0;
    }

    contextFormer(processName) {
        let process = this.findProcess(processName);
        if (process === null)
            return [0x1];

        let initialState = this.findInitialState(processName);
        if (initialState === null)
            return [0x2];

        let chainArray = this.chainFinder(initialState);

        let processHeader, processParameters, startBracket, processContext, automatonSource;
        processHeader = "proctype " + processName + "(";
        processParameters = process.processParameters;
        startBracket = ") {\n";
        processContext = process.processContext;
        automatonSource = "";

        let allStatesInChain = chainArray[0];
        let allTransitionsInChain = chainArray[1];

        automatonSource += "\tgoto " + initialState.stateName + "_\n";

        for (let currentState of allStatesInChain) {
            automatonSource += currentState.stateName + "_:";

            let contains = false;
            for (let currentTransition of allTransitionsInChain) {
                if (currentTransition.firstStateName === currentState.stateName) {
                    if (!contains)
                        automatonSource += "\tdo\n";

                    automatonSource += "\t:: atomic { " + JSON.parse(currentTransition.condition) + " -> goto " + currentTransition.secondStateName + "_ }\n";
                    contains = true;
                }
            }

            automatonSource += (contains) ? "\tod\n" : "end_" + currentState.stateName + ":\n\t0\n";
        }

        automatonSource += "}\n";

        return [0x0, processHeader, processParameters, startBracket, processContext, automatonSource]
    }

    chainFinder(initialState) {
        let allStatesInChain = new Set([initialState]);
        let allTransitionsInChain = new Set();

        let changed = false;
        let addedStates = [initialState];

        let process = this.findProcess(initialState.processName);

        let notUsedTransitions = new Set();
        for (let currentTransition of process.transitions.values())
            notUsedTransitions.add(currentTransition);

        do {
            let toAnalyze = addedStates.slice();
            addedStates = [];

            for (let currentStateInChain of toAnalyze) {
                for (let currentTransitionInChain of notUsedTransitions) {
                    if (currentTransitionInChain.firstStateName === currentTransitionInChain.secondStateName) {
                        if (currentTransitionInChain.firstStateName === currentStateInChain.stateName) {
                            allTransitionsInChain.add(currentTransitionInChain);
                            changed = true;
                        }

                        notUsedTransitions.delete(currentTransitionInChain);
                        continue;
                    }

                    let stateName = (currentTransitionInChain.firstStateName === currentStateInChain.stateName) ? currentTransitionInChain.secondStateName :
                        ((currentTransitionInChain.secondStateName === currentStateInChain.stateName) ? currentTransitionInChain.firstStateName : null);

                    if (stateName === null)
                        continue;

                    let state = this.findState(initialState.processName, stateName);
                    if (!allStatesInChain.has(state)) {
                        addedStates.push(state);
                        allStatesInChain.add(state);
                    }

                    allTransitionsInChain.add(currentTransitionInChain);
                    notUsedTransitions.delete(currentTransitionInChain);

                    changed = true;
                }
            }
        } while (changed && addedStates.length !== 0);

        return [allStatesInChain, allTransitionsInChain];
    }

    saveContext() {
        let scrollElement = $("#constructor");
        let globalContextElement = $("#application-global-context");
        let initElement = $("#application-init-content");

        let savedCommands = this.commands.slice();
        let savedUndoIndex = this.undoIndex;
        let savedScroll = [scrollElement.scrollLeft(), scrollElement.scrollTop()];
        let savedContext = [globalContextElement.val(), initElement.val()];
        let savedProcesses = [];

        for (let currentProcess of this.processes.values()) {
            let command = new RemoveProcessCommand(currentProcess.processName);
            command.execute(this);
            savedProcesses.push(command);
        }

        this.commands.splice(0, this.commands.length);
        this.undoIndex = -1;

        Gui.blockUndo(true);
        Gui.blockRedo(true);

        return [savedProcesses, savedCommands, savedUndoIndex, savedScroll, savedContext];
    }

    restoreContext(savedContext) {
        for (let currentProcess of this.processes.values()) {
            let command = new RemoveProcessCommand(currentProcess.processName);
            command.execute(this);
        }

        this.processes = new Map();
        this.commands = savedContext[1];
        this.undoIndex = savedContext[2];

        for (let currentProcess of savedContext[0])
            currentProcess.unexecute(this);

        let scrollElement = $("#constructor");
        let globalContextElement = $("#application-global-context");
        let initElement = $("#application-init-content");

        scrollElement.scrollLeft(savedContext[3][0]);
        scrollElement.scrollTop(savedContext[3][1]);

        globalContextElement.val(savedContext[4][0]);
        initElement.val(savedContext[4][1]);

        if (this.undoIndex > -1)
            Gui.blockUndo(false);

        if (this.undoIndex < this.commands.length - 1)
            Gui.blockRedo(false);
    }

    shiftToDefault() {
        for (let currentProcess of this.processes.values()) {
            let command = new MoveProcessCommand(currentProcess.processName, currentProcess.x, currentProcess.y);
            command.execute(this);

            let area = currentProcess.areaElement;
            if (typeof area.data("scroll-left") !== "undefined" && typeof area.data("scroll-top") !== "undefined") {
                currentProcess.scroll(area.data("scroll-left"), area.data("scroll-top"));
                area.removeData("scroll-left");
                area.removeData("scroll-top");
            }

            for (let currentState of currentProcess.states.values()) {
                let command = new MoveStateCommand(currentState.processName, currentState.stateName, currentState.x, currentState.y);
                command.execute(this);
            }
        }
    }

    startSimulation(promelaSource, simulationLog) {
        if (this.simulation === true)
            this.stopSimulation();

        Constructor.area.disable();
        Gui.showCounter();

        this.timeouts = this.simulate(promelaSource, simulationLog);
        this.simulation = true;
    }

    stopSimulation() {
        if (this.simulation !== true)
            return;

        Gui.hideCounter();
        Constructor.area.enable();

        if (this.timeouts !== null)
            for (let currentTimeout of this.timeouts)
                clearTimeout(currentTimeout);

        if (Environment.savedConstructor !== null)
            Constructor.automaton.fromSmcf(Environment.savedConstructor);

        this.simulation = false;
        this.timeouts = null;
    }

    simulate(promelaSource, simulationLog) {
        const ENTRY_REGEXP = /\s*(\d+):\tproc\s+(\d+)\s\((.*)\)\ssource\.pml:(\d+)\s\(state\s(\d+)\)\t\[(.*)]/i;
        const STATE_REGEXP = /\s*goto\s+(.*)_\s*/i;

        let simulationArray = simulationLog.split("\n");
        let delayFormer = 1;
        let counter = 0;

        let timeouts = [];
        let previousElement = {};
        let previousState = {};

        for (let currentProcess of this.processes.values()) {
            let initialState = this.findInitialState(currentProcess.processName);
            if (initialState !== null) {
                initialState.select();
                previousElement[currentProcess.processName] = initialState;
                previousState[currentProcess.processName] = initialState;
            }
        }

        for (let currentLine of simulationArray) {
            let match = currentLine.match(ENTRY_REGEXP);

            if (!match || match.length !== 7)
                continue;

            let processName = match[3];
            let lineNumber = parseInt(match[4]);
            let command = match[6];

            let lastIndex = processName.lastIndexOf(":");
            if (lastIndex !== -1 && lastIndex !== processName.length - 1)
                processName = processName.substring(0, lastIndex);

            let process = this.findProcess(processName);
            if (process === null)
                continue;

            match = command.match(STATE_REGEXP);
            if (match && match.length === 2 && match[0] === command) {
                let stateName = match[1];

                let state = this.findState(processName, stateName);
                if (state === null)
                    continue;

                previousState[process.processName] = state;

                timeouts.push(setTimeout(() => {
                    previousElement[process.processName].deselect(true);
                    state.select(true);
                    previousElement[process.processName] = state;

                    Gui.setCounter(++counter);
                }, delayFormer++ * 1000));
            }
            else {
                let lines = promelaSource.split("\n");
                if (lines.length < lineNumber)
                    continue;

                let currentLine = lines[lineNumber - 1];
                if ((currentLine.match(/->/g) || []).length >= 2)
                    continue;

                let startIndex = 0, lastIndex = 0;
                let currentLineNumber = 1;
                for (let index = 0; index < promelaSource.length; ++index) {
                    if (currentLineNumber === lineNumber) {
                        startIndex = index;
                        break;
                    }

                    if (promelaSource.charAt(index) === "\n")
                        ++currentLineNumber;
                }

                let arrowIndex = promelaSource.indexOf("->", startIndex);
                if (arrowIndex === -1)
                    continue;

                startIndex = -1;
                for (let index = arrowIndex - 1; index >= 0; --index)
                    if (promelaSource.charAt(index) === "{") {
                        startIndex = index;
                        break;
                    }

                lastIndex = -1;
                for (let index = arrowIndex + 2; index < promelaSource.length; ++index)
                    if (promelaSource.charAt(index) === "}") {
                        lastIndex = index;
                        break;
                    }

                let fromState = previousState[process.processName];
                let toState = promelaSource.substring(arrowIndex + 2, lastIndex);
                let condition = promelaSource.substring(startIndex + 1, arrowIndex);

                match = toState.match(STATE_REGEXP);
                if (!match || match.length !== 2 || match[0] !== toState)
                    continue;

                toState = this.findState(process.processName, match[1]);
                if (toState === null)
                    continue;

                condition = condition.replace(/\s/g, "");
                if (condition.length === 0)
                    continue;

                for (let [transitionKey, transitionValue] of process.transitions.entries()) {
                    let key = Transition.fromKey(transitionKey);

                    if (key[0] !== fromState.stateName || key[1] !== toState.stateName)
                        continue;

                    if (condition === JSON.parse(key[2]).replace(/\s/g, "")) {
                        timeouts.push(setTimeout(() => {
                            previousElement[process.processName].deselect(true);
                            transitionValue.select(true);
                            previousElement[process.processName] = transitionValue;
                        }, delayFormer++ * 1000));

                        break;
                    }
                }
            }

        }

        return timeouts;
    }
}
