class Area {
    constructor() {
        this.overlay = $("#constructor-overlay");
        this.overlay.on("click", (event) => this.onOverlayLeftClick(event));
        this.overlay.on("contextmenu", (event) => this.onOverlayRightClick(event));

        this.root = $("#constructor-root");
        this.root.width(Constructor.AREA_SIZE);
        this.root.height(Constructor.AREA_SIZE);

        this.constructor = $("#constructor");
        this.constructor.scrollLeft(Constructor.AREA_SIZE / 2);
        this.constructor.scrollTop(Constructor.AREA_SIZE / 2);

        this.root.on("click", (event) => this.onLeftClick(event));
        this.root.on("contextmenu", (event) => this.onRightClick(event));
        this.root.on("mousedown", (event) => this.onMouseDown(event));
        this.root.on("mouseup", (event) => this.onMouseUp(event));
        this.root.on("mousemove", (event) => this.onMouseMove(event));
        this.root.on("mouseleave", (event) => this.onMouseLeave(event));
    }

    reset() {
        this.overlay.off("click");
        this.overlay.off("contextmenu");
        this.root.off("click");
        this.root.off("contextmenu");
        this.root.off("mousedown");
        this.root.off("mouseup");
        this.root.off("mousemove");
        this.root.off("mouseleave");
    }

    disable() {
        Gui.blockConstructorOptions();

        Constructor.enabled = false;

        this.overlay.show();
    }

    enable() {
        $("#menu-constructor-new-process-option").removeClass("unavailable-option");
        $("#new-process-button").removeClass("unavailable-button");

        Constructor.enabled = true;

        this.overlay.hide();
    }

    onOverlayLeftClick(event) {
        Constructor.automaton.stopSimulation();

        event.preventDefault();
        event.stopPropagation();
    }

    onOverlayRightClick(event) {
        if (Environment.constructorMode === true && Environment.savedConstructor !== null && Environment.operationResult !== null &&
            Environment.savedSource !== null && Constructor.automaton !== null)
            Constructor.automaton.startSimulation(Environment.savedSource, Environment.operationResult);

        event.preventDefault();
        event.stopPropagation();
    }

    onLeftClick(event) {
        let offset = this.root.offset();
        let x = event.pageX - offset.left;
        let y = event.pageY - offset.top;

        log(LogLevel.DEBUG, "Left click event (x: '" + x + "', y: '" + y + "') on area element.");

        Menu.hideContextMenu();
        Constructor.deselectAll();

        event.stopPropagation();
    }

    onRightClick(event) {
        let offset = this.root.offset();
        let x = event.pageX - offset.left;
        let y = event.pageY - offset.top;

        log(LogLevel.DEBUG, "Right click event (x: '" + x + "', y: '" + y + "') on area element.");

        Menu.hideContextMenu();
        Menu.showAreaContextMenu(event.clientX, event.clientY);

        Constructor.resetVariables();

        event.preventDefault();
        event.stopPropagation();
    }

    onMouseDown(event) {
        Gui.clearSelection();

        let offset = this.root.offset();
        let x = event.pageX - offset.left;
        let y = event.pageY - offset.top;

        log(LogLevel.DEBUG, "Mouse down event (x: '" + x + "', y: '" + y + "') on area element.");

        Menu.hideContextMenu();

        Constructor.resetVariables();
        Constructor.holdAreaX = x;
        Constructor.holdAreaY = y;
        Constructor.holdArea = true;

        event.stopPropagation();
    }

    onMouseUp(event) {
        let offset = this.root.offset();
        let x = event.pageX - offset.left;
        let y = event.pageY - offset.top;

        log(LogLevel.DEBUG, "Mouse up event (x: '" + x + "', y: '" + y + "') on area element.");

        let temp = Constructor.moved;
        Constructor.resetVariables();
        Constructor.moved = temp;

        event.stopPropagation();
    }

    onMouseMove(event) {
        let offset = this.root.offset();
        let x = event.pageX - offset.left;
        let y = event.pageY - offset.top;

        /* log(LogLevel.DEBUG, "Mouse move event (x: '" + x + "', y: '" + y + "') on area element."); */

        if (Constructor.holdArea && Constructor.holdAreaX && Constructor.holdAreaY) {
            let constructor = this.constructor;
            constructor.scrollLeft(constructor.scrollLeft() - (x - Constructor.holdAreaX));
            constructor.scrollTop(constructor.scrollTop() - (y - Constructor.holdAreaY));
        }
        else if (Constructor.movableProcess && Constructor.movableProcessX && Constructor.movableProcessY) {
            let width = Constructor.movableProcess.containerElement.width();
            let height = Constructor.movableProcess.containerElement.height();
            let newX = x - Constructor.movableProcessX;
            let newY = y - Constructor.movableProcessY;

            if (newX < 0) {
                Constructor.movableProcessX = x;
                newX = 0;
            }

            if (newX + width > Constructor.AREA_SIZE) {
                Constructor.movableProcessX = x + width - Constructor.AREA_SIZE;
                newX = Constructor.AREA_SIZE - width;
            }

            if (newY < 0) {
                Constructor.movableProcessY = y;
                newY = 0;
            }

            if (newY + height > Constructor.AREA_SIZE) {
                Constructor.movableProcessY = y + height - Constructor.AREA_SIZE;
                newY = Constructor.AREA_SIZE - height;
            }

            if (newX !== Constructor.movableProcess.x || newY !== Constructor.movableProcess.y) {
                Constructor.automaton.executeCommand(new MoveProcessCommand(Constructor.movableProcess.processName, newX, newY));
                Constructor.moved = true;
            }
        }
    }

    onMouseLeave(event) {
        log(LogLevel.DEBUG, "Mouse out event on area element.");

        Constructor.resetVariables();

        event.stopPropagation();
    }
}