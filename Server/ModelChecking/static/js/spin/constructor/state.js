class State {
    constructor(processName, stateName, x, y, rootSvg) {
        this.processName = processName;
        this.stateName = stateName;
        this.x = x;
        this.y = y;
        this.isCreated = false;
        this.isSelected = false;
        this.isInitial = false;
        this.rootSvg = rootSvg;
        this.stateSvg = null;
        this.nameSvg = null;
    }

    create() {
        if (this.isCreated)
            throw Error();

        this.stateSvg = this.rootSvg.circle(Constructor.SVG_STATE_RADIUS * 2).move(this.x - Constructor.SVG_STATE_RADIUS, this.y - Constructor.SVG_STATE_RADIUS);
        this.stateSvg.attr({"class": "unselected-state"});

        this.nameSvg = this.rootSvg.text(this.stateName);
        this.nameSvg.attr({"class": "state-name"});
        this.nameSvg.move(this.x - this.nameSvg.bbox().width / 2, this.y - this.nameSvg.bbox().height / 2);

        this.stateSvg.on("contextmenu", (event) => this.onRightClick(event));
        this.nameSvg.on("contextmenu", (event) => this.onRightClick(event));

        this.stateSvg.on("click", (event) => this.onLeftClick(event));
        this.nameSvg.on("click", (event) => this.onLeftClick(event));

        this.stateSvg.on("mousedown", (event) => this.onMouseDown(event));
        this.nameSvg.on("mousedown", (event) => this.onMouseDown(event));

        this.stateSvg.on("mouseup", (event) => this.onMouseUp(event));
        this.nameSvg.on("mouseup", (event) => this.onMouseUp(event));

        this.isCreated = true;
    }

    remove() {
        if (!this.isCreated)
            throw Error();

        this.stateSvg.off("contextmenu");
        this.nameSvg.off("contextmenu");

        this.stateSvg.off("click");
        this.nameSvg.off("click");

        this.stateSvg.off("mousedown");
        this.nameSvg.off("mousedown");

        this.stateSvg.off("mouseup");
        this.nameSvg.off("mouseup");

        this.stateSvg.remove();
        this.nameSvg.remove();

        this.isCreated = false;
        this.isSelected = false;
        this.isInitial = false;
    }

    rename(newStateName) {
        if (!this.isCreated)
            throw Error();

        this.nameSvg.text(newStateName);
        this.nameSvg.move(this.x - this.nameSvg.bbox().width / 2, this.y - this.nameSvg.bbox().height / 2);

        this.stateName = newStateName;
    }

    move(x, y) {
        if (!this.isCreated)
            throw Error();

        this.stateSvg.move(x - Constructor.SVG_STATE_RADIUS, y - Constructor.SVG_STATE_RADIUS);
        this.nameSvg.move(x - this.nameSvg.bbox().width / 2, y - this.nameSvg.bbox().height / 2);

        this.x = x;
        this.y = y;
    }

    setInitial() {
        if (!this.isCreated || this.isInitial)
            throw Error();

        this.stateSvg.attr(this.isSelected ? {"class": "selected-initial-state"} : {"class": "unselected-initial-state"});
        this.isInitial = true;
    }

    removeInitial() {
        if (!this.isCreated || !this.isInitial)
            throw Error();

        this.stateSvg.attr(this.isSelected ? {"class": "selected-state"} : {"class": "unselected-state"});
        this.isInitial = false;
    }

    select(animate) {
        if (!this.isCreated || this.isSelected)
            throw Error();

        if (animate)
            this.stateSvg.animate().attr(this.isInitial ? {"class": "selected-initial-state"} : {"class": "selected-state"});
        else
            this.stateSvg.attr(this.isInitial ? {"class": "selected-initial-state"} : {"class": "selected-state"});

        this.isSelected = true;
    }

    deselect(animate) {
        if (!this.isCreated || !this.isSelected)
            throw Error();

        if (animate)
            this.stateSvg.animate().attr(this.isInitial ? {"class": "unselected-initial-state"} : {"class": "unselected-state"});
        else
            this.stateSvg.attr(this.isInitial ? {"class": "unselected-initial-state"} : {"class": "unselected-state"});

        this.isSelected = false;
    }

    onLeftClick(event) {
        let box = this.rootSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Left click event (x: '" + x + "', y: '" + y + "') on state '" + this.stateName + "' for process '" + this.processName + "'.");

        Menu.hideContextMenu();

        if (!Constructor.moved)
            Constructor.changeStateSelection(this);

        Constructor.moved = null;

        event.stopPropagation();
    }

    onRightClick(event) {
        let box = this.rootSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Right click event (x: '" + x + "', y: '" + y + "') on state '" + this.stateName + "' for process '" + this.processName + "'.");

        Menu.hideContextMenu();
        Menu.showStateContextMenu(event.clientX, event.clientY, this.processName, this.stateName);

        Constructor.resetVariables();

        event.preventDefault();
        event.stopPropagation();
    }

    onMouseDown(event) {
        Gui.clearSelection();

        let box = this.rootSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Mouse down event (x: '" + x + "', y: '" + y + "') on state '" + this.stateName + "' for process '" + this.processName + "'.");

        Menu.hideContextMenu();

        Constructor.resetVariables();
        Constructor.movableState = this;

        event.stopPropagation();
    }

    onMouseUp(event) {
        let box = this.rootSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Mouse up event (x: '" + x + "', y: '" + y + "') on state '" + this.stateName + "' for process '" + this.processName + "'.");

        let temp = Constructor.moved;
        Constructor.resetVariables();
        Constructor.moved = temp;

        event.stopPropagation();
    }
}