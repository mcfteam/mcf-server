/**
 * TODO
 */
class Command {
    constructor() {
        this.isExecuted = false;
    }

    execute(automaton) {
        if (this.isExecuted)
            throw Error();

        this.isExecuted = true;
    }

    unexecute(automaton) {
        if (!this.isExecuted)
            throw Error();

        this.isExecuted = false;
    }
}


/**
 * TODO
 */
class CreateProcessCommand extends Command {
    constructor(processName, x, y, rootElement) {
        super();

        this.processName = processName;
        this.x = x;
        this.y = y;
        this.rootElement = rootElement;
    }

    execute(automaton) {
        super.execute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process !== null)
            throw Error();

        process = new Process(this.processName, this.x, this.y, this.rootElement);

        process.create();
        automaton.processes.set(this.processName, process);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        process.remove();
        automaton.processes.delete(this.processName);

        if (automaton.processes.size === 0) {
            Constructor.maxAltitudeIndex = 0;
            Gui.blockNewOptions();
        }
        else {
            let maxAltitudeIndex = -1;
            let focusedProcess = null;
            for (let currentProcess of automaton.processes.values())
                if (maxAltitudeIndex < currentProcess.altitudeIndex) {
                    focusedProcess = currentProcess;
                    maxAltitudeIndex = currentProcess.altitudeIndex;
                }

            if (focusedProcess !== null)
                focusedProcess.setFocus();
        }
    }
}


/**
 * TODO
 */
class RemoveProcessCommand extends Command {
    constructor(processName) {
        super();

        this.processName = processName;
        this.savedTransitions = null;
        this.savedStates = null;
        this.isSelected = null;
        this.x = null;
        this.y = null;
        this.width = null;
        this.height = null;
        this.scrollLeft = null;
        this.scrollTop = null;
        this.rootElement = null;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.savedTransitions !== null || this.savedStates !== null || this.isSelected !== null || this.x !== null || this.y !== null ||
            this.width !== null || this.height !== null || this.scrollLeft !== null || this.scrollTop !== null || this.rootElement !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        this.savedTransitions = [];
        this.savedStates = [];
        this.isSelected = process.isSelected;
        this.x = process.x;
        this.y = process.y;
        this.width = process.containerElement.width();
        this.height = process.containerElement.height();
        this.scrollLeft = process.areaElement.scrollLeft();
        this.scrollTop = process.areaElement.scrollTop();
        this.rootElement = process.rootElement;

        let states = automaton.findAllStatesForProcess(this.processName);
        if (states !== null) {
            for (let currentState of states) {
                let transitions = automaton.findAllTransitionsForState(currentState.processName, currentState.stateName);
                if (transitions !== null) {
                    for (let currentTransition of transitions) {
                        let transitionCommand = new RemoveTransitionCommand(currentTransition.processName, currentTransition.firstStateName, currentTransition.secondStateName, currentTransition.condition);
                        transitionCommand.execute(automaton);
                        this.savedTransitions.push(transitionCommand);
                    }
                }

                let stateCommand = new RemoveStateCommand(currentState.processName, currentState.stateName);
                stateCommand.execute(automaton);
                this.savedStates.push(stateCommand);
            }
        }

        process.remove();
        automaton.processes.delete(this.processName);

        if (automaton.processes.size === 0) {
            Constructor.maxAltitudeIndex = 0;
            Gui.blockNewOptions();
        }
        else {
            let maxAltitudeIndex = -1;
            let focusedProcess = null;
            for (let currentProcess of automaton.processes.values())
                if (maxAltitudeIndex < currentProcess.altitudeIndex) {
                    focusedProcess = currentProcess;
                    maxAltitudeIndex = currentProcess.altitudeIndex;
                }

            if (focusedProcess !== null)
                focusedProcess.setFocus();
        }

        if (this.isSelected)
            Gui.blockSelectedOptions();
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        if (this.savedTransitions === null || this.savedStates === null || this.isSelected === null || this.x === null || this.y === null ||
            this.width === null || this.height === null || this.scrollLeft === null || this.scrollTop === null || this.rootElement === null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process !== null)
            throw Error();

        process = new Process(this.processName, this.x, this.y, this.rootElement);
        process.create();

        if (this.isSelected) {
            process.select();
            Gui.unblockSelectedOptionsByProcess(process.processName);
        }

        process.resize(this.width, this.height);
        process.scroll(this.scrollLeft, this.scrollTop);
        process.setFocus();

        automaton.processes.set(this.processName, process);

        for (let current of this.savedStates)
            current.unexecute(automaton);

        for (let current of this.savedTransitions)
            current.unexecute(automaton);

        this.savedTransitions = null;
        this.savedStates = null;
        this.isSelected = null;
        this.x = null;
        this.y = null;
        this.width = null;
        this.height = null;
        this.scrollLeft = null;
        this.scrollTop = null;
        this.rootElement = null;
    }
}


/**
 * TODO
 */
class SelectProcessCommand extends Command {
    constructor(processName) {
        super();

        this.processName = processName;
        this.savedSelection = null;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.savedSelection !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let selectedState = automaton.findSelectedState();
        if (selectedState === null) {
            let selectedTransition = automaton.findSelectedTransition();
            if (selectedTransition === null) {
                let selectedProcess = automaton.findSelectedProcess();
                if (selectedProcess !== null)
                    this.savedSelection = new DeselectProcessCommand(selectedProcess.processName);
            }
            else
                this.savedSelection = new DeselectTransitionCommand(selectedTransition.processName, selectedTransition.firstStateName, selectedTransition.secondStateName, selectedTransition.condition);
        }
        else
            this.savedSelection = new DeselectStateCommand(selectedState.processName, selectedState.stateName);

        if (this.savedSelection)
            this.savedSelection.execute(automaton);

        process.select();
        Gui.unblockSelectedOptionsByProcess(this.processName);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        process.deselect();
        Gui.blockSelectedOptions();

        if (this.savedSelection !== null) {
            this.savedSelection.unexecute(automaton);
            this.savedSelection = null;
        }
    }
}


/**
 * TODO
 */
class DeselectProcessCommand extends Command {
    constructor(processName) {
        super();

        this.processName = processName;
    }

    execute(automaton) {
        super.execute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        process.deselect();
        Gui.blockSelectedOptions();
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        process.select();
        Gui.unblockSelectedOptionsByProcess(this.processName);
    }
}


/**
 * TODO
 */
class RenameProcessCommand extends Command {
    constructor(oldProcessName, newProcessName) {
        super();

        this.oldProcessName = oldProcessName;
        this.newProcessName = newProcessName;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.newProcessName === this.oldProcessName)
            throw Error();

        let oldProcess = automaton.findProcess(this.oldProcessName);
        if (oldProcess === null)
            throw Error();

        let newProcess = automaton.findProcess(this.newProcessName);
        if (newProcess !== null)
            throw Error();

        let states = automaton.findAllStatesForProcess(this.oldProcessName);
        if (states !== null)
            for (let current of states)
                current.processName = this.newProcessName;

        let transitions = automaton.findAllTransitionsForProcess(this.oldProcessName);
        if (transitions !== null)
            for (let current of transitions)
                current.processName = this.newProcessName;

        automaton.processes.delete(oldProcess.processName);
        oldProcess.rename(this.newProcessName);
        automaton.processes.set(oldProcess.processName, oldProcess);

        if (oldProcess.isSelected)
            Gui.unblockSelectedOptionsByProcess(oldProcess.processName);

        Gui.unblockNewOptions(oldProcess.processName);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        if (this.newProcessName === this.oldProcessName)
            throw Error();

        let oldProcess = automaton.findProcess(this.oldProcessName);
        if (oldProcess !== null)
            throw Error();

        let newProcess = automaton.findProcess(this.newProcessName);
        if (newProcess === null)
            throw Error();

        let states = automaton.findAllStatesForProcess(this.newProcessName);
        if (states !== null)
            for (let current of states)
                current.processName = this.oldProcessName;

        let transitions = automaton.findAllTransitionsForProcess(this.newProcessName);
        if (transitions !== null)
            for (let current of transitions)
                current.processName = this.oldProcessName;

        automaton.processes.delete(newProcess.processName);
        newProcess.rename(this.oldProcessName);
        automaton.processes.set(newProcess.processName, newProcess);

        if (newProcess.isSelected)
            Gui.unblockSelectedOptionsByProcess(newProcess.processName);

        Gui.unblockNewOptions(newProcess.processName);
    }
}


/**
 * TODO
 */
class MoveProcessCommand extends Command {
    constructor(processName, x, y) {
        super();

        this.processName = processName;
        this.oldX = null;
        this.oldY = null;
        this.newX = x;
        this.newY = y;
    }

    optimizationHook(automaton, x, y) {
        if (!this.isExecuted || this.oldX === null || this.oldY === null)
            throw Error();

        this.newX = x;
        this.newY = y;

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        process.move(this.newX, this.newY);
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.oldX !== null || this.oldY !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        this.oldX = process.x;
        this.oldY = process.y;

        process.move(this.newX, this.newY);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        if (this.oldX === null || this.oldY === null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        process.move(this.oldX, this.oldY);

        this.oldX = null;
        this.oldY = null;
    }
}


/**
 * TODO
 */
class ResizeProcessCommand extends Command {
    constructor(processName, width, height) {
        super();

        this.processName = processName;
        this.oldWidth = null;
        this.oldHeight = null;
        this.newWidth = width;
        this.newHeight = height;
    }

    optimizationHook(automaton, width, height) {
        if (!this.isExecuted || this.oldWidth === null || this.oldHeight === null)
            throw Error();

        this.newWidth = width;
        this.newHeight = height;

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        process.resize(this.newWidth, this.newHeight);
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.oldWidth !== null || this.oldHeight !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        this.oldWidth = process.containerElement.width();
        this.oldHeight = process.containerElement.height();

        process.resize(this.newWidth, this.newHeight);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        if (this.oldWidth === null || this.oldHeight === null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        process.resize(this.oldWidth, this.oldHeight);

        this.oldWidth = null;
        this.oldHeight = null;
    }
}


/**
 * TODO
 */
class CreateStateCommand extends Command {
    constructor(processName, stateName, x, y) {
        super();

        this.processName = processName;
        this.stateName = stateName;
        this.x = x;
        this.y = y;
    }

    execute(automaton) {
        super.execute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state !== null)
            throw Error();

        state = new State(this.processName, this.stateName, this.x, this.y, process.insideSvg);
        state.create();
        process.states.set(this.stateName, state);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.remove();
        process.states.delete(this.stateName);
    }
}


/**
 * TODO
 */
class RemoveStateCommand extends Command {
    constructor(processName, stateName) {
        super();

        this.processName = processName;
        this.stateName = stateName;
        this.savedTransitions = null;
        this.isSelected = null;
        this.isInitial = null;
        this.x = null;
        this.y = null;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.savedTransitions !== null || this.isSelected !== null || this.isInitial !== null || this.x !== null || this.y !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        this.savedTransitions = [];
        this.isSelected = state.isSelected;
        this.isInitial = state.isInitial;
        this.x = state.x;
        this.y = state.y;

        let transitions = automaton.findAllTransitionsForState(this.processName, this.stateName);
        if (transitions !== null) {
            for (let current of transitions) {
                let command = new RemoveTransitionCommand(current.processName, current.firstStateName, current.secondStateName, current.condition);
                command.execute(automaton);
                this.savedTransitions.push(command);
            }
        }

        state.remove();
        process.states.delete(this.stateName);

        if (this.isSelected)
            Gui.blockSelectedOptions();
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        if (this.savedTransitions === null || this.isSelected === null || this.isInitial === null || this.x === null || this.y === null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state !== null)
            throw Error();

        state = new State(this.processName, this.stateName, this.x, this.y, process.insideSvg);
        state.create();

        if (this.isInitial)
            state.setInitial();

        if (this.isSelected) {
            state.select();
            Gui.unblockSelectedOptionsByState(state.processName, state.stateName);
        }

        process.states.set(this.stateName, state);

        for (let current of this.savedTransitions)
            current.unexecute(automaton);

        this.isSelected = null;
        this.isInitial = null;
        this.savedTransitions = null;
        this.x = null;
        this.y = null;
    }
}


/**
 * TODO
 */
class SetInitialForStateCommand extends Command {
    constructor(processName, stateName) {
        super();

        this.processName = processName;
        this.stateName = stateName;
        this.savedInitial = null;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.savedInitial !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        this.savedInitial = automaton.findInitialState(this.processName);
        if (this.savedInitial !== null) {
            this.savedInitial = new ResetInitialForStateCommand(this.savedInitial.processName, this.savedInitial.stateName);
            this.savedInitial.execute(automaton);
        }

        state.setInitial();
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.removeInitial();

        if (this.savedInitial !== null) {
            this.savedInitial.unexecute(automaton);
            this.savedInitial = null;
        }
    }
}


/**
 * TODO
 */
class ResetInitialForStateCommand extends Command {
    constructor(processName, stateName) {
        super();

        this.processName = processName;
        this.stateName = stateName;
    }

    execute(automaton) {
        super.execute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.removeInitial();
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.setInitial();
    }
}


/**
 * TODO
 */
class SelectStateCommand extends Command {
    constructor(processName, stateName) {
        super();

        this.processName = processName;
        this.stateName = stateName;
        this.savedSelection = null;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.savedSelection !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        let selectedState = automaton.findSelectedState();
        if (selectedState === null) {
            let selectedTransition = automaton.findSelectedTransition();
            if (selectedTransition === null) {
                let selectedProcess = automaton.findSelectedProcess();
                if (selectedProcess !== null)
                    this.savedSelection = new DeselectProcessCommand(selectedProcess.processName);
            }
            else
                this.savedSelection = new DeselectTransitionCommand(selectedTransition.processName, selectedTransition.firstStateName, selectedTransition.secondStateName, selectedTransition.condition);
        }
        else
            this.savedSelection = new DeselectStateCommand(selectedState.processName, selectedState.stateName);

        if (this.savedSelection)
            this.savedSelection.execute(automaton);

        state.select();
        Gui.unblockSelectedOptionsByState(this.processName, this.stateName);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.deselect();
        Gui.blockSelectedOptions();

        if (this.savedSelection !== null) {
            this.savedSelection.unexecute(automaton);
            this.savedSelection = null;
        }
    }
}


/**
 * TODO
 */
class DeselectStateCommand extends Command {
    constructor(processName, stateName) {
        super();

        this.processName = processName;
        this.stateName = stateName;
    }

    execute(automaton) {
        super.execute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.deselect();
        Gui.blockSelectedOptions();
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.select();
        Gui.unblockSelectedOptionsByState(this.processName, this.stateName);
    }
}


/**
 * TODO
 */
class RenameStateCommand extends Command {
    constructor(processName, oldStateName, newStateName) {
        super();

        this.processName = processName;
        this.oldStateName = oldStateName;
        this.newStateName = newStateName;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.newStateName === this.oldStateName)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let oldState = automaton.findState(this.processName, this.oldStateName);
        if (oldState === null)
            throw Error();

        let newState = automaton.findState(this.processName, this.newStateName);
        if (newState !== null)
            throw Error();

        let transitions = automaton.findAllTransitionsForState(this.processName, this.oldStateName);
        if (transitions !== null)
            for (let current of transitions) {
                process.transitions.delete(Transition.toKey(current.firstStateName, current.secondStateName, current.condition));
                current.changeState(this.oldStateName, this.newStateName);
                process.transitions.set(Transition.toKey(current.firstStateName, current.secondStateName, current.condition), current);
            }

        process.states.delete(oldState.stateName);
        oldState.rename(this.newStateName);
        process.states.set(oldState.stateName, oldState);

        if (oldState.isSelected)
            Gui.unblockSelectedOptionsByState(oldState.processName, oldState.stateName);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        if (this.newStateName === this.oldStateName)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let newState = automaton.findState(this.processName, this.newStateName);
        if (newState === null)
            throw Error();

        let oldState = automaton.findState(this.processName, this.oldStateName);
        if (oldState !== null)
            throw Error();

        let transitions = automaton.findAllTransitionsForState(this.processName, this.newStateName);
        if (transitions !== null)
            for (let current of transitions) {
                process.transitions.delete(Transition.toKey(current.firstStateName, current.secondStateName, current.condition));
                current.changeState(this.newStateName, this.oldStateName);
                process.transitions.set(Transition.toKey(current.firstStateName, current.secondStateName, current.condition), current);
            }

        process.states.delete(newState.stateName);
        newState.rename(this.oldStateName);
        process.states.set(newState.stateName, newState);

        if (newState.isSelected)
            Gui.unblockSelectedOptionsByState(newState.processName, newState.stateName);
    }
}


/**
 * TODO
 */
class MoveStateCommand extends Command {
    constructor(processName, stateName, x, y) {
        super();

        this.processName = processName;
        this.stateName = stateName;
        this.oldX = null;
        this.oldY = null;
        this.newX = x;
        this.newY = y;
    }

    optimizationHook(automaton, x, y) {
        if (!this.isExecuted || this.oldX === null || this.oldY === null)
            throw Error();

        this.newX = x;
        this.newY = y;

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.move(this.newX, this.newY);

        let transitions = automaton.findAllTransitionsForState(this.processName, this.stateName);
        if (transitions !== null)
            for (let current of transitions)
                current.moveByState(this.stateName, this.newX, this.newY);
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.oldX !== null || this.oldY !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        this.oldX = state.x;
        this.oldY = state.y;

        state.move(this.newX, this.newY);

        let transitions = automaton.findAllTransitionsForState(this.processName, this.stateName);
        if (transitions !== null)
            for (let current of transitions)
                current.moveByState(this.stateName, this.newX, this.newY);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        if (this.oldX === null || this.oldY === null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let state = automaton.findState(this.processName, this.stateName);
        if (state === null)
            throw Error();

        state.move(this.oldX, this.oldY);

        let transitions = automaton.findAllTransitionsForState(this.processName, this.stateName);
        if (transitions !== null)
            for (let current of transitions)
                current.moveByState(this.stateName, this.oldX, this.oldY);

        this.oldX = null;
        this.oldY = null;
    }
}


/**
 * TODO
 */
class CreateTransitionCommand extends Command {
    constructor(processName, firstStateName, secondStateName, condition) {
        super();

        this.processName = processName;
        this.firstStateName = firstStateName;
        this.secondStateName = secondStateName;
        this.condition = condition;
    }

    execute(automaton) {
        super.execute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
        if (transition !== null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        transition = new Transition(this.processName, this.firstStateName, firstState.x, firstState.y, this.secondStateName, secondState.x, secondState.y,
            this.condition, process.insideSvg, process.selectedMarker, process.unselectedMarker);

        transition.create();
        process.transitions.set(Transition.toKey(this.firstStateName, this.secondStateName, this.condition), transition);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
        if (transition === null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        transition.remove();
        process.transitions.delete(Transition.toKey(this.firstStateName, this.secondStateName, this.condition));
    }
}


/**
 * TODO
 */
class RemoveTransitionCommand extends Command {
    constructor(processName, firstStateName, secondStateName, condition) {
        super();

        this.processName = processName;
        this.firstStateName = firstStateName;
        this.firstX = null;
        this.firstY = null;
        this.secondStateName = secondStateName;
        this.secondX = null;
        this.secondY = null;
        this.condition = condition;
        this.isSelected = null;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.firstX !== null || this.firstY !== null || this.secondX !== null || this.secondY !== null || this.isSelected !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
        if (transition === null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        this.firstX = firstState.x;
        this.firstY = firstState.y;
        this.secondX = secondState.x;
        this.secondY = secondState.y;
        this.isSelected = transition.isSelected;

        transition.remove();
        process.transitions.delete(Transition.toKey(this.firstStateName, this.secondStateName, this.condition));

        if (this.isSelected)
            Gui.blockSelectedOptions();
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        if (this.firstX === null || this.firstY === null || this.secondX === null || this.secondY === null || this.isSelected === null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
        if (transition !== null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        transition = new Transition(this.processName, this.firstStateName, this.firstX, this.firstY, this.secondStateName, this.secondX, this.secondY,
            this.condition, process.insideSvg, process.selectedMarker, process.unselectedMarker);

        transition.create();

        if (this.isSelected) {
            transition.select();
            Gui.unblockSelectedOptionsByTransition(transition.processName, transition.firstStateName, transition.secondStateName, transition.condition);
        }

        process.transitions.set(Transition.toKey(this.firstStateName, this.secondStateName, this.condition), transition);

        this.firstX = null;
        this.firstY = null;
        this.secondX = null;
        this.secondY = null;
        this.isSelected = null;
    }
}


/**
 * TODO
 */
class SetConditionForTransitionCommand extends Command {
    constructor(processName, firstStateName, secondStateName, oldCondition, newCondition) {
        super();

        this.processName = processName;
        this.firstStateName = firstStateName;
        this.secondStateName = secondStateName;
        this.oldCondition = oldCondition;
        this.newCondition = newCondition;
    }

    execute(automaton) {
        super.execute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.oldCondition);
        if (transition === null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        process.transitions.delete(Transition.toKey(transition.firstStateName, transition.secondStateName, transition.condition));
        transition.setCondition(this.newCondition);
        process.transitions.set(Transition.toKey(transition.firstStateName, transition.secondStateName, transition.condition), transition);

        if (transition.isSelected)
            Gui.unblockSelectedOptionsByTransition(transition.processName, transition.firstStateName, transition.secondStateName, transition.condition);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.newCondition);
        if (transition === null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        process.transitions.delete(Transition.toKey(transition.firstStateName, transition.secondStateName, transition.condition));
        transition.setCondition(this.oldCondition);
        process.transitions.set(Transition.toKey(transition.firstStateName, transition.secondStateName, transition.condition), transition);

        if (transition.isSelected)
            Gui.unblockSelectedOptionsByTransition(transition.processName, transition.firstStateName, transition.secondStateName, transition.condition);
    }
}


/**
 * TODO
 */
class SelectTransitionCommand extends Command {
    constructor(processName, firstStateName, secondStateName, condition) {
        super();

        this.processName = processName;
        this.firstStateName = firstStateName;
        this.secondStateName = secondStateName;
        this.condition = condition;
        this.savedSelection = null;
    }

    execute(automaton) {
        super.execute(automaton);

        if (this.savedSelection !== null)
            throw Error();

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
        if (transition === null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        let selectedState = automaton.findSelectedState();
        if (selectedState === null) {
            let selectedTransition = automaton.findSelectedTransition();
            if (selectedTransition === null) {
                let selectedProcess = automaton.findSelectedProcess();
                if (selectedProcess !== null)
                    this.savedSelection = new DeselectProcessCommand(selectedProcess.processName);
            }
            else
                this.savedSelection = new DeselectTransitionCommand(selectedTransition.processName, selectedTransition.firstStateName, selectedTransition.secondStateName, selectedTransition.condition);
        }
        else
            this.savedSelection = new DeselectStateCommand(selectedState.processName, selectedState.stateName);

        if (this.savedSelection)
            this.savedSelection.execute(automaton);

        transition.select();
        Gui.unblockSelectedOptionsByTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
        if (transition === null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        transition.deselect();
        Gui.blockSelectedOptions();

        if (this.savedSelection !== null) {
            this.savedSelection.unexecute(automaton);
            this.savedSelection = null;
        }
    }
}


/**
 * TODO
 */
class DeselectTransitionCommand extends Command {
    constructor(processName, firstStateName, secondStateName, condition) {
        super();

        this.processName = processName;
        this.firstStateName = firstStateName;
        this.secondStateName = secondStateName;
        this.condition = condition;
    }

    execute(automaton) {
        super.execute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
        if (transition === null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        transition.deselect();
        Gui.blockSelectedOptions();
    }

    unexecute(automaton) {
        super.unexecute(automaton);

        let process = automaton.findProcess(this.processName);
        if (process === null)
            throw Error();

        let transition = automaton.findTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
        if (transition === null)
            throw Error();

        let firstState = automaton.findState(this.processName, this.firstStateName);
        if (firstState === null)
            throw Error();

        let secondState = automaton.findState(this.processName, this.secondStateName);
        if (secondState === null)
            throw Error();

        transition.select();
        Gui.unblockSelectedOptionsByTransition(this.processName, this.firstStateName, this.secondStateName, this.condition);
    }
}