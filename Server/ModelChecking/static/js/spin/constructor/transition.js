class Transition {
    constructor(processName, firstStateName, firstX, firstY, secondStateName, secondX, secondY, condition, rootSvg, selectedMarkerSVG, unselectedMarkerSVG) {
        this.processName = processName;
        this.firstStateName = firstStateName;
        this.firstX = firstX;
        this.firstY = firstY;
        this.secondStateName = secondStateName;
        this.secondX = secondX;
        this.secondY = secondY;
        this.condition = condition;
        this.isCreated = false;
        this.isSelected = false;
        this.rootSvg = rootSvg;
        this.selectedMarkerSvg = selectedMarkerSVG;
        this.unselectedMarkerSVG = unselectedMarkerSVG;
        this.transitionSvg = null;
        this.conditionSvg = null;
    }

    static toKey(firstStateName, secondStateName, condition) {
        return "" + firstStateName + ":" + secondStateName + ":" + condition;
    }

    static fromKey(key) {
        let firstIndex = key.indexOf(":");
        let lastIndex = key.lastIndexOf(":");
        return [key.substring(0, firstIndex), key.substring(firstIndex + 1, lastIndex), key.substring(lastIndex + 1)];
    }

    create() {
        if (this.isCreated)
            throw Error();

        this.transitionSvg = this.rootSvg.line();
        this.transitionSvg.marker("end", this.unselectedMarkerSVG);
        this.transitionSvg.attr({"class": "unselected-transition"});

        this.conditionSvg = this.rootSvg.text(JSON.parse(this.condition));
        this.conditionSvg.attr({"class": "transition-name"});

        this.transitionSvg.on("contextmenu", (event) => this.onRightClick(event));
        this.conditionSvg.on("contextmenu", (event) => this.onRightClick(event));

        this.transitionSvg.on("click", (event) => this.onLeftClick(event));
        this.conditionSvg.on("click", (event) => this.onLeftClick(event));

        this.transitionSvg.on("mousedown", (event) => this.onMouseDown(event));
        this.conditionSvg.on("mousedown", (event) => this.onMouseDown(event));

        this.transitionSvg.on("mouseup", (event) => this.onMouseUp(event));
        this.conditionSvg.on("mouseup", (event) => this.onMouseUp(event));

        this.isCreated = true;

        this.move(this.firstX, this.firstY, this.secondX, this.secondY);
    }

    remove() {
        if (!this.isCreated)
            throw Error();

        this.transitionSvg.off("contextmenu");
        this.conditionSvg.off("contextmenu");

        this.transitionSvg.off("click");
        this.conditionSvg.off("click");

        this.transitionSvg.off("mousedown");
        this.conditionSvg.off("mousedown");

        this.transitionSvg.off("mouseup");
        this.conditionSvg.off("mouseup");

        this.transitionSvg.remove();
        this.conditionSvg.remove();

        this.isCreated = false;
        this.isSelected = false;
    }

    changeState(oldStateName, newStateName) {
        if (!this.isCreated)
            throw Error();

        if (this.firstStateName === oldStateName)
            this.firstStateName = newStateName;

        if (this.secondStateName === oldStateName)
            this.secondStateName = newStateName;
    }

    setCondition(condition) {
        if (!this.isCreated)
            throw Error();

        this.conditionSvg.text(JSON.parse(condition));
        this.move(this.firstX, this.firstY, this.secondX, this.secondY);

        this.condition = condition;
    }

    moveByState(stateName, x, y) {
        if (!this.isCreated)
            throw Error();

        if (stateName === this.firstStateName)
            this.move(x, y, this.secondX, this.secondY);

        if (stateName === this.secondStateName)
            this.move(this.firstX, this.firstY, x, y);
    }

    move(firstX, firstY, secondX, secondY) {
        if (!this.isCreated)
            throw Error();

        // TODO

        this.firstX = firstX;
        this.firstY = firstY;
        this.secondX = secondX;
        this.secondY = secondY;

        let dx = this.secondX - this.firstX;
        let dy = this.secondY - this.firstY;

        if (dx === 0 && dy === 0) {
            // TODO
            dx = 1;
        }

        let temp = 1 / Math.sqrt(dx * dx + dy * dy);
        let firstCoefficient = Constructor.SVG_STATE_RADIUS * temp;
        let secondCoefficient = (Constructor.SVG_STATE_RADIUS + Constructor.SVG_MARKER_SIZE) * temp;

        let fromX = this.firstX + firstCoefficient * dx;
        let fromY = this.firstY + firstCoefficient * dy;
        let toX = this.secondX - secondCoefficient * dx;
        let toY = this.secondY - secondCoefficient * dy;

        this.transitionSvg.plot(fromX, fromY, toX, toY);

        let halfWidth = this.conditionSvg.bbox().width / 2;
        let halfHeight = this.conditionSvg.bbox().height / 2;

        let conditionX = this.firstX + dx / 2;
        let conditionY = this.firstY + dy / 2 - halfHeight;

        let cos = dx * temp;
        let sin = dy * temp;

        if (dx < 0) {
            cos = -cos;
            sin = -sin;
        }

        let matrix = new SVG.Matrix(1, 0, 0, 1, conditionX, conditionY);
        matrix = matrix.multiply(new SVG.Matrix(cos, sin, -sin, cos, 0, 0));
        matrix = matrix.multiply(new SVG.Matrix(1, 0, 0, 1, -conditionX, -conditionY));

        this.conditionSvg.move(conditionX - halfWidth, conditionY - halfHeight - 3);
        this.conditionSvg.transform(matrix);
    }

    select(animate) {
        if (!this.isCreated || this.isSelected)
            throw Error();

        if (animate) {
            this.transitionSvg.marker("end", this.selectedMarkerSvg);
            this.transitionSvg.animate().attr({"class": "selected-transition"});
            this.conditionSvg.animate().attr({"class": "selected-transition-name"});
        }
        else {
            this.transitionSvg.marker("end", this.selectedMarkerSvg);
            this.transitionSvg.attr({"class": "selected-transition"});
            this.conditionSvg.attr({"class": "selected-transition-name"});
        }

        this.isSelected = true;
    }

    deselect(animate) {
        if (!this.isCreated || !this.isSelected)
            throw Error();

        if (animate) {
            this.transitionSvg.marker("end", this.unselectedMarkerSVG);
            this.transitionSvg.animate().attr({"class": "unselected-transition"});
            this.conditionSvg.animate().attr({"class": "transition-name"});
        }
        else {
            this.transitionSvg.marker("end", this.unselectedMarkerSVG);
            this.transitionSvg.attr({"class": "unselected-transition"});
            this.conditionSvg.attr({"class": "transition-name"});
        }

        this.isSelected = false;
    }

    onLeftClick(event) {
        let box = this.rootSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Left click event (x: '" + x + "', y: '" + y + "') on transition '" + this.firstStateName + " -> " + this.secondStateName + " : " + this.condition + "' with process '" + this.processName + "'.");

        Menu.hideContextMenu();

        if (!Constructor.moved)
            Constructor.changeTransitionSelection(this);

        Constructor.moved = null;

        event.stopPropagation();
    }

    onRightClick(event) {
        let box = this.rootSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Right click event (x: '" + x + "', y: '" + y + "') on transition '" + this.firstStateName + " -> " + this.secondStateName + " : " + this.condition + "' with process '" + this.processName + "'.");

        Menu.hideContextMenu();
        Menu.showTransitionContextMenu(event.clientX, event.clientY, this.processName, this.firstStateName, this.secondStateName, this.condition);

        Constructor.resetVariables();

        event.preventDefault();
        event.stopPropagation();
    }

    onMouseDown(event) {
        Gui.clearSelection();

        let box = this.rootSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Mouse down event (x: '" + x + "', y: '" + y + "') on transition '" + this.firstStateName + " -> " + this.secondStateName + " : " + this.condition + "' with process '" + this.processName + "'.");

        Menu.hideContextMenu();

        Constructor.resetVariables();

        event.stopPropagation();
    }

    onMouseUp(event) {
        let box = this.rootSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Mouse up event (x: '" + x + "', y: '" + y + "') on transition '" + this.firstStateName + " -> " + this.secondStateName + " : " + this.condition + "'.");

        let temp = Constructor.moved;
        Constructor.resetVariables();
        Constructor.moved = temp;

        event.stopPropagation();
    }
}