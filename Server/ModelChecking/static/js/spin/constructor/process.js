class Process {
    constructor(processName, x, y, rootElement) {
        this.states = new Map();
        this.transitions = new Map();
        this.processName = processName;
        this.x = x;
        this.y = y;
        this.rootElement = rootElement;
        this.isCreated = false;
        this.isSelected = false;
        this.altitudeIndex = 0;
        this.processParameters = null;
        this.processContext = null;
        this.containerElement = null;
        this.areaElement = null;
        this.headerElement = null;
        this.captionElement = null;
        this.insideSvg = null;
        this.selectedMarker = null;
        this.unselectedMarker = null;
    }

    create() {
        if (this.isCreated)
            throw Error();

        this.processParameters = "/* Define process parameters here. */";
        this.processContext = "\t/*\n\t\tEntry point of process.\n\t\tDefine process context here.\n\t*/\n";

        this.containerElement = $("<div></div>");
        this.containerElement.css({left: this.x, top: this.y});
        this.containerElement.addClass("process-container");
        this.rootElement.append(this.containerElement);

        this.headerElement = $("<div></div>");
        this.headerElement.addClass("process-header");
        this.containerElement.append(this.headerElement);

        this.captionElement = $("<p>" + this.processName + "</p>");
        this.captionElement.addClass("process-caption");
        this.headerElement.append(this.captionElement);

        this.areaElement = $("<div></div>");
        this.areaElement.attr("id", "process-" + this.processName);
        this.areaElement.addClass("process-content");
        this.containerElement.append(this.areaElement);

        this.insideSvg = SVG("process-" + this.processName).size(Constructor.SVG_SIZE.toString(), Constructor.SVG_SIZE.toString());

        let smallGrid = this.insideSvg.pattern(Constructor.SVG_SMALL_GRID_INTERVAL, Constructor.SVG_SMALL_GRID_INTERVAL, pattern => {
            let path = pattern.path("M " + Constructor.SVG_SMALL_GRID_INTERVAL + " 0 L 0 0 0 " + Constructor.SVG_SMALL_GRID_INTERVAL);
            path.attr({"fill": "none", "stroke": "gray", "stroke-width": "0.5"});
        });

        let bigGrid = this.insideSvg.pattern(Constructor.SVG_BIG_GRID_INTERVAL, Constructor.SVG_BIG_GRID_INTERVAL, pattern => {
            pattern.rect(Constructor.SVG_BIG_GRID_INTERVAL, Constructor.SVG_BIG_GRID_INTERVAL).fill(smallGrid);

            let path = pattern.path("M " + Constructor.SVG_BIG_GRID_INTERVAL + " 0 L 0 0 0 " + Constructor.SVG_BIG_GRID_INTERVAL);
            path.attr({"fill": "none", "stroke": "gray", "stroke-width": "1"});
        });

        this.insideSvg.rect("100%", "100%").fill(bigGrid);

        this.selectedMarker = this.insideSvg.marker(4, 3).ref(0, 5);
        this.selectedMarker.path("M 0 0 L 10 5 L 0 10 z");
        this.selectedMarker.attr({"viewBox": "0 0 10 10", "class": "selected-marker"});

        this.unselectedMarker = this.insideSvg.marker(4, 3).ref(0, 5);
        this.unselectedMarker.attr({"viewBox": "0 0 10 10", "class": "unselected-marker"});
        this.unselectedMarker.path("M 0 0 L 10 5 L 0 10 z");

        this.areaElement.scrollLeft(Constructor.SVG_SIZE / 2);
        this.areaElement.scrollTop(Constructor.SVG_SIZE / 2);

        let self = this;
        this.containerElement.resizable({resize: (event) => self.onResize(event)});
        this.containerElement.on("mousedown", (event) => this.onContainerMouseDown(event));

        this.headerElement.on("click", (event) => this.onHeaderLeftClick(event));
        this.headerElement.on("contextmenu", (event) => this.onHeaderRightClick(event));
        this.headerElement.on("mousedown", (event) => this.onHeaderMouseDown(event));

        this.insideSvg.on("click", (event) => this.onLeftClick(event));
        this.insideSvg.on("contextmenu", (event) => this.onRightClick(event));
        this.insideSvg.on("mousedown", (event) => this.onMouseDown(event));
        this.insideSvg.on("mouseup", (event) => this.onMouseUp(event));
        this.insideSvg.on("mousemove", (event) => this.onMouseMove(event));
        this.insideSvg.on("mouseleave", (event) => this.onMouseLeave(event));

        this.isCreated = true;

        this.setFocus();
    }

    remove() {
        if (!this.isCreated)
            throw Error();

        this.states.clear();
        this.transitions.clear();

        this.containerElement.resizable({resize: null});
        this.containerElement.remove();

        if (this.containerElement)
            this.containerElement.off("mousedown");

        if (this.headerElement) {
            this.headerElement.off("click");
            this.headerElement.off("contextmenu");
            this.headerElement.off("mousedown");
        }

        if (this.insideSvg) {
            this.insideSvg.off("click");
            this.insideSvg.off("contextmenu");
            this.insideSvg.off("mousedown");
            this.insideSvg.off("mouseup");
            this.insideSvg.off("mousemove");
            this.insideSvg.off("mouseleave");
        }

        this.isCreated = false;
        this.isSelected = false;
        this.altitudeIndex = 0;
        this.processParameters = null;
        this.processContext = null;
        this.containerElement = null;
        this.areaElement = null;
        this.headerElement = null;
        this.captionElement = null;
        this.insideSvg = null;
        this.selectedMarker = null;
        this.unselectedMarker = null;
    }

    rename(newProcessName) {
        if (!this.isCreated)
            throw Error();

        this.captionElement.text(newProcessName);

        this.processName = newProcessName;
    }

    move(x, y) {
        if (!this.isCreated)
            throw Error();

        this.containerElement.css({left: x, top: y});

        this.x = x;
        this.y = y;
    }

    select() {
        if (!this.isCreated || this.isSelected)
            throw Error();

        this.headerElement.addClass("process-selected");
        this.isSelected = true;
    }

    deselect() {
        if (!this.isCreated || !this.isSelected)
            throw Error();

        this.headerElement.removeClass("process-selected");
        this.isSelected = false;
    }

    resize(width, height) {
        if (!this.isCreated)
            throw Error();

        this.containerElement.width(width);
        this.containerElement.height(height);

        this.areaElement.width(width);
        this.areaElement.height(height - this.headerElement.height())
    }

    scroll(left, top) {
        if (!this.isCreated)
            throw Error();

        this.areaElement.scrollLeft(left);
        this.areaElement.scrollTop(top);
    }

    setFocus() {
        if (!this.isCreated)
            throw Error();

        this.containerElement.css("z-index", ++Constructor.maxAltitudeIndex);
        Gui.unblockNewOptions(this.processName);

        this.altitudeIndex = Constructor.maxAltitudeIndex;
    }

    onResize(event) {
        let container = this.containerElement;
        let root = this.rootElement;

        let left = container.offset().left - root.offset().left;
        let top = container.offset().top - root.offset().top;
        let width = container.width();
        let height = container.height();

        if (left + width > Constructor.AREA_SIZE)
            container.width(Constructor.AREA_SIZE - left);

        if (top + height > Constructor.AREA_SIZE)
            container.height(Constructor.AREA_SIZE - top);

        Constructor.automaton.executeCommand(new ResizeProcessCommand(this.processName, container.width(), container.height()));
    }

    onContainerMouseDown(event) {
        Gui.clearSelection();

        log(LogLevel.DEBUG, "Mouse down event on container element for process '" + this.processName + "'.");

        this.setFocus();

        event.stopPropagation();
    }

    onHeaderMouseDown(event) {
        let offset = this.headerElement.offset();
        let x = event.pageX - offset.left;
        let y = event.pageY - offset.top;

        log(LogLevel.DEBUG, "Mouse down event on header element for process '" + this.processName + "'.");

        Constructor.resetVariables();
        Constructor.movableProcess = this;
        Constructor.movableProcessX = x;
        Constructor.movableProcessY = y;
    }

    onHeaderLeftClick(event) {
        log(LogLevel.DEBUG, "Left click event on header element for process '" + this.processName + "'.");

        Menu.hideContextMenu();

        if (!Constructor.moved)
            Constructor.changeProcessSelection(this);

        Constructor.moved = null;

        event.stopPropagation();
    }

    onHeaderRightClick(event) {
        log(LogLevel.DEBUG, "Right click event on header element for process '" + this.processName + "'.");

        Menu.hideContextMenu();
        Menu.showProcessContextMenu(event.clientX, event.clientY, this.processName);

        Constructor.resetVariables();

        event.preventDefault();
        event.stopPropagation();
    }

    onLeftClick(event) {
        let box = this.insideSvg.rbox();
        let x = event.clientX - box.x;
        let y = event.clientY - box.y;

        log(LogLevel.DEBUG, "Left click event (x: '" + x + "', y: '" + y + "') on constructor svg for process '" + this.processName + "'.");

        Menu.hideContextMenu();
        Constructor.deselectAll();

        event.stopPropagation();
    }

    onRightClick(event) {
        let box = this.insideSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Right click event (x: '" + x + "', y: '" + y + "') on constructor svg for process '" + this.processName + "'.");

        Menu.hideContextMenu();
        Menu.showSvgContextMenu(event.clientX, event.clientY, this.processName);

        Constructor.resetVariables();

        event.preventDefault();
        event.stopPropagation();
    }

    onMouseDown(event) {
        Gui.clearSelection();

        let box = this.insideSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Mouse down event (x: '" + x + "', y: '" + y + "') on constructor svg for process '" + this.processName + "'.");

        Menu.hideContextMenu();

        Constructor.resetVariables();
        Constructor.holdSvgX = x;
        Constructor.holdSvgY = y;
        Constructor.holdSvg = true;
    }

    onMouseUp(event) {
        let box = this.insideSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        log(LogLevel.DEBUG, "Mouse up event (x: '" + x + "', y: '" + y + "') on constructor svg for process '" + this.processName + "'.");

        let temp = Constructor.moved;
        Constructor.resetVariables();
        Constructor.moved = temp;

        event.stopPropagation();
    }

    onMouseMove(event) {
        let box = this.insideSvg.rbox();
        let x = event.pageX - box.x;
        let y = event.pageY - box.y;

        /* log(LogLevel.DEBUG, "Mouse move event (x: '" + x + "', y: '" + y + "') on constructor svg for process '" + this.processName + "'."); */

        if (Constructor.holdSvg && Constructor.holdSvgX && Constructor.holdSvgY) {
            let area = this.areaElement;

            let scrollTop = area.scrollTop();
            let scrollLeft = area.scrollLeft();

            area.scrollLeft(scrollLeft - (x - Constructor.holdSvgX));
            area.scrollTop(scrollTop - (y - Constructor.holdSvgY));
        }
        else if (Constructor.movableState) {
            let newX = Math.round(x / Constructor.SVG_SMALL_GRID_INTERVAL) * Constructor.SVG_SMALL_GRID_INTERVAL;
            let newY = Math.round(y / Constructor.SVG_SMALL_GRID_INTERVAL) * Constructor.SVG_SMALL_GRID_INTERVAL;

            if (newX - Constructor.SVG_STATE_RADIUS < 0)
                newX = Constructor.SVG_STATE_RADIUS;

            if (newX + Constructor.SVG_STATE_RADIUS > Constructor.SVG_SIZE)
                newX = Constructor.SVG_SIZE - Constructor.SVG_STATE_RADIUS;

            if (newY - Constructor.SVG_STATE_RADIUS < 0)
                newY = Constructor.SVG_STATE_RADIUS;

            if (newY + Constructor.SVG_STATE_RADIUS > Constructor.SVG_SIZE)
                newY = Constructor.SVG_SIZE - Constructor.SVG_STATE_RADIUS;

            if (newX !== Constructor.movableState.x || newY !== Constructor.movableState.y) {
                Constructor.automaton.executeCommand(new MoveStateCommand(this.processName, Constructor.movableState.stateName, newX, newY));
                Constructor.moved = true;
            }
        }
    }

    onMouseLeave(event) {
        log(LogLevel.DEBUG, "Mouse out event on constructor svg for process '" + this.processName + "'.");

        Constructor.holdSvg = null;
        Constructor.holdSvgX = null;
        Constructor.holdSvgY = null;

        Constructor.moved = null;
        Constructor.movableState = null;

        event.stopPropagation();
    }
}