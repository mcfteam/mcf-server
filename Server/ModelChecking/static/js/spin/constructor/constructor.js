let Constructor = {
    AREA_SIZE: 5000,
    PROCESS_DEFAULT_WIDTH: 400,
    PROCESS_DEFAULT_HEIGHT: 200,
    SVG_SIZE: 5000,
    SVG_SMALL_GRID_INTERVAL: 10,
    SVG_BIG_GRID_INTERVAL: 50,
    SVG_GRID_DIMENSION: 50 / 10,
    SVG_STATE_RADIUS: 50 / 2,
    SVG_MARKER_SIZE: 5,
    TAU_AREA_WIDTH: 800,
    TAU_AREA_HEIGHT: 400,
    PROCESS_NAME_REGEXP: /[A-Za-z]\w{0,10}/i,
    STATE_NAME_REGEXP: /[A-Za-z]\w{0,10}/i,
    TRANSITION_CONDITION_REGEXP: /[^']+/i,

    automaton: null,
    area: null,
    enabled: null,
    maxAltitudeIndex: null,
    holdArea: null,
    holdAreaX: null,
    holdAreaY: null,
    holdSvg: null,
    holdSvgX: null,
    holdSvgY: null,
    moved: null,
    movableProcess: null,
    movableProcessX: null,
    movableProcessY: null,
    movableState: null,

    initializeAutomation: function() {
        if (!SVG.supported) {
            log(LogLevel.ERROR, "SVG not supported.");
            return;
        }

        this.automaton = new FiniteStateAutomaton();
        this.maxAltitudeIndex = 0;

        this.area = new Area();
        this.enabled = true;
    },

    resetSvg: function() {
        $("#constructor-root").empty();

        if (this.automaton)
            this.automaton.stopSimulation();

        if (this.area) {
            this.area.reset();
            this.area = null;
        }

        this.automaton = null;
        this.enabled = null;
    },

    resetVariables: function() {
        this.holdArea = null;
        this.holdAreaX = null;
        this.holdAreaY = null;

        this.holdSvg = null;
        this.holdSvgX = null;
        this.holdSvgY = null;

        this.moved = null;
        this.movableProcess = null;
        this.movableProcessX = null;
        this.movableProcessY = null;
        this.movableState = null;
    },

    shiftSvg: function() {
        let scrollElement = $("#constructor");
        if (typeof scrollElement.data("scroll-left") !== "undefined" && typeof scrollElement.data("scroll-top") !== "undefined") {
            scrollElement.scrollLeft(scrollElement.data("scroll-left"));
            scrollElement.scrollTop(scrollElement.data("scroll-top"));
            scrollElement.removeData("scroll-top");
            scrollElement.removeData("scroll-left");
        }

        this.automaton.shiftToDefault();
    },

    deselectAll: function() {
        Menu.hideContextMenu();

        let selectedState = this.automaton.findSelectedState();
        if (selectedState === null) {
            let selectedTransition = this.automaton.findSelectedTransition();
            if (selectedTransition === null) {
                let selectedProcess = this.automaton.findSelectedProcess();
                if (selectedProcess !== null)
                    this.automaton.executeCommand(new DeselectProcessCommand(selectedProcess.processName));
            }
            else
                this.automaton.executeCommand(new DeselectTransitionCommand(selectedTransition.processName, selectedTransition.firstStateName, selectedTransition.secondStateName, selectedTransition.condition));
        }
        else
            this.automaton.executeCommand(new DeselectStateCommand(selectedState.processName, selectedState.stateName));
    },

    createProcess: function(processName) {
        Menu.hideContextMenu();

        let process = this.automaton.findProcess(processName);
        if (process !== null)
            return;

        let area = $("#constructor");
        let x = area.scrollLeft() + area.width() / 2 - this.PROCESS_DEFAULT_WIDTH / 2;
        let y = area.scrollTop() + area.height() / 2 - this.PROCESS_DEFAULT_HEIGHT / 2;

        if (x < 0)
            x = 0;

        if (y < 0)
            y = 0;

        if (x + this.PROCESS_DEFAULT_WIDTH > this.AREA_SIZE)
            x = this.AREA_SIZE - this.PROCESS_DEFAULT_WIDTH;

        if (y + this.PROCESS_DEFAULT_HEIGHT > this.AREA_SIZE)
            y = this.AREA_SIZE - this.PROCESS_DEFAULT_HEIGHT;

        this.automaton.executeCommand(new CreateProcessCommand(processName, x, y, $("#constructor-root")));
    },

    renameProcess: function(oldProcessName, newProcessName) {
        Menu.hideContextMenu();

        let process = this.automaton.findProcess(oldProcessName);
        if (process !== null)
            this.automaton.executeCommand(new RenameProcessCommand(oldProcessName, newProcessName));
    },

    removeProcess: function(processName) {
        Menu.hideContextMenu();

        let process = this.automaton.findProcess(processName);
        if (process !== null)
            this.automaton.executeCommand(new RemoveProcessCommand(processName));
    },

    changeProcessSelection: function(instance) {
        Menu.hideContextMenu();

        if (instance.isSelected === true)
            this.automaton.executeCommand(new DeselectProcessCommand(instance.processName));
        else if (instance.isSelected === false)
            this.automaton.executeCommand(new SelectProcessCommand(instance.processName));
    },

    changeProcessContext: function(processName, processParameters, processContext) {
        Menu.hideContextMenu();

        let process = this.automaton.findProcess(processName);
        if (process !== null) {
            process.processParameters = processParameters;
            process.processContext = processContext;
        }
    },

    createState: function(processName, stateName) {
        Menu.hideContextMenu();

        let process = this.automaton.findProcess(processName);
        if (process === null)
            return;

        let state = this.automaton.findState(processName, stateName);
        if (state !== null)
            return;

        let area = process.areaElement;
        let x = Math.round((area.scrollLeft() + area.width() / 2) / this.SVG_SMALL_GRID_INTERVAL) * this.SVG_SMALL_GRID_INTERVAL;
        let y = Math.round((area.scrollTop() + area.height() / 2) / this.SVG_SMALL_GRID_INTERVAL) * this.SVG_SMALL_GRID_INTERVAL;

        this.automaton.executeCommand(new CreateStateCommand(processName, stateName, x, y));
    },

    renameState: function(processName, oldStateName, newStateName) {
        Menu.hideContextMenu();

        let state = this.automaton.findState(processName, oldStateName);
        if (state !== null)
            this.automaton.executeCommand(new RenameStateCommand(processName, oldStateName, newStateName));
    },

    removeState: function(processName, stateName) {
        Menu.hideContextMenu();

        let state = this.automaton.findState(processName, stateName);
        if (state !== null)
            this.automaton.executeCommand(new RemoveStateCommand(processName, stateName));
    },

    changeStateSelection: function(instance) {
        Menu.hideContextMenu();

        if (instance.isSelected === true)
            this.automaton.executeCommand(new DeselectStateCommand(instance.processName, instance.stateName));
        else if (instance.isSelected === false)
            this.automaton.executeCommand(new SelectStateCommand(instance.processName, instance.stateName));
    },

    changeStateInitial: function(processName, stateName) {
        Menu.hideContextMenu();

        let state = this.automaton.findState(processName, stateName);
        if (state !== null) {
            if (state.isInitial)
                this.automaton.executeCommand(new ResetInitialForStateCommand(processName, stateName));
            else
                this.automaton.executeCommand(new SetInitialForStateCommand(processName, stateName));
        }
    },

    createTransition: function(processName, firstStateName, secondStateName, condition) {
        Menu.hideContextMenu();

        condition = JSON.stringify(condition);

        let transition = this.automaton.findTransition(processName, firstStateName, secondStateName, condition);
        if (transition === null)
            this.automaton.executeCommand(new CreateTransitionCommand(processName, firstStateName, secondStateName, condition));
    },

    changeConditionForTransition: function(processName, firstStateName, secondStateName, oldCondition, newCondition) {
        Menu.hideContextMenu();

        oldCondition = JSON.stringify(oldCondition);
        newCondition = JSON.stringify(newCondition);

        let transition = this.automaton.findTransition(processName, firstStateName, secondStateName, newCondition);
        if (transition !== null)
            return;

        transition = this.automaton.findTransition(processName, firstStateName, secondStateName, oldCondition);
        if (transition === null)
            return;

        this.automaton.executeCommand(new SetConditionForTransitionCommand(processName, firstStateName, secondStateName, oldCondition, newCondition));
    },

    removeTransition: function(processName, firstStateName, secondStateName, condition) {
        Menu.hideContextMenu();

        condition = JSON.stringify(condition);

        let transition = this.automaton.findTransition(processName, firstStateName, secondStateName, condition);
        if (transition !== null)
            this.automaton.executeCommand(new RemoveTransitionCommand(processName, firstStateName, secondStateName, condition));
    },

    changeTransitionSelection: function(instance) {
        Menu.hideContextMenu();

        if (instance.isSelected === true)
            this.automaton.executeCommand(new DeselectTransitionCommand(instance.processName, instance.firstStateName, instance.secondStateName, instance.condition));
        else if (instance.isSelected === false)
            this.automaton.executeCommand(new SelectTransitionCommand(instance.processName, instance.firstStateName, instance.secondStateName, instance.condition));
    },

    undo: function() {
        Menu.hideContextMenu();

        this.automaton.undo();
    },

    redo: function() {
        Menu.hideContextMenu();

        this.automaton.redo();
    },

    checkProcessName: function(processName) {
        let match = processName.match(this.PROCESS_NAME_REGEXP);

        if (match && match.length === 1 && match[0] === processName) {
            let process = this.automaton.findProcess(processName);
            if (process !== null)
                return 0x2;

            return 0x0;
        }
        else
            return 0x1;
    },

    checkStateName: function(processName, stateName) {
        let match = stateName.match(this.STATE_NAME_REGEXP);

        if (match && match.length === 1 && match[0] === stateName) {
            let state = this.automaton.findState(processName, stateName);
            if (state !== null)
                return 0x2;

            return 0x0;
        }
        else
            return 0x1;
    },

    checkTransition: function(processName, firstStateName, secondStateName, condition) {
        let match = condition.match(this.TRANSITION_CONDITION_REGEXP);

        if (match && match.length === 1 && match[0] === condition) {
            let state = this.automaton.findTransition(processName, firstStateName, secondStateName, JSON.stringify(condition));
            if (state !== null)
                return 0x2;

            return 0x0;
        }
        else
            return 0x1;
    }
};