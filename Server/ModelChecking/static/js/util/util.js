/**
 * Enum for log() function.
 **/
const LogLevel = {DEBUG: 0x1, LOG: 0x2, INFO: 0x3, ERROR: 0x4};


/***
 *
 * Shell for the console.log() function, which print message with time, tag and level.
 *
 * @param level - Log level (type LogLevel enum).
 * @param message - Log message (type String).
 *
 * @author Boyarkin Nikita
 *
 */
function log(level, message) {
    // Get caller function name.
    let tag = "Unknown";
    try {
        tag = (!arguments.callee || !arguments.callee.caller) ? "Unknown" : arguments.callee.caller.name;
    }
    catch (error) {
    }

    // Get current time.
    let now = new Date();
    let time = now.toLocaleTimeString([], {hour12: false}) + "." + ("0" + now.getMilliseconds()).slice(-3);

    // Result message.
    let string = time + " - " + tag + "()" + " - " + message;

    // Call special function depending on log level.
    switch (level) {
        case LogLevel.DEBUG:
            console.debug(string);
            return;
        case LogLevel.LOG:
            console.log(string);
            return;
        case LogLevel.INFO:
            console.info(string);
            return;
        case LogLevel.ERROR:
            console.error(string);
            return;
        default:
            return;
    }
}