let Interaction = {
    groupInformation: null,
    membersInformation: null,
    curatorsInformation: null,

    initializeGroup(groupInformation, membersInformation, curatorsInformation) {
        this.groupInformation = (groupInformation) ? groupInformation.toString() : null;
        this.membersInformation = (membersInformation) ? membersInformation.toString() : null;
        this.curatorsInformation = (curatorsInformation) ? curatorsInformation.toString() : null;
    },

    onWindowLoad: function() {
        this.restoreContext();
    },
    
    restoreContext: function() {
        if (this.groupInformation !== null) {
            let groupJson = JSON.parse(this.groupInformation.toString())[0];

            let groupTitle = groupJson["fields"]["title"];
            if (groupTitle)
                $("#information-group-title").text(groupTitle);

            let groupId = groupJson["pk"];
            if (groupId)
                $("#information-group-identifier").text("#" + groupId);
        }

        if (this.membersInformation !== null && this.curatorsInformation !== null) {
            let membersJson = JSON.parse(this.membersInformation.toString());
            let curatorsJson = JSON.parse(this.curatorsInformation.toString());

            Workarea.initialize(membersJson, curatorsJson);
        }
    }
};