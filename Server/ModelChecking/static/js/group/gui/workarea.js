let Workarea = {
    isMembersMode: null,
    membersList: null,
    curatorsList: null,

    initialize: function(membersJson, curatorsJson) {
        this.membersFormer(membersJson);
        this.curatorsFormer(curatorsJson);
        this.initializeHandlers();
    },

    initializeHandlers: function() {
        let membersTab = $("#student-tab"),
            curatorsTab = $("#curator-tab"),
            membersContent = $("#student-content"),
            curatorsContent = $("#curator-content");

        membersTab.addClass("active-tab");
        curatorsTab.removeClass("active-tab");
        membersContent.show();
        curatorsContent.hide();

        this.isMembersMode = true;

        this.removeHandlers();
        this.addHandlers();

        membersTab.on("click", this.onMemberTabClick);
        curatorsTab.on("click", this.onCursorTabClick);
    },

    membersFormer: function(membersJson) {
        if (this.membersList !== null)
            return;

        this.membersList = [];
        for (let memberInformation of membersJson) {
            let item = $("#clonable-student-item").clone();
            item.removeAttr("id");

            let memberIdentifier = memberInformation["pk"];
            if (memberIdentifier !== undefined)
                item.attr("data-id", memberIdentifier);

            let memberEmail = memberInformation["fields"]["email"];
            if (memberEmail !== undefined)
                item.attr("data-email", memberEmail);

            let memberFirstName = memberInformation["fields"]["first_name"];
            if (memberFirstName !== undefined)
                item.attr("data-first-name", memberFirstName);

            let memberLastName = memberInformation["fields"]["last_name"];
            if (memberLastName !== undefined)
                item.attr("data-last-name", memberLastName);

            if (memberLastName !== undefined && memberFirstName !== undefined)
                item.find("figcaption").text(memberLastName + " " + memberFirstName);

            item.appendTo($("#student-content"));
            this.membersList.push(item);
        }
    },

    curatorsFormer: function(curatorsJson) {
        if (this.curatorsList !== null)
            return;

        this.curatorsList = [];
        for (let curatorInformation of curatorsJson) {
            let item = $("#clonable-curator-item").clone();
            item.removeAttr("id");

            let curatorIdentifier = curatorInformation["pk"];
            if (curatorIdentifier !== undefined)
                item.attr("data-id", curatorIdentifier);

            let curatorEmail = curatorInformation["fields"]["email"];
            if (curatorEmail !== undefined)
                item.attr("data-email", curatorEmail);

            let curatorFirstName = curatorInformation["fields"]["first_name"];
            if (curatorFirstName !== undefined)
                item.attr("data-first-name", curatorFirstName);

            let curatorLastName = curatorInformation["fields"]["last_name"];
            if (curatorLastName !== undefined)
                item.attr("data-last-name", curatorLastName);

            let curatorPosition = curatorInformation["fields"]["position"];
            if (curatorPosition !== undefined)
                item.attr("data-position", curatorPosition);

            if (curatorLastName !== undefined && curatorFirstName !== undefined)
                item.find("figcaption").text(curatorLastName + " " + curatorFirstName);

            item.appendTo($("#curator-content"));
            this.curatorsList.push(item);
        }
    },

    addHandlers: function() {
        if (this.membersList === null || this.curatorsList === null)
            return;

        let workarea = this.isMembersMode ? $("#student-content") : $("#curator-content");
        workarea.on("click", this.onLeftWorkareaClick);
        workarea.on("contextmenu", this.onRightWorkareaClick);

        let list = this.isMembersMode ? this.membersList : this.curatorsList;
        for (let currentItem of list) {
            currentItem.on("click", this.onLeftClick);
            currentItem.on("contextmenu", this.onRightClick);
            currentItem.on("dblclick", this.onDoubleClick);
        }
    },

    removeHandlers: function() {
        if (this.membersList === null || this.curatorsList === null)
            return;

        let workarea = this.isMembersMode ? $("#curator-content") : $("#student-content");
        workarea.off("click");
        workarea.off("contextmenu");

        let list = this.isMembersMode ? this.curatorsList : this.membersList;
        for (let currentItem of list) {
            currentItem.off("click");
            currentItem.off("contextmenu");
            currentItem.off("dblclick");
        }
    },

    onMemberTabClick(event) {
        if (!Workarea.isMembersMode) {
            $("#student-tab").addClass("active-tab");
            $("#curator-tab").removeClass("active-tab");
            $("#student-content").show();
            $("#curator-content").hide();

            $("#student-content").children().removeClass("active-item");
            $("#curator-content").children().removeClass("active-item");
            $("#about-student").hide();
            $("#about-curator").hide();

            Workarea.isMembersMode = true;

            Workarea.removeHandlers();
            Workarea.addHandlers();
        }
    },

    onCursorTabClick(event) {
        if (Workarea.isMembersMode) {
            $("#student-tab").removeClass("active-tab");
            $("#curator-tab").addClass("active-tab");
            $("#student-content").hide();
            $("#curator-content").show();

            $("#student-content").children().removeClass("active-item");
            $("#curator-content").children().removeClass("active-item");
            $("#about-student").hide();
            $("#about-curator").hide();

            Workarea.isMembersMode = false;

            Workarea.removeHandlers();
            Workarea.addHandlers();
        }
    },

    onLeftWorkareaClick(event) {
        let element = $(this);

        element.children().removeClass("active-item");
        $("#about-student").hide();
        $("#about-curator").hide();

        event.stopPropagation();
    },

    onRightWorkareaClick(event) {
        let element = $(this);

        // TODO: Show context menu.

        event.preventDefault();
        event.stopPropagation();
    },

    onLeftClick(event) {
        let element = $(this);

        let isSelected = element.hasClass("active-item");
        element.parent().children().removeClass("active-item");

        let aboutMember = $("#about-student");
        let aboutCurator = $("#about-curator");
        if (!isSelected) {
            element.addClass("active-item");

            if (Workarea.isMembersMode) {
                let memberId = element.attr("data-id");
                $("#about-student-identifier").text((memberId !== undefined) ? "#" + memberId : "");

                let memberEmail = element.attr("data-email");
                $("#about-student-email").text((memberEmail !== undefined) ? memberEmail : "");

                let memberFirstName = element.attr("data-first-name");
                $("#about-student-first-name").text((memberFirstName !== undefined) ? memberFirstName : "");

                let memberLastName = element.attr("data-last-name");
                $("#about-student-last-name").text((memberLastName !== undefined) ? memberLastName : "");

                // TODO: Buttons.

                aboutMember.show();
            } else {
                let curatorId = element.attr("data-id");
                $("#about-curator-identifier").text((curatorId !== undefined) ? "#" + curatorId : "");

                let curatorEmail = element.attr("data-email");
                $("#about-curator-email").text((curatorEmail !== undefined) ? curatorEmail : "");

                let curatorFirstName = element.attr("data-first-name");
                $("#about-curator-first-name").text((curatorFirstName !== undefined) ? curatorFirstName : "");

                let curatorLastName = element.attr("data-last-name");
                $("#about-curator-last-name").text((curatorLastName !== undefined) ? curatorLastName : "");

                let curatorPosition = element.attr("data-position");
                $("#about-curator-position").text((curatorPosition !== undefined) ? curatorPosition : "");

                // TODO: Buttons.

                aboutCurator.show();
            }
        } else {
            aboutMember.hide();
            aboutCurator.hide()
        }

        event.stopPropagation();
    },

    onRightClick(event) {
        let element = $(this);

        // TODO: Show context menu.

        event.preventDefault();
        event.stopPropagation();
    },

    onDoubleClick(event) {
        let element = $(this);

        if (Workarea.isMembersMode) {
            let memberId = element.attr("data-id");
            if (memberId !== undefined)
                window.location.href = "/student?id=" + memberId;
        } else {
            let curatorId = element.attr("data-id");
            if (curatorId !== undefined)
                window.location.href = "/teacher?id=" + curatorId;
        }

        event.stopPropagation();
    }
};
