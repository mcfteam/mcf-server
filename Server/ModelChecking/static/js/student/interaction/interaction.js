let Interaction = {
    studentInformation: null,
    projectsInformation: null,
    groupsInformation: null,

    initializeStudent(studentInformation, projectsInformation, groupsInformation) {
        this.studentInformation = (studentInformation) ? studentInformation.toString() : null;
        this.projectsInformation = (projectsInformation) ? projectsInformation.toString() : null;
        this.groupsInformation = (groupsInformation) ? groupsInformation.toString() : null;
    },

    onWindowLoad: function() {
        this.restoreContext();
    },
    
    restoreContext: function() {
        if (this.studentInformation !== null) {
            let studentJson = JSON.parse(this.studentInformation.toString())[0];

            let studentFirstName = studentJson["fields"]["first_name"];
            if (studentFirstName)
                $("#information-student-first-name").text(studentFirstName);

            let studentLastName = studentJson["fields"]["last_name"];
            if (studentLastName)
                $("#information-student-last-name").text(studentLastName);

            let studentEmail = studentJson["fields"]["email"];
            if (studentEmail)
                $("#information-student-email").text(studentEmail);

            let studentId = studentJson["pk"];
            if (studentId)
                $("#information-student-identifier").text("#" + studentId);
        }

        if (this.projectsInformation !== null && this.groupsInformation !== null) {
            let projectsJson = JSON.parse(this.projectsInformation.toString());
            let groupsJson = JSON.parse(this.groupsInformation.toString());

            Workarea.initialize(projectsJson, groupsJson);
        }
    }
};