let Error = {
    /**
     *
     * Information about personal cabinet.
     *
     */
    cabinetErrorCode: null,

    /**
     *
     * Initializes personal cabinet error code.
     *
     * @param cabinetErrorCode - Get/Post error code.
     * @type {String}
     *
     * @author Boyarkin Nikita
     *
     */
    initializeError: function(cabinetErrorCode) {
        this.cabinetErrorCode = ($.isNumeric(cabinetErrorCode)) ? parseInt(cabinetErrorCode) : null;
    },

    /**
     * TODO
     */
    onWindowLoad: function() {
        this.restoreContext();
    },

    /**
     * TODO
     */
    restoreContext: function() {
        let cabinetContentScreen = $("#cabinet-content-screen"),
            cabinetErrorScreen = $("#cabinet-error-screen"),
            cabinetErrorMessage = $("#cabinet-error-message");

        if (this.cabinetErrorCode === null || this.cabinetErrorCode === 0x0) {
            cabinetContentScreen.show();
            cabinetErrorScreen.hide();
        } else {
            cabinetContentScreen.hide();
            cabinetErrorScreen.show();

            cabinetErrorMessage.text(this.getErrorMessage());
        }
    },

    /**
     *
     * Returns error message, based on current error code.
     *
     * @return {String}
     *
     * @author Boyarkin Nikita
     *
     */
    getErrorMessage: function() {
        switch (this.cabinetErrorCode) {
            case 0x1:
                return "0x01 - The request does not contains the 'id' field.";
            case 0x2:
                return "0x02 - User is not logged in.";
            case 0x3:
                return "0x03 - Field 'id' must be a number.";
            case 0x4:
                return "0x04 - Field 'id' must be a positive.";
            case 0x5:
                return "0x05 - This page does not yet exist.";
            case 0x6:
                return "0x06 - Another type of query.";
            default:
                return null;
        }
    }
};