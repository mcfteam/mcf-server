let Dialog = {
    ESCAPE_KEY: 0x1B,
    ENTER_KEY: 0xD,

    contextDisplayed: null,
    dialogDisplayed: null,
    dialogSubmitButton: null,

    resetVariables: function() {
        this.contextDisplayed = false;
        this.dialogDisplayed = false;
        this.dialogSubmitButton = null;
    },

    keyHandler: function(event) {
        if (this.dialogDisplayed === true || this.contextDisplayed === true) {
            this.dialogHandler(event);
            return false;
        }

        return true;
    },

    dialogHandler: function(event) {
        switch (event.keyCode) {
            case this.ESCAPE_KEY:
                if (this.dialogDisplayed === true)
                    this.hideDialog();
                else if (this.contextDisplayed === true)
                    this.hideContext();
                break;

            case this.ENTER_KEY:
                if (this.dialogSubmitButton)
                    this.dialogSubmitButton.click();
                break;

            default:
                break;
        }
    },
};