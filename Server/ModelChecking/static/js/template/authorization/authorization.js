let Authorization = {
    /**
     *
     * Information about authorized user.
     *
     */
    userErrorCode: null,
    userType: null,
    userEmail: null,

    /**
     *
     * Initializes authorization variables.
     *
     * @param userErrorCode - Login/Registration error code.
     * @type {String}
     *
     * @param userType - User type ('student' or 'teacher').
     * @type {String}
     *
     * @param userEmail - User email.
     * @type {String}
     *
     * @author Boyarkin Nikita
     *
     */
    initializeAuthorization: function(userErrorCode, userType, userEmail) {
        this.userErrorCode = ($.isNumeric(userErrorCode)) ? parseInt(userErrorCode) : null;
        this.userType = (userType !== "student" && userType !== "teacher") ? null : userType.toString();
        this.userEmail = (userEmail) ? userEmail.toString() : null;
    },

    /**
     * TODO
     */
    onWindowLoad: function() {
        this.restoreContext();
    },

    /**
     * TODO
     */
    onWindowClick: function(event) {
        if (event.target === $("#authenticate-dialog-content")[0])
            this.hideDialog();
    },

    /**
     * TODO
     */
    restoreContext: function() {
        let loginSuccessElement = $("#login-success"),
            loginFailedElement = $("#login-failed"),
            loginSuccessEmailTitle = $("#login-success-email-title"),
            loginSuccessProfileElement = $("#login-success-to-profile"),
            loginSuccessLogoutIdentifierElement = $("#logout-user-identifier");

        if (this.userErrorCode === 0x0 && this.userType !== null && this.userEmail !== null) {
            loginSuccessElement.show();
            loginFailedElement.hide();

            loginSuccessEmailTitle.text(this.userEmail);
            loginSuccessProfileElement.prop("href", "/" + this.userType);
            loginSuccessLogoutIdentifierElement.val();
        } else {
            loginSuccessElement.hide();
            loginFailedElement.show();
        }

        this.initiateErrorDialogIfNeeded();
    },

    /**
     * TODO
     */
    hideDialog: function() {
        $("#authenticate-dialog-content").hide();
        $("#dialog-register").hide();
        $("#dialog-login").hide();

        Dialog.dialogDisplayed = false;
        Dialog.dialogSubmitButton = false;
    },

    /**
     * TODO
     */
    showRegistrationDialog: function() {
        Dialog.hideContext();

        $("#dialog-register-first-name").val("");
        $("#dialog-register-last-name").val("");
        $("#dialog-register-email").val("");
        $("#dialog-register-password").val("");
        $("#dialog-register-password-confirm").val("");
        $("#dialog-register-is-teacher").prop("checked", false);
        $("#dialog-register-position").val("");

        $("#dialog-register-submit-error").text("");
        $("#dialog-register-submit-error").hide();

        $("#authenticate-dialog-content").show();
        $("#dialog-register").show();

        Dialog.dialogDisplayed = true;
        Dialog.dialogSubmitButton = $("#dialog-register-submit-button");
    },

    /**
     * TODO
     */
    showLoginDialog: function() {
        Dialog.hideContext();

        $("#dialog-login-email").val((this.userEmail !== null) ? this.userEmail : "");
        $("#dialog-login-password").val("");

        $("#authenticate-dialog-content").show();
        $("#dialog-login").show();

        $("#dialog-login-submit-error").text("");
        $("#dialog-login-submit-error").hide();

        Dialog.dialogDisplayed = true;
        Dialog.dialogSubmitButton = $("#dialog-login-submit-button");
    },

    /**
     * TODO
     */
    validateRegistrationForm: function() {
        let password = $("#dialog-register-password").val();
        let passwordConfirm = $("#dialog-register-password-confirm").val();

        // TODO Check password quality.
        if (password !== passwordConfirm) {
            $("#dialog-register-submit-error").text("Passwords must match!");
            $("#dialog-register-submit-error").show();
            return false;
        }

        return true;
    },

    /**
     * TODO
     */
    teacherCheckedChanged: function(element) {
        let registerPositionElement = $("#dialog-register-position");
        if (element.checked)
            registerPositionElement.prop("required", "required");
        else
            registerPositionElement.removeAttr("required");
    },

    /**
     * TODO
     */
    initiateErrorDialogIfNeeded: function() {
        if (this.userErrorCode === null || this.userErrorCode === undefined || this.userErrorCode === 0x0)
            return;

        if (this.userErrorCode <= 0x7) {
            Authorization.hideDialog();
            Authorization.showRegistrationDialog();

            $("#dialog-register-submit-error").text(this.getErrorMessage());
            $("#dialog-register-submit-error").show();
        } else if (this.userErrorCode === 0x8) {
            Authorization.hideDialog();
            Authorization.showLoginDialog();

            $("#dialog-login-submit-error").text(this.getErrorMessage());
            $("#dialog-login-submit-error").show();
        }
    },

    /**
     *
     * Returns error message, based on current error code.
     *
     * @return {String}
     *
     * @author Boyarkin Nikita
     *
     */
    getErrorMessage: function() {
        switch (this.userErrorCode) {
            case 0x1:
                return "0x01 - Wrong instance of parameter.";
            case 0x2:
                return "0x02 - Wrong type of user.";
            case 0x3:
                return "0x03 - One or more fields are empty.";
            case 0x4:
                return "0x04 - One or more fields are exceed the limit.";
            case 0x5:
                return "0x05 - Wrong email field.";
            case 0x6:
                return "0x06 - User with this email is already exists.";
            case 0x7:
                return "0x07 - Unknown exception.";
            case 0x8:
                return "0x08 - Invalid email or password.";
            case 0x9:
                return "0x09 - User is authenticated, but not exists.";
            default:
                return null;
        }
    }
};
