let Interaction = {
    teacherInformation: null,
    projectsInformation: null,
    groupsInformation: null,

    initializeTeacher(teacherInformation, projectsInformation, groupsInformation) {
        this.teacherInformation = (teacherInformation) ? teacherInformation.toString() : null;
        this.projectsInformation = (projectsInformation) ? projectsInformation.toString() : null;
        this.groupsInformation = (groupsInformation) ? groupsInformation.toString() : null;
    },

    onWindowLoad: function() {
        this.restoreContext();
    },
    
    restoreContext: function() {
        if (this.teacherInformation !== null) {
            let teacherJson = JSON.parse(this.teacherInformation.toString())[0];

            let teacherFirstName = teacherJson["fields"]["first_name"];
            if (teacherFirstName)
                $("#information-teacher-first-name").text(teacherFirstName);

            let teacherLastName = teacherJson["fields"]["last_name"];
            if (teacherLastName)
                $("#information-teacher-last-name").text(teacherLastName);

            let teacherEmail = teacherJson["fields"]["email"];
            if (teacherEmail)
                $("#information-teacher-email").text(teacherEmail);

            let teacherPosition = teacherJson["fields"]["position"];
            if (teacherPosition)
                $("#information-teacher-position").text(teacherPosition);

            let teacherId = teacherJson["pk"];
            if (teacherId)
                $("#information-teacher-identifier").text("#" + teacherId);
        }

        if (this.projectsInformation !== null && this.groupsInformation !== null) {
            let projectsJson = JSON.parse(this.projectsInformation.toString());
            let groupsJson = JSON.parse(this.groupsInformation.toString());

            Workarea.initialize(projectsJson, groupsJson);
        }
    }
};