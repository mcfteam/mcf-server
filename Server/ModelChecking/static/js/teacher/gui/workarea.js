let Workarea = {
    isProjectsMode: null,
    projectsList: null,
    groupsList: null,

    initialize: function(projectsJson, groupsJson) {
        this.projectsFormer(projectsJson);
        this.groupsFormer(groupsJson);
        this.initializeHandlers();
    },

    initializeHandlers: function() {
        let projectsTab = $("#project-tab"),
            groupsTab = $("#group-tab"),
            projectsContent = $("#project-content"),
            groupsContent = $("#group-content");

        projectsTab.addClass("active-tab");
        groupsTab.removeClass("active-tab");
        projectsContent.show();
        groupsContent.hide();

        this.isProjectsMode = true;

        this.removeHandlers();
        this.addHandlers();

        projectsTab.on("click", this.onProjectTabClick);
        groupsTab.on("click", this.onGroupTabClick);
    },

    projectsFormer: function(projectsJson) {
        if (this.projectsList !== null)
            return;

        this.projectsList = [];
        for (let projectInformation of projectsJson) {
            let item = $("#clonable-project-item").clone();
            item.removeAttr("id");

            let projectIdentifier = projectInformation["pk"];
            if (projectIdentifier !== undefined)
                item.attr("data-id", projectIdentifier);

            let projectTitle = projectInformation["fields"]["title"];
            if (projectTitle !== undefined)
                item.attr("data-title", projectTitle);

            if (projectTitle !== undefined)
                item.find("figcaption").text(projectTitle);

            item.appendTo($("#project-content"));
            this.projectsList.push(item);
        }
    },

    groupsFormer: function(groupsJson) {
        if (this.groupsList !== null)
            return;

        this.groupsList = [];
        for (let groupInformation of groupsJson) {
            let item = $("#clonable-group-item").clone();
            item.removeAttr("id");

            let groupIdentifier = groupInformation["pk"];
            if (groupIdentifier !== undefined)
                item.attr("data-id", groupIdentifier);

            let groupTitle = groupInformation["fields"]["title"];
            if (groupTitle !== undefined)
                item.attr("data-title", groupTitle);

            if (groupTitle !== undefined)
                item.find("figcaption").text(groupTitle);

            item.appendTo($("#group-content"));
            this.groupsList.push(item);
        }
    },

    addHandlers: function() {
        if (this.projectsList === null || this.groupsList === null)
            return;

        let workarea = this.isProjectsMode ? $("#project-content") : $("#group-content");
        workarea.on("click", this.onLeftWorkareaClick);
        workarea.on("contextmenu", this.onRightWorkareaClick);

        let list = this.isProjectsMode ? this.projectsList : this.groupsList;
        for (let currentItem of list) {
            currentItem.on("click", this.onLeftClick);
            currentItem.on("contextmenu", this.onRightClick);
            currentItem.on("dblclick", this.onDoubleClick);
        }
    },

    removeHandlers: function() {
        if (this.projectsList === null || this.groupsList === null)
            return;

        let workarea = this.isProjectsMode ? $("#group-content") : $("#project-content");
        workarea.off("click");
        workarea.off("contextmenu");

        let list = this.isProjectsMode ? this.groupsList : this.projectsList;
        for (let currentItem of list) {
            currentItem.off("click");
            currentItem.off("contextmenu");
            currentItem.off("dblclick");
        }
    },

    onProjectTabClick(event) {
        if (!Workarea.isProjectsMode) {
            $("#project-tab").addClass("active-tab");
            $("#group-tab").removeClass("active-tab");
            $("#project-content").show();
            $("#group-content").hide();

            $("#project-content").children().removeClass("active-item");
            $("#group-content").children().removeClass("active-item");
            $("#about-project").hide();
            $("#about-group").hide();

            Workarea.isProjectsMode = true;

            Workarea.removeHandlers();
            Workarea.addHandlers();
        }
    },

    onGroupTabClick(event) {
        if (Workarea.isProjectsMode) {
            $("#project-tab").removeClass("active-tab");
            $("#group-tab").addClass("active-tab");
            $("#project-content").hide();
            $("#group-content").show();

            $("#project-content").children().removeClass("active-item");
            $("#group-content").children().removeClass("active-item");
            $("#about-project").hide();
            $("#about-group").hide();

            Workarea.isProjectsMode = false;

            Workarea.removeHandlers();
            Workarea.addHandlers();
        }
    },

    onLeftWorkareaClick(event) {
        let element = $(this);

        element.children().removeClass("active-item");
        $("#about-project").hide();
        $("#about-group").hide();

        event.stopPropagation();
    },

    onRightWorkareaClick(event) {
        let element = $(this);

        // TODO: Show context menu.

        event.preventDefault();
        event.stopPropagation();
    },

    onLeftClick(event) {
        let element = $(this);

        let isSelected = element.hasClass("active-item");
        element.parent().children().removeClass("active-item");

        let aboutProject = $("#about-project");
        let aboutGroup = $("#about-group");
        if (!isSelected) {
            element.addClass("active-item");

            if (Workarea.isProjectsMode) {
                let projectId = element.attr("data-id");
                $("#about-project-identifier").text((projectId !== undefined) ? "#" + projectId : "");

                let projectTitle = element.attr("data-title");
                $("#about-project-title").text((projectTitle !== undefined) ? projectTitle : "");

                // TODO: Buttons.

                aboutProject.show();
            } else {
                let groupId = element.attr("data-id");
                $("#about-group-identifier").text((groupId !== undefined) ? "#" + groupId : "");

                let groupTitle = element.attr("data-title");
                $("#about-group-title").text((groupTitle !== undefined) ? groupTitle : "");

                // TODO: Buttons.

                aboutGroup.show();
            }
        } else {
            aboutProject.hide();
            aboutGroup.hide()
        }

        event.stopPropagation();
    },

    onRightClick(event) {
        let element = $(this);

        // TODO: Show context menu.

        event.preventDefault();
        event.stopPropagation();
    },

    onDoubleClick(event) {
        let element = $(this);

        if (Workarea.isProjectsMode) {
            let projectId = element.attr("data-id");
            if (projectId !== undefined)
                window.location.href = "/project?id=" + projectId;
        } else {
            let groupId = element.attr("data-id");
            if (groupId !== undefined)
                window.location.href = "/group?id=" + groupId;
        }

        event.stopPropagation();
    }
};
