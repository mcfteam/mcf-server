from django.contrib import admin

from ModelChecking.models import *

admin.site.register(User)
admin.site.register(Group)
admin.site.register(Curator)
admin.site.register(Member)
admin.site.register(File)
admin.site.register(Project)
