# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-11-07 01:13
from __future__ import unicode_literals

import ModelChecking.managers
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Curator',
            fields=[
                ('curator_id', models.BigAutoField(db_column='curator_id', primary_key=True, serialize=False, unique=True)),
                ('is_owner', models.BooleanField(db_column='is_owner', serialize=False)),
                ('is_notification', models.BooleanField(db_column='is_notification', serialize=False)),
            ],
            options={
                'db_table': 'curator',
            },
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('file_id', models.BigAutoField(db_column='file_id', primary_key=True, serialize=False, unique=True)),
                ('name', models.CharField(db_column='name', max_length=511)),
                ('extension', models.CharField(db_column='extension', max_length=255)),
                ('created_time', models.DateTimeField(auto_now_add=True, db_column='created_time')),
                ('modified_time', models.DateTimeField(auto_now=True, db_column='modified_time')),
                ('byte_array', models.BinaryField(db_column='byte_array', serialize=False)),
            ],
            options={
                'db_table': 'file',
            },
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('group_id', models.BigAutoField(db_column='group_id', primary_key=True, serialize=False, unique=True)),
                ('title', models.CharField(db_column='title', max_length=127)),
            ],
            options={
                'db_table': 'group',
            },
        ),
        migrations.CreateModel(
            name='Member',
            fields=[
                ('member_id', models.BigAutoField(db_column='member_id', primary_key=True, serialize=False, unique=True)),
                ('is_notification', models.BooleanField(db_column='is_notification', serialize=False)),
                ('group_id', models.ForeignKey(db_column='group_id', on_delete=django.db.models.deletion.CASCADE, serialize=False, to='ModelChecking.Group')),
            ],
            options={
                'db_table': 'member',
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('project_id', models.BigAutoField(db_column='project_id', primary_key=True, serialize=False, unique=True)),
                ('title', models.CharField(db_column='title', max_length=127)),
            ],
            options={
                'db_table': 'project',
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_id', models.BigAutoField(db_column='user_id', primary_key=True, serialize=False, unique=True)),
                ('email', models.EmailField(db_column='email', max_length=255, unique=True)),
                ('password', models.CharField(db_column='password', max_length=127, serialize=False)),
                ('joined_time', models.DateTimeField(auto_now_add=True, db_column='joined_time')),
                ('last_login', models.DateTimeField(blank=True, db_column='last_login', null=True, serialize=False)),
                ('is_superuser', models.BooleanField(db_column='is_superuser', default=False, serialize=False)),
                ('is_active', models.BooleanField(db_column='is_active', default=True, serialize=False)),
                ('is_staff', models.BooleanField(db_column='is_staff', default=False, serialize=False)),
                ('user_type', models.SmallIntegerField(db_column='user_type', default=0)),
                ('first_name', models.CharField(blank=True, db_column='first_name', max_length=127)),
                ('last_name', models.CharField(blank=True, db_column='last_name', max_length=127)),
                ('position', models.CharField(blank=True, db_column='position', max_length=127)),
                ('groups', models.ManyToManyField(blank=True, related_name='user_set', related_query_name='user', serialize=False, to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, related_name='user_set', related_query_name='user', serialize=False, to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'db_table': 'user',
            },
            managers=[
                ('objects', ModelChecking.managers.UserManager()),
            ],
        ),
        migrations.AddField(
            model_name='project',
            name='user_id',
            field=models.ForeignKey(db_column='user_id', on_delete=django.db.models.deletion.CASCADE, serialize=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='member',
            name='user_id',
            field=models.ForeignKey(db_column='user_id', on_delete=django.db.models.deletion.CASCADE, serialize=False, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='file',
            name='project_id',
            field=models.ForeignKey(db_column='project_id', on_delete=django.db.models.deletion.CASCADE, serialize=False, to='ModelChecking.Project'),
        ),
        migrations.AddField(
            model_name='curator',
            name='group_id',
            field=models.ForeignKey(db_column='group_id', on_delete=django.db.models.deletion.CASCADE, serialize=False, to='ModelChecking.Group'),
        ),
        migrations.AddField(
            model_name='curator',
            name='user_id',
            field=models.ForeignKey(db_column='user_id', on_delete=django.db.models.deletion.CASCADE, serialize=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
